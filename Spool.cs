﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Familiar_Breed_Breeder
{
    class Spool
    {
        public Fiber dominant;
        public Fiber recessive;

        public Spool()
        {
            dominant = new Fiber(true);
            recessive = new Fiber(false);
        }

        public Spool(Fiber _dominant, Fiber _recessive)
        {
            dominant = _dominant;
            recessive = _recessive;
        }

        public Spool(decimal _dom_value, decimal _dom_weight, decimal _rec_value, decimal _rec_weight, decimal _variance)
        {
            dominant.value = _dom_value;
            dominant.weight = _dom_weight;
            dominant.variance = _variance;
            recessive.value = _rec_value;
            recessive.weight = _rec_weight;
            recessive.variance = _variance;
        }


    }
}
