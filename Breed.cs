﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Familiar_Breed_Breeder
{
    class Breed
    {
        public int index;
        public string name;

        public Spool[] breed_stat = new Spool[8];

        public decimal hp;
        public decimal mp;

        public Breed()
        {
            index = -1;
            name = "default";
            hp = 0;
            mp = 0;

            for (int i = 0; i < (int)Familiar.Stats.MAX_STATS; i++)
            {
                breed_stat[i] = new Spool();
            }

        }

        public Breed(int _index,string _name)
        {
            index = _index;
            name = _name;
            hp = 0;
            mp = 0;
            for (int i = 0; i < (int)Familiar.Stats.MAX_STATS; i++)
            {
                breed_stat[i] = new Spool();
            }

            
           
        }
    }
}
