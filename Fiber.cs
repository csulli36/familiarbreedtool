﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Familiar_Breed_Breeder
{
    class Fiber
    {
        public decimal value;
        public decimal weight;
        public decimal variance;

        public Fiber()
        {
            value = 0;
            weight = .6m;
            variance = 10;
        }

        public Fiber(bool dominant)
        {
            if(dominant)
            {
                value = 10;
                weight = .8m;
                variance = 10;
            }
            else
            {
                value = -5;
                weight = .2m;
                variance = 20;
            }
        }

        public Fiber(decimal _value, decimal _weight, decimal _variance)
        {
            value = _value;
            weight = _weight;
            variance = _variance;
        }
    }
}
