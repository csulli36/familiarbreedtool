﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Familiar_Breed_Breeder
{
    class Element
    {
        public int index;
        public string name;
        public decimal[] stats;

        public decimal hp;
        public decimal mp;

        public Element()
        {
            index = -1;
            name = "default";
            stats = new decimal[8];

            
            mp = 0;
        }

        public Element(int _index,string _name)
        {
            index = _index;
            name = _name;
            stats = new decimal[8];
            hp = 0;
            mp = 0;
        }
    }
}
