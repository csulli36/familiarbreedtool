﻿namespace Familiar_Breed_Breeder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.BreedsPage = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.breed_wolf_button = new System.Windows.Forms.RadioButton();
            this.breed_spider_button = new System.Windows.Forms.RadioButton();
            this.breed_lizard_button = new System.Windows.Forms.RadioButton();
            this.breed_hawk_button = new System.Windows.Forms.RadioButton();
            this.breed_gorilla_button = new System.Windows.Forms.RadioButton();
            this.breed_fox_button = new System.Windows.Forms.RadioButton();
            this.breed_feline_button = new System.Windows.Forms.RadioButton();
            this.breed_bear_button = new System.Windows.Forms.RadioButton();
            this.label80 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.breed_luck_variance = new System.Windows.Forms.NumericUpDown();
            this.breed_speed_variance = new System.Windows.Forms.NumericUpDown();
            this.breed_insight_variance = new System.Windows.Forms.NumericUpDown();
            this.breed_dexterity_variance = new System.Windows.Forms.NumericUpDown();
            this.breed_willpower_variance = new System.Windows.Forms.NumericUpDown();
            this.breed_stamina_variance = new System.Windows.Forms.NumericUpDown();
            this.breed_spirit_variance = new System.Windows.Forms.NumericUpDown();
            this.breed_strength_variance = new System.Windows.Forms.NumericUpDown();
            this.breed_luck_rec_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_speed_rec_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_insight_rec_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_dexterity_rec_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_willpower_rec_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_stamina_rec_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_spirit_rec_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_strength_rec_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_luck_dom_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_speed_dom_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_insight_dom_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_dexterity_dom_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_willpower_dom_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_stamina_dom_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_spirit_dom_weight = new System.Windows.Forms.NumericUpDown();
            this.breed_strength_dom_weight = new System.Windows.Forms.NumericUpDown();
            this.SaveBreeds = new System.Windows.Forms.Button();
            this.SaveBreedButton = new System.Windows.Forms.Button();
            this.LoadBreedButton = new System.Windows.Forms.Button();
            this.breed_mp_num = new System.Windows.Forms.NumericUpDown();
            this.breed_luck_num = new System.Windows.Forms.NumericUpDown();
            this.breed_hp_num = new System.Windows.Forms.NumericUpDown();
            this.breed_speed_num = new System.Windows.Forms.NumericUpDown();
            this.breed_insight_num = new System.Windows.Forms.NumericUpDown();
            this.breed_dexterity_num = new System.Windows.Forms.NumericUpDown();
            this.breed_willpower_num = new System.Windows.Forms.NumericUpDown();
            this.breed_stam_num = new System.Windows.Forms.NumericUpDown();
            this.breed_spirit_num = new System.Windows.Forms.NumericUpDown();
            this.breed_strength_num = new System.Windows.Forms.NumericUpDown();
            this.magic_points_label = new System.Windows.Forms.Label();
            this.health_label = new System.Windows.Forms.Label();
            this.luck_label = new System.Windows.Forms.Label();
            this.speed_label = new System.Windows.Forms.Label();
            this.dexterity_label = new System.Windows.Forms.Label();
            this.insight_label = new System.Windows.Forms.Label();
            this.willpower_label = new System.Windows.Forms.Label();
            this.spirit_label = new System.Windows.Forms.Label();
            this.stamina_label = new System.Windows.Forms.Label();
            this.strength_label = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ElementsPage = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.element_water_button = new System.Windows.Forms.RadioButton();
            this.element_shadow_button = new System.Windows.Forms.RadioButton();
            this.element_metal_button = new System.Windows.Forms.RadioButton();
            this.element_light_button = new System.Windows.Forms.RadioButton();
            this.element_fire_button = new System.Windows.Forms.RadioButton();
            this.element_electric_button = new System.Windows.Forms.RadioButton();
            this.element_earth_button = new System.Windows.Forms.RadioButton();
            this.element_air_button = new System.Windows.Forms.RadioButton();
            this.save_element_button = new System.Windows.Forms.Button();
            this.elements_save_button = new System.Windows.Forms.Button();
            this.elements_load_button = new System.Windows.Forms.Button();
            this.elements_mp_num = new System.Windows.Forms.NumericUpDown();
            this.elements_luck_num = new System.Windows.Forms.NumericUpDown();
            this.elements_hp_num = new System.Windows.Forms.NumericUpDown();
            this.elements_speed_num = new System.Windows.Forms.NumericUpDown();
            this.elements_insight_num = new System.Windows.Forms.NumericUpDown();
            this.elements_dexterity_num = new System.Windows.Forms.NumericUpDown();
            this.elements_willpower_num = new System.Windows.Forms.NumericUpDown();
            this.elements_stamina_num = new System.Windows.Forms.NumericUpDown();
            this.elements_spirit_num = new System.Windows.Forms.NumericUpDown();
            this.elements_strength_num = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.Familiars = new System.Windows.Forms.TabPage();
            this.save_familiars = new System.Windows.Forms.Button();
            this.effect_three_damage_two_label = new System.Windows.Forms.Label();
            this.effect_three_damage_one_label = new System.Windows.Forms.Label();
            this.effect_three_duration_label = new System.Windows.Forms.Label();
            this.effect_three_name_label = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.effect_two_damage_two_label = new System.Windows.Forms.Label();
            this.effect_two_damage_one_label = new System.Windows.Forms.Label();
            this.effect_two_duration_label = new System.Windows.Forms.Label();
            this.effect_two_name_label = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label_1 = new System.Windows.Forms.Label();
            this.animation_label = new System.Windows.Forms.Label();
            this.distance_label = new System.Windows.Forms.Label();
            this.mp_cost_label = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.effect_one_damage_two_label = new System.Windows.Forms.Label();
            this.hit_chance_label = new System.Windows.Forms.Label();
            this.effect_one_damage_one_label = new System.Windows.Forms.Label();
            this.damage_label = new System.Windows.Forms.Label();
            this.effect_one_duration_label = new System.Windows.Forms.Label();
            this.cooldown_label = new System.Windows.Forms.Label();
            this.effect_one_name_label = new System.Windows.Forms.Label();
            this.crit_chance_label = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.attack_drop_down = new System.Windows.Forms.ComboBox();
            this.element_name_label = new System.Windows.Forms.Label();
            this.sub_breed_name_label = new System.Windows.Forms.Label();
            this.main_breed_name_label = new System.Windows.Forms.Label();
            this.name_label = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.mp_num = new System.Windows.Forms.Label();
            this.hp_num = new System.Windows.Forms.Label();
            this.luck_num = new System.Windows.Forms.Label();
            this.speed_num = new System.Windows.Forms.Label();
            this.dexterity_num = new System.Windows.Forms.Label();
            this.insight_num = new System.Windows.Forms.Label();
            this.willpower_num = new System.Windows.Forms.Label();
            this.spirit_num = new System.Windows.Forms.Label();
            this.stamina_num = new System.Windows.Forms.Label();
            this.strength_num = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.FamiliarDropDown = new System.Windows.Forms.ComboBox();
            this.statusStrip3 = new System.Windows.Forms.StatusStrip();
            this.MakeFamiliar = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.threadLCK = new System.Windows.Forms.Label();
            this.threadSPD = new System.Windows.Forms.Label();
            this.threadDEX = new System.Windows.Forms.Label();
            this.threadINS = new System.Windows.Forms.Label();
            this.threadWIL = new System.Windows.Forms.Label();
            this.threadSPR = new System.Windows.Forms.Label();
            this.threadSTA = new System.Windows.Forms.Label();
            this.threadSTR = new System.Windows.Forms.Label();
            this.threadLCKSR = new System.Windows.Forms.RadioButton();
            this.threadLCKSD = new System.Windows.Forms.RadioButton();
            this.threadLCKMR = new System.Windows.Forms.RadioButton();
            this.threadLCKMD = new System.Windows.Forms.RadioButton();
            this.threadSPDSR = new System.Windows.Forms.RadioButton();
            this.threadSPDSD = new System.Windows.Forms.RadioButton();
            this.threadSPDMR = new System.Windows.Forms.RadioButton();
            this.threadSPDMD = new System.Windows.Forms.RadioButton();
            this.threadDEXSR = new System.Windows.Forms.RadioButton();
            this.threadDEXSD = new System.Windows.Forms.RadioButton();
            this.threadDEXMR = new System.Windows.Forms.RadioButton();
            this.threadDEXMD = new System.Windows.Forms.RadioButton();
            this.threadINSSR = new System.Windows.Forms.RadioButton();
            this.threadINSSD = new System.Windows.Forms.RadioButton();
            this.threadINSMR = new System.Windows.Forms.RadioButton();
            this.threadINSMD = new System.Windows.Forms.RadioButton();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.threadWILSR = new System.Windows.Forms.RadioButton();
            this.threadWILSD = new System.Windows.Forms.RadioButton();
            this.threadWILMR = new System.Windows.Forms.RadioButton();
            this.threadWILMD = new System.Windows.Forms.RadioButton();
            this.threadSPRSR = new System.Windows.Forms.RadioButton();
            this.threadSPRSD = new System.Windows.Forms.RadioButton();
            this.threadSPRMR = new System.Windows.Forms.RadioButton();
            this.threadSPRMD = new System.Windows.Forms.RadioButton();
            this.threadSTASR = new System.Windows.Forms.RadioButton();
            this.threadSTASD = new System.Windows.Forms.RadioButton();
            this.threadSTAMR = new System.Windows.Forms.RadioButton();
            this.threadSTAMD = new System.Windows.Forms.RadioButton();
            this.threadSTRSR = new System.Windows.Forms.RadioButton();
            this.threadSTRSD = new System.Windows.Forms.RadioButton();
            this.threadSTRMR = new System.Windows.Forms.RadioButton();
            this.threadSTRMD = new System.Windows.Forms.RadioButton();
            this.label59 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numLCKSRW = new System.Windows.Forms.NumericUpDown();
            this.numSTRSR = new System.Windows.Forms.NumericUpDown();
            this.numSPDSRW = new System.Windows.Forms.NumericUpDown();
            this.numSTASR = new System.Windows.Forms.NumericUpDown();
            this.numDEXSRW = new System.Windows.Forms.NumericUpDown();
            this.numSPRSR = new System.Windows.Forms.NumericUpDown();
            this.numINSSRW = new System.Windows.Forms.NumericUpDown();
            this.numWILSR = new System.Windows.Forms.NumericUpDown();
            this.numWILSRW = new System.Windows.Forms.NumericUpDown();
            this.numINSSR = new System.Windows.Forms.NumericUpDown();
            this.numSPRSRW = new System.Windows.Forms.NumericUpDown();
            this.numDEXSR = new System.Windows.Forms.NumericUpDown();
            this.numSTASRW = new System.Windows.Forms.NumericUpDown();
            this.numSPDSR = new System.Windows.Forms.NumericUpDown();
            this.numSTRSRW = new System.Windows.Forms.NumericUpDown();
            this.numLCKSR = new System.Windows.Forms.NumericUpDown();
            this.subBreedBox = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.numLCKSDW = new System.Windows.Forms.NumericUpDown();
            this.numSPDSDW = new System.Windows.Forms.NumericUpDown();
            this.numDEXSDW = new System.Windows.Forms.NumericUpDown();
            this.numINSSDW = new System.Windows.Forms.NumericUpDown();
            this.numWILSDW = new System.Windows.Forms.NumericUpDown();
            this.numSPRSDW = new System.Windows.Forms.NumericUpDown();
            this.numSTASDW = new System.Windows.Forms.NumericUpDown();
            this.numSTRSDW = new System.Windows.Forms.NumericUpDown();
            this.numLCKSD = new System.Windows.Forms.NumericUpDown();
            this.numSPDSD = new System.Windows.Forms.NumericUpDown();
            this.numDEXSD = new System.Windows.Forms.NumericUpDown();
            this.numINSSD = new System.Windows.Forms.NumericUpDown();
            this.numWILSD = new System.Windows.Forms.NumericUpDown();
            this.numSPRSD = new System.Windows.Forms.NumericUpDown();
            this.numSTASD = new System.Windows.Forms.NumericUpDown();
            this.numSTRSD = new System.Windows.Forms.NumericUpDown();
            this.label47 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numLCKMRW = new System.Windows.Forms.NumericUpDown();
            this.numSTRMR = new System.Windows.Forms.NumericUpDown();
            this.numSPDMRW = new System.Windows.Forms.NumericUpDown();
            this.numSTAMR = new System.Windows.Forms.NumericUpDown();
            this.numDEXMRW = new System.Windows.Forms.NumericUpDown();
            this.numSPRMR = new System.Windows.Forms.NumericUpDown();
            this.numINSMRW = new System.Windows.Forms.NumericUpDown();
            this.numWILMR = new System.Windows.Forms.NumericUpDown();
            this.numWILMRW = new System.Windows.Forms.NumericUpDown();
            this.numINSMR = new System.Windows.Forms.NumericUpDown();
            this.numSPRMRW = new System.Windows.Forms.NumericUpDown();
            this.numDEXMR = new System.Windows.Forms.NumericUpDown();
            this.numSTAMRW = new System.Windows.Forms.NumericUpDown();
            this.numSPDMR = new System.Windows.Forms.NumericUpDown();
            this.numSTRMRW = new System.Windows.Forms.NumericUpDown();
            this.numLCKMR = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numLCKMDW = new System.Windows.Forms.NumericUpDown();
            this.numSPDMDW = new System.Windows.Forms.NumericUpDown();
            this.numDEXMDW = new System.Windows.Forms.NumericUpDown();
            this.numINSMDW = new System.Windows.Forms.NumericUpDown();
            this.numWILMDW = new System.Windows.Forms.NumericUpDown();
            this.numSPRMDW = new System.Windows.Forms.NumericUpDown();
            this.numSTAMDW = new System.Windows.Forms.NumericUpDown();
            this.numSTRMDW = new System.Windows.Forms.NumericUpDown();
            this.numLCKMD = new System.Windows.Forms.NumericUpDown();
            this.numSPDMD = new System.Windows.Forms.NumericUpDown();
            this.numDEXMD = new System.Windows.Forms.NumericUpDown();
            this.numINSMD = new System.Windows.Forms.NumericUpDown();
            this.numWILMD = new System.Windows.Forms.NumericUpDown();
            this.numSPRMD = new System.Windows.Forms.NumericUpDown();
            this.numSTAMD = new System.Windows.Forms.NumericUpDown();
            this.numSTRMD = new System.Windows.Forms.NumericUpDown();
            this.label44 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.mainBreedBox = new System.Windows.Forms.ComboBox();
            this.statusStrip4 = new System.Windows.Forms.StatusStrip();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.testcombine1 = new System.Windows.Forms.Button();
            this.tabControl.SuspendLayout();
            this.BreedsPage.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.breed_luck_variance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_speed_variance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_insight_variance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_dexterity_variance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_willpower_variance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_stamina_variance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_spirit_variance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_strength_variance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_luck_rec_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_speed_rec_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_insight_rec_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_dexterity_rec_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_willpower_rec_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_stamina_rec_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_spirit_rec_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_strength_rec_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_luck_dom_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_speed_dom_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_insight_dom_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_dexterity_dom_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_willpower_dom_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_stamina_dom_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_spirit_dom_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_strength_dom_weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_mp_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_luck_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_hp_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_speed_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_insight_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_dexterity_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_willpower_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_stam_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_spirit_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_strength_num)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.ElementsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.elements_mp_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_luck_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_hp_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_speed_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_insight_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_dexterity_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_willpower_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_stamina_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_spirit_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_strength_num)).BeginInit();
            this.statusStrip2.SuspendLayout();
            this.Familiars.SuspendLayout();
            this.MakeFamiliar.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKSRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRSR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDSRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTASR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXSRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRSR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSSRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILSR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILSRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSSR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRSRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXSR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTASRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDSR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRSRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKSR)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKSDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDSDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXSDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSSDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILSDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRSDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTASDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRSDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTASD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRSD)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKMRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRMR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDMRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTAMR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXMRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRMR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSMRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILMR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILMRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSMR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRMRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXMR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTAMRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDMR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRMRW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKMR)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKMDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDMDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXMDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSMDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILMDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRMDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTAMDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRMDW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKMD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDMD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXMD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSMD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILMD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRMD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTAMD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRMD)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(659, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl.Controls.Add(this.BreedsPage);
            this.tabControl.Controls.Add(this.ElementsPage);
            this.tabControl.Controls.Add(this.Familiars);
            this.tabControl.Controls.Add(this.MakeFamiliar);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 24);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(659, 480);
            this.tabControl.TabIndex = 1;
            this.tabControl.Tag = "";
            // 
            // BreedsPage
            // 
            this.BreedsPage.Controls.Add(this.panel1);
            this.BreedsPage.ForeColor = System.Drawing.Color.Gray;
            this.BreedsPage.Location = new System.Drawing.Point(4, 25);
            this.BreedsPage.Name = "BreedsPage";
            this.BreedsPage.Padding = new System.Windows.Forms.Padding(3);
            this.BreedsPage.Size = new System.Drawing.Size(651, 451);
            this.BreedsPage.TabIndex = 0;
            this.BreedsPage.Text = "Breeds";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Controls.Add(this.statusStrip1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(579, 453);
            this.panel1.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.breed_wolf_button);
            this.splitContainer1.Panel1.Controls.Add(this.breed_spider_button);
            this.splitContainer1.Panel1.Controls.Add(this.breed_lizard_button);
            this.splitContainer1.Panel1.Controls.Add(this.breed_hawk_button);
            this.splitContainer1.Panel1.Controls.Add(this.breed_gorilla_button);
            this.splitContainer1.Panel1.Controls.Add(this.breed_fox_button);
            this.splitContainer1.Panel1.Controls.Add(this.breed_feline_button);
            this.splitContainer1.Panel1.Controls.Add(this.breed_bear_button);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label80);
            this.splitContainer1.Panel2.Controls.Add(this.label79);
            this.splitContainer1.Panel2.Controls.Add(this.label78);
            this.splitContainer1.Panel2.Controls.Add(this.label77);
            this.splitContainer1.Panel2.Controls.Add(this.breed_luck_variance);
            this.splitContainer1.Panel2.Controls.Add(this.breed_speed_variance);
            this.splitContainer1.Panel2.Controls.Add(this.breed_insight_variance);
            this.splitContainer1.Panel2.Controls.Add(this.breed_dexterity_variance);
            this.splitContainer1.Panel2.Controls.Add(this.breed_willpower_variance);
            this.splitContainer1.Panel2.Controls.Add(this.breed_stamina_variance);
            this.splitContainer1.Panel2.Controls.Add(this.breed_spirit_variance);
            this.splitContainer1.Panel2.Controls.Add(this.breed_strength_variance);
            this.splitContainer1.Panel2.Controls.Add(this.breed_luck_rec_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_speed_rec_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_insight_rec_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_dexterity_rec_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_willpower_rec_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_stamina_rec_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_spirit_rec_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_strength_rec_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_luck_dom_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_speed_dom_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_insight_dom_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_dexterity_dom_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_willpower_dom_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_stamina_dom_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_spirit_dom_weight);
            this.splitContainer1.Panel2.Controls.Add(this.breed_strength_dom_weight);
            this.splitContainer1.Panel2.Controls.Add(this.SaveBreeds);
            this.splitContainer1.Panel2.Controls.Add(this.SaveBreedButton);
            this.splitContainer1.Panel2.Controls.Add(this.LoadBreedButton);
            this.splitContainer1.Panel2.Controls.Add(this.breed_mp_num);
            this.splitContainer1.Panel2.Controls.Add(this.breed_luck_num);
            this.splitContainer1.Panel2.Controls.Add(this.breed_hp_num);
            this.splitContainer1.Panel2.Controls.Add(this.breed_speed_num);
            this.splitContainer1.Panel2.Controls.Add(this.breed_insight_num);
            this.splitContainer1.Panel2.Controls.Add(this.breed_dexterity_num);
            this.splitContainer1.Panel2.Controls.Add(this.breed_willpower_num);
            this.splitContainer1.Panel2.Controls.Add(this.breed_stam_num);
            this.splitContainer1.Panel2.Controls.Add(this.breed_spirit_num);
            this.splitContainer1.Panel2.Controls.Add(this.breed_strength_num);
            this.splitContainer1.Panel2.Controls.Add(this.magic_points_label);
            this.splitContainer1.Panel2.Controls.Add(this.health_label);
            this.splitContainer1.Panel2.Controls.Add(this.luck_label);
            this.splitContainer1.Panel2.Controls.Add(this.speed_label);
            this.splitContainer1.Panel2.Controls.Add(this.dexterity_label);
            this.splitContainer1.Panel2.Controls.Add(this.insight_label);
            this.splitContainer1.Panel2.Controls.Add(this.willpower_label);
            this.splitContainer1.Panel2.Controls.Add(this.spirit_label);
            this.splitContainer1.Panel2.Controls.Add(this.stamina_label);
            this.splitContainer1.Panel2.Controls.Add(this.strength_label);
            this.splitContainer1.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.splitContainer1.Size = new System.Drawing.Size(579, 431);
            this.splitContainer1.SplitterDistance = 153;
            this.splitContainer1.TabIndex = 2;
            // 
            // breed_wolf_button
            // 
            this.breed_wolf_button.AutoSize = true;
            this.breed_wolf_button.Location = new System.Drawing.Point(8, 179);
            this.breed_wolf_button.Name = "breed_wolf_button";
            this.breed_wolf_button.Size = new System.Drawing.Size(47, 17);
            this.breed_wolf_button.TabIndex = 7;
            this.breed_wolf_button.TabStop = true;
            this.breed_wolf_button.Text = "Wolf";
            this.breed_wolf_button.UseVisualStyleBackColor = true;
            this.breed_wolf_button.CheckedChanged += new System.EventHandler(this.breed_wolf_button_CheckedChanged);
            // 
            // breed_spider_button
            // 
            this.breed_spider_button.AutoSize = true;
            this.breed_spider_button.Location = new System.Drawing.Point(8, 156);
            this.breed_spider_button.Name = "breed_spider_button";
            this.breed_spider_button.Size = new System.Drawing.Size(55, 17);
            this.breed_spider_button.TabIndex = 6;
            this.breed_spider_button.TabStop = true;
            this.breed_spider_button.Text = "Spider";
            this.breed_spider_button.UseVisualStyleBackColor = true;
            this.breed_spider_button.CheckedChanged += new System.EventHandler(this.breed_spider_button_CheckedChanged);
            // 
            // breed_lizard_button
            // 
            this.breed_lizard_button.AutoSize = true;
            this.breed_lizard_button.Location = new System.Drawing.Point(8, 133);
            this.breed_lizard_button.Name = "breed_lizard_button";
            this.breed_lizard_button.Size = new System.Drawing.Size(53, 17);
            this.breed_lizard_button.TabIndex = 5;
            this.breed_lizard_button.TabStop = true;
            this.breed_lizard_button.Text = "Lizard";
            this.breed_lizard_button.UseVisualStyleBackColor = true;
            this.breed_lizard_button.CheckedChanged += new System.EventHandler(this.breed_lizard_button_CheckedChanged);
            // 
            // breed_hawk_button
            // 
            this.breed_hawk_button.AutoSize = true;
            this.breed_hawk_button.Location = new System.Drawing.Point(8, 110);
            this.breed_hawk_button.Name = "breed_hawk_button";
            this.breed_hawk_button.Size = new System.Drawing.Size(53, 17);
            this.breed_hawk_button.TabIndex = 4;
            this.breed_hawk_button.TabStop = true;
            this.breed_hawk_button.Text = "Hawk";
            this.breed_hawk_button.UseVisualStyleBackColor = true;
            this.breed_hawk_button.CheckedChanged += new System.EventHandler(this.breed_hawk_button_CheckedChanged);
            // 
            // breed_gorilla_button
            // 
            this.breed_gorilla_button.AutoSize = true;
            this.breed_gorilla_button.Location = new System.Drawing.Point(8, 87);
            this.breed_gorilla_button.Name = "breed_gorilla_button";
            this.breed_gorilla_button.Size = new System.Drawing.Size(54, 17);
            this.breed_gorilla_button.TabIndex = 3;
            this.breed_gorilla_button.TabStop = true;
            this.breed_gorilla_button.Text = "Gorilla";
            this.breed_gorilla_button.UseVisualStyleBackColor = true;
            this.breed_gorilla_button.CheckedChanged += new System.EventHandler(this.breed_gorilla_button_CheckedChanged);
            // 
            // breed_fox_button
            // 
            this.breed_fox_button.AutoSize = true;
            this.breed_fox_button.Location = new System.Drawing.Point(8, 64);
            this.breed_fox_button.Name = "breed_fox_button";
            this.breed_fox_button.Size = new System.Drawing.Size(42, 17);
            this.breed_fox_button.TabIndex = 2;
            this.breed_fox_button.TabStop = true;
            this.breed_fox_button.Text = "Fox";
            this.breed_fox_button.UseVisualStyleBackColor = true;
            this.breed_fox_button.CheckedChanged += new System.EventHandler(this.breed_fox_button_CheckedChanged);
            // 
            // breed_feline_button
            // 
            this.breed_feline_button.AutoSize = true;
            this.breed_feline_button.Location = new System.Drawing.Point(8, 41);
            this.breed_feline_button.Name = "breed_feline_button";
            this.breed_feline_button.Size = new System.Drawing.Size(53, 17);
            this.breed_feline_button.TabIndex = 1;
            this.breed_feline_button.TabStop = true;
            this.breed_feline_button.Text = "Feline";
            this.breed_feline_button.UseVisualStyleBackColor = true;
            this.breed_feline_button.CheckedChanged += new System.EventHandler(this.breed_feline_button_CheckedChanged);
            // 
            // breed_bear_button
            // 
            this.breed_bear_button.AutoSize = true;
            this.breed_bear_button.Location = new System.Drawing.Point(8, 17);
            this.breed_bear_button.Name = "breed_bear_button";
            this.breed_bear_button.Size = new System.Drawing.Size(47, 17);
            this.breed_bear_button.TabIndex = 0;
            this.breed_bear_button.TabStop = true;
            this.breed_bear_button.Text = "Bear";
            this.breed_bear_button.UseVisualStyleBackColor = true;
            this.breed_bear_button.CheckedChanged += new System.EventHandler(this.breed_bear_button_CheckedChanged);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(299, 31);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(49, 13);
            this.label80.TabIndex = 66;
            this.label80.Text = "Variance";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(246, 31);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(26, 13);
            this.label79.TabIndex = 65;
            this.label79.Text = "RW";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(193, 31);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(26, 13);
            this.label78.TabIndex = 64;
            this.label78.Text = "DW";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(138, 31);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(40, 13);
            this.label77.TabIndex = 63;
            this.label77.Text = "Base +";
            // 
            // breed_luck_variance
            // 
            this.breed_luck_variance.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_luck_variance.Location = new System.Drawing.Point(302, 250);
            this.breed_luck_variance.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.breed_luck_variance.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.breed_luck_variance.Name = "breed_luck_variance";
            this.breed_luck_variance.Size = new System.Drawing.Size(47, 20);
            this.breed_luck_variance.TabIndex = 61;
            // 
            // breed_speed_variance
            // 
            this.breed_speed_variance.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_speed_variance.Location = new System.Drawing.Point(302, 224);
            this.breed_speed_variance.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.breed_speed_variance.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.breed_speed_variance.Name = "breed_speed_variance";
            this.breed_speed_variance.Size = new System.Drawing.Size(47, 20);
            this.breed_speed_variance.TabIndex = 59;
            // 
            // breed_insight_variance
            // 
            this.breed_insight_variance.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_insight_variance.Location = new System.Drawing.Point(302, 169);
            this.breed_insight_variance.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.breed_insight_variance.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.breed_insight_variance.Name = "breed_insight_variance";
            this.breed_insight_variance.Size = new System.Drawing.Size(47, 20);
            this.breed_insight_variance.TabIndex = 58;
            // 
            // breed_dexterity_variance
            // 
            this.breed_dexterity_variance.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_dexterity_variance.Location = new System.Drawing.Point(302, 198);
            this.breed_dexterity_variance.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.breed_dexterity_variance.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.breed_dexterity_variance.Name = "breed_dexterity_variance";
            this.breed_dexterity_variance.Size = new System.Drawing.Size(47, 20);
            this.breed_dexterity_variance.TabIndex = 57;
            // 
            // breed_willpower_variance
            // 
            this.breed_willpower_variance.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_willpower_variance.Location = new System.Drawing.Point(302, 143);
            this.breed_willpower_variance.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.breed_willpower_variance.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.breed_willpower_variance.Name = "breed_willpower_variance";
            this.breed_willpower_variance.Size = new System.Drawing.Size(47, 20);
            this.breed_willpower_variance.TabIndex = 56;
            // 
            // breed_stamina_variance
            // 
            this.breed_stamina_variance.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_stamina_variance.Location = new System.Drawing.Point(302, 88);
            this.breed_stamina_variance.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.breed_stamina_variance.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.breed_stamina_variance.Name = "breed_stamina_variance";
            this.breed_stamina_variance.Size = new System.Drawing.Size(47, 20);
            this.breed_stamina_variance.TabIndex = 55;
            // 
            // breed_spirit_variance
            // 
            this.breed_spirit_variance.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_spirit_variance.Location = new System.Drawing.Point(302, 117);
            this.breed_spirit_variance.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.breed_spirit_variance.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.breed_spirit_variance.Name = "breed_spirit_variance";
            this.breed_spirit_variance.Size = new System.Drawing.Size(47, 20);
            this.breed_spirit_variance.TabIndex = 54;
            // 
            // breed_strength_variance
            // 
            this.breed_strength_variance.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_strength_variance.Location = new System.Drawing.Point(302, 62);
            this.breed_strength_variance.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.breed_strength_variance.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.breed_strength_variance.Name = "breed_strength_variance";
            this.breed_strength_variance.Size = new System.Drawing.Size(47, 20);
            this.breed_strength_variance.TabIndex = 53;
            // 
            // breed_luck_rec_weight
            // 
            this.breed_luck_rec_weight.DecimalPlaces = 2;
            this.breed_luck_rec_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_luck_rec_weight.Location = new System.Drawing.Point(249, 250);
            this.breed_luck_rec_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_luck_rec_weight.Name = "breed_luck_rec_weight";
            this.breed_luck_rec_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_luck_rec_weight.TabIndex = 51;
            // 
            // breed_speed_rec_weight
            // 
            this.breed_speed_rec_weight.DecimalPlaces = 2;
            this.breed_speed_rec_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_speed_rec_weight.Location = new System.Drawing.Point(249, 224);
            this.breed_speed_rec_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_speed_rec_weight.Name = "breed_speed_rec_weight";
            this.breed_speed_rec_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_speed_rec_weight.TabIndex = 49;
            // 
            // breed_insight_rec_weight
            // 
            this.breed_insight_rec_weight.DecimalPlaces = 2;
            this.breed_insight_rec_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_insight_rec_weight.Location = new System.Drawing.Point(249, 169);
            this.breed_insight_rec_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_insight_rec_weight.Name = "breed_insight_rec_weight";
            this.breed_insight_rec_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_insight_rec_weight.TabIndex = 48;
            // 
            // breed_dexterity_rec_weight
            // 
            this.breed_dexterity_rec_weight.DecimalPlaces = 2;
            this.breed_dexterity_rec_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_dexterity_rec_weight.Location = new System.Drawing.Point(249, 198);
            this.breed_dexterity_rec_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_dexterity_rec_weight.Name = "breed_dexterity_rec_weight";
            this.breed_dexterity_rec_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_dexterity_rec_weight.TabIndex = 47;
            // 
            // breed_willpower_rec_weight
            // 
            this.breed_willpower_rec_weight.DecimalPlaces = 2;
            this.breed_willpower_rec_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_willpower_rec_weight.Location = new System.Drawing.Point(249, 143);
            this.breed_willpower_rec_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_willpower_rec_weight.Name = "breed_willpower_rec_weight";
            this.breed_willpower_rec_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_willpower_rec_weight.TabIndex = 46;
            // 
            // breed_stamina_rec_weight
            // 
            this.breed_stamina_rec_weight.DecimalPlaces = 2;
            this.breed_stamina_rec_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_stamina_rec_weight.Location = new System.Drawing.Point(249, 88);
            this.breed_stamina_rec_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_stamina_rec_weight.Name = "breed_stamina_rec_weight";
            this.breed_stamina_rec_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_stamina_rec_weight.TabIndex = 45;
            // 
            // breed_spirit_rec_weight
            // 
            this.breed_spirit_rec_weight.DecimalPlaces = 2;
            this.breed_spirit_rec_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_spirit_rec_weight.Location = new System.Drawing.Point(249, 117);
            this.breed_spirit_rec_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_spirit_rec_weight.Name = "breed_spirit_rec_weight";
            this.breed_spirit_rec_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_spirit_rec_weight.TabIndex = 44;
            // 
            // breed_strength_rec_weight
            // 
            this.breed_strength_rec_weight.DecimalPlaces = 2;
            this.breed_strength_rec_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_strength_rec_weight.Location = new System.Drawing.Point(249, 62);
            this.breed_strength_rec_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_strength_rec_weight.Name = "breed_strength_rec_weight";
            this.breed_strength_rec_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_strength_rec_weight.TabIndex = 43;
            // 
            // breed_luck_dom_weight
            // 
            this.breed_luck_dom_weight.DecimalPlaces = 2;
            this.breed_luck_dom_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_luck_dom_weight.Location = new System.Drawing.Point(196, 250);
            this.breed_luck_dom_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_luck_dom_weight.Name = "breed_luck_dom_weight";
            this.breed_luck_dom_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_luck_dom_weight.TabIndex = 41;
            // 
            // breed_speed_dom_weight
            // 
            this.breed_speed_dom_weight.DecimalPlaces = 2;
            this.breed_speed_dom_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_speed_dom_weight.Location = new System.Drawing.Point(196, 224);
            this.breed_speed_dom_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_speed_dom_weight.Name = "breed_speed_dom_weight";
            this.breed_speed_dom_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_speed_dom_weight.TabIndex = 39;
            // 
            // breed_insight_dom_weight
            // 
            this.breed_insight_dom_weight.DecimalPlaces = 2;
            this.breed_insight_dom_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_insight_dom_weight.Location = new System.Drawing.Point(196, 169);
            this.breed_insight_dom_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_insight_dom_weight.Name = "breed_insight_dom_weight";
            this.breed_insight_dom_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_insight_dom_weight.TabIndex = 38;
            // 
            // breed_dexterity_dom_weight
            // 
            this.breed_dexterity_dom_weight.DecimalPlaces = 2;
            this.breed_dexterity_dom_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_dexterity_dom_weight.Location = new System.Drawing.Point(196, 198);
            this.breed_dexterity_dom_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_dexterity_dom_weight.Name = "breed_dexterity_dom_weight";
            this.breed_dexterity_dom_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_dexterity_dom_weight.TabIndex = 37;
            // 
            // breed_willpower_dom_weight
            // 
            this.breed_willpower_dom_weight.DecimalPlaces = 2;
            this.breed_willpower_dom_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_willpower_dom_weight.Location = new System.Drawing.Point(196, 143);
            this.breed_willpower_dom_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_willpower_dom_weight.Name = "breed_willpower_dom_weight";
            this.breed_willpower_dom_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_willpower_dom_weight.TabIndex = 36;
            // 
            // breed_stamina_dom_weight
            // 
            this.breed_stamina_dom_weight.DecimalPlaces = 2;
            this.breed_stamina_dom_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_stamina_dom_weight.Location = new System.Drawing.Point(196, 88);
            this.breed_stamina_dom_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_stamina_dom_weight.Name = "breed_stamina_dom_weight";
            this.breed_stamina_dom_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_stamina_dom_weight.TabIndex = 35;
            // 
            // breed_spirit_dom_weight
            // 
            this.breed_spirit_dom_weight.DecimalPlaces = 2;
            this.breed_spirit_dom_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_spirit_dom_weight.Location = new System.Drawing.Point(196, 117);
            this.breed_spirit_dom_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_spirit_dom_weight.Name = "breed_spirit_dom_weight";
            this.breed_spirit_dom_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_spirit_dom_weight.TabIndex = 34;
            // 
            // breed_strength_dom_weight
            // 
            this.breed_strength_dom_weight.DecimalPlaces = 2;
            this.breed_strength_dom_weight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.breed_strength_dom_weight.Location = new System.Drawing.Point(196, 62);
            this.breed_strength_dom_weight.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.breed_strength_dom_weight.Name = "breed_strength_dom_weight";
            this.breed_strength_dom_weight.Size = new System.Drawing.Size(47, 20);
            this.breed_strength_dom_weight.TabIndex = 33;
            // 
            // SaveBreeds
            // 
            this.SaveBreeds.Location = new System.Drawing.Point(168, 340);
            this.SaveBreeds.Name = "SaveBreeds";
            this.SaveBreeds.Size = new System.Drawing.Size(75, 23);
            this.SaveBreeds.TabIndex = 32;
            this.SaveBreeds.Text = "Save Breed";
            this.SaveBreeds.UseVisualStyleBackColor = true;
            this.SaveBreeds.Click += new System.EventHandler(this.SaveBreeds_Click);
            // 
            // SaveBreedButton
            // 
            this.SaveBreedButton.Location = new System.Drawing.Point(291, 387);
            this.SaveBreedButton.Name = "SaveBreedButton";
            this.SaveBreedButton.Size = new System.Drawing.Size(75, 23);
            this.SaveBreedButton.TabIndex = 31;
            this.SaveBreedButton.Text = "Save Stats";
            this.SaveBreedButton.UseVisualStyleBackColor = true;
            this.SaveBreedButton.Click += new System.EventHandler(this.SaveBreedButton_Click);
            // 
            // LoadBreedButton
            // 
            this.LoadBreedButton.Location = new System.Drawing.Point(44, 387);
            this.LoadBreedButton.Name = "LoadBreedButton";
            this.LoadBreedButton.Size = new System.Drawing.Size(75, 23);
            this.LoadBreedButton.TabIndex = 30;
            this.LoadBreedButton.Text = "Load Stats";
            this.LoadBreedButton.UseVisualStyleBackColor = true;
            this.LoadBreedButton.Click += new System.EventHandler(this.LoadBreedButton_Click);
            // 
            // breed_mp_num
            // 
            this.breed_mp_num.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_mp_num.Location = new System.Drawing.Point(141, 305);
            this.breed_mp_num.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.breed_mp_num.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.breed_mp_num.Name = "breed_mp_num";
            this.breed_mp_num.Size = new System.Drawing.Size(47, 20);
            this.breed_mp_num.TabIndex = 29;
            // 
            // breed_luck_num
            // 
            this.breed_luck_num.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_luck_num.Location = new System.Drawing.Point(141, 250);
            this.breed_luck_num.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.breed_luck_num.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.breed_luck_num.Name = "breed_luck_num";
            this.breed_luck_num.Size = new System.Drawing.Size(47, 20);
            this.breed_luck_num.TabIndex = 28;
            // 
            // breed_hp_num
            // 
            this.breed_hp_num.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_hp_num.Location = new System.Drawing.Point(141, 279);
            this.breed_hp_num.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.breed_hp_num.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.breed_hp_num.Name = "breed_hp_num";
            this.breed_hp_num.Size = new System.Drawing.Size(47, 20);
            this.breed_hp_num.TabIndex = 27;
            // 
            // breed_speed_num
            // 
            this.breed_speed_num.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_speed_num.Location = new System.Drawing.Point(141, 224);
            this.breed_speed_num.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.breed_speed_num.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.breed_speed_num.Name = "breed_speed_num";
            this.breed_speed_num.Size = new System.Drawing.Size(47, 20);
            this.breed_speed_num.TabIndex = 26;
            // 
            // breed_insight_num
            // 
            this.breed_insight_num.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_insight_num.Location = new System.Drawing.Point(141, 169);
            this.breed_insight_num.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.breed_insight_num.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.breed_insight_num.Name = "breed_insight_num";
            this.breed_insight_num.Size = new System.Drawing.Size(47, 20);
            this.breed_insight_num.TabIndex = 25;
            // 
            // breed_dexterity_num
            // 
            this.breed_dexterity_num.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_dexterity_num.Location = new System.Drawing.Point(141, 198);
            this.breed_dexterity_num.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.breed_dexterity_num.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.breed_dexterity_num.Name = "breed_dexterity_num";
            this.breed_dexterity_num.Size = new System.Drawing.Size(47, 20);
            this.breed_dexterity_num.TabIndex = 24;
            // 
            // breed_willpower_num
            // 
            this.breed_willpower_num.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_willpower_num.Location = new System.Drawing.Point(141, 143);
            this.breed_willpower_num.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.breed_willpower_num.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.breed_willpower_num.Name = "breed_willpower_num";
            this.breed_willpower_num.Size = new System.Drawing.Size(47, 20);
            this.breed_willpower_num.TabIndex = 23;
            // 
            // breed_stam_num
            // 
            this.breed_stam_num.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_stam_num.Location = new System.Drawing.Point(141, 88);
            this.breed_stam_num.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.breed_stam_num.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.breed_stam_num.Name = "breed_stam_num";
            this.breed_stam_num.Size = new System.Drawing.Size(47, 20);
            this.breed_stam_num.TabIndex = 22;
            // 
            // breed_spirit_num
            // 
            this.breed_spirit_num.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_spirit_num.Location = new System.Drawing.Point(141, 117);
            this.breed_spirit_num.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.breed_spirit_num.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.breed_spirit_num.Name = "breed_spirit_num";
            this.breed_spirit_num.Size = new System.Drawing.Size(47, 20);
            this.breed_spirit_num.TabIndex = 21;
            // 
            // breed_strength_num
            // 
            this.breed_strength_num.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.breed_strength_num.Location = new System.Drawing.Point(141, 62);
            this.breed_strength_num.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.breed_strength_num.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.breed_strength_num.Name = "breed_strength_num";
            this.breed_strength_num.Size = new System.Drawing.Size(47, 20);
            this.breed_strength_num.TabIndex = 20;
            // 
            // magic_points_label
            // 
            this.magic_points_label.AutoSize = true;
            this.magic_points_label.Location = new System.Drawing.Point(22, 308);
            this.magic_points_label.Name = "magic_points_label";
            this.magic_points_label.Size = new System.Drawing.Size(68, 13);
            this.magic_points_label.TabIndex = 19;
            this.magic_points_label.Text = "Magic Points";
            // 
            // health_label
            // 
            this.health_label.AutoSize = true;
            this.health_label.Location = new System.Drawing.Point(22, 281);
            this.health_label.Name = "health_label";
            this.health_label.Size = new System.Drawing.Size(70, 13);
            this.health_label.TabIndex = 18;
            this.health_label.Text = "Health Points";
            // 
            // luck_label
            // 
            this.luck_label.AutoSize = true;
            this.luck_label.Location = new System.Drawing.Point(22, 254);
            this.luck_label.Name = "luck_label";
            this.luck_label.Size = new System.Drawing.Size(31, 13);
            this.luck_label.TabIndex = 15;
            this.luck_label.Text = "Luck";
            // 
            // speed_label
            // 
            this.speed_label.AutoSize = true;
            this.speed_label.Location = new System.Drawing.Point(22, 227);
            this.speed_label.Name = "speed_label";
            this.speed_label.Size = new System.Drawing.Size(38, 13);
            this.speed_label.TabIndex = 14;
            this.speed_label.Text = "Speed";
            // 
            // dexterity_label
            // 
            this.dexterity_label.AutoSize = true;
            this.dexterity_label.Location = new System.Drawing.Point(22, 200);
            this.dexterity_label.Name = "dexterity_label";
            this.dexterity_label.Size = new System.Drawing.Size(48, 13);
            this.dexterity_label.TabIndex = 13;
            this.dexterity_label.Text = "Dexterity";
            // 
            // insight_label
            // 
            this.insight_label.AutoSize = true;
            this.insight_label.Location = new System.Drawing.Point(22, 173);
            this.insight_label.Name = "insight_label";
            this.insight_label.Size = new System.Drawing.Size(38, 13);
            this.insight_label.TabIndex = 12;
            this.insight_label.Text = "Insight";
            // 
            // willpower_label
            // 
            this.willpower_label.AutoSize = true;
            this.willpower_label.Location = new System.Drawing.Point(22, 146);
            this.willpower_label.Name = "willpower_label";
            this.willpower_label.Size = new System.Drawing.Size(53, 13);
            this.willpower_label.TabIndex = 11;
            this.willpower_label.Text = "Willpower";
            // 
            // spirit_label
            // 
            this.spirit_label.AutoSize = true;
            this.spirit_label.Location = new System.Drawing.Point(22, 119);
            this.spirit_label.Name = "spirit_label";
            this.spirit_label.Size = new System.Drawing.Size(30, 13);
            this.spirit_label.TabIndex = 10;
            this.spirit_label.Text = "Spirit";
            // 
            // stamina_label
            // 
            this.stamina_label.AutoSize = true;
            this.stamina_label.Location = new System.Drawing.Point(22, 92);
            this.stamina_label.Name = "stamina_label";
            this.stamina_label.Size = new System.Drawing.Size(45, 13);
            this.stamina_label.TabIndex = 9;
            this.stamina_label.Text = "Stamina";
            // 
            // strength_label
            // 
            this.strength_label.AutoSize = true;
            this.strength_label.Location = new System.Drawing.Point(22, 64);
            this.strength_label.Name = "strength_label";
            this.strength_label.Size = new System.Drawing.Size(47, 13);
            this.strength_label.TabIndex = 8;
            this.strength_label.Text = "Strength";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 431);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(579, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(64, 17);
            this.toolStripStatusLabel1.Text = "Hi Markus!";
            // 
            // ElementsPage
            // 
            this.ElementsPage.Controls.Add(this.splitContainer2);
            this.ElementsPage.Controls.Add(this.statusStrip2);
            this.ElementsPage.Location = new System.Drawing.Point(4, 25);
            this.ElementsPage.Name = "ElementsPage";
            this.ElementsPage.Padding = new System.Windows.Forms.Padding(3);
            this.ElementsPage.Size = new System.Drawing.Size(651, 451);
            this.ElementsPage.TabIndex = 1;
            this.ElementsPage.Text = "Elements";
            this.ElementsPage.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.element_water_button);
            this.splitContainer2.Panel1.Controls.Add(this.element_shadow_button);
            this.splitContainer2.Panel1.Controls.Add(this.element_metal_button);
            this.splitContainer2.Panel1.Controls.Add(this.element_light_button);
            this.splitContainer2.Panel1.Controls.Add(this.element_fire_button);
            this.splitContainer2.Panel1.Controls.Add(this.element_electric_button);
            this.splitContainer2.Panel1.Controls.Add(this.element_earth_button);
            this.splitContainer2.Panel1.Controls.Add(this.element_air_button);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.save_element_button);
            this.splitContainer2.Panel2.Controls.Add(this.elements_save_button);
            this.splitContainer2.Panel2.Controls.Add(this.elements_load_button);
            this.splitContainer2.Panel2.Controls.Add(this.elements_mp_num);
            this.splitContainer2.Panel2.Controls.Add(this.elements_luck_num);
            this.splitContainer2.Panel2.Controls.Add(this.elements_hp_num);
            this.splitContainer2.Panel2.Controls.Add(this.elements_speed_num);
            this.splitContainer2.Panel2.Controls.Add(this.elements_insight_num);
            this.splitContainer2.Panel2.Controls.Add(this.elements_dexterity_num);
            this.splitContainer2.Panel2.Controls.Add(this.elements_willpower_num);
            this.splitContainer2.Panel2.Controls.Add(this.elements_stamina_num);
            this.splitContainer2.Panel2.Controls.Add(this.elements_spirit_num);
            this.splitContainer2.Panel2.Controls.Add(this.elements_strength_num);
            this.splitContainer2.Panel2.Controls.Add(this.label1);
            this.splitContainer2.Panel2.Controls.Add(this.label2);
            this.splitContainer2.Panel2.Controls.Add(this.label3);
            this.splitContainer2.Panel2.Controls.Add(this.label4);
            this.splitContainer2.Panel2.Controls.Add(this.label5);
            this.splitContainer2.Panel2.Controls.Add(this.label6);
            this.splitContainer2.Panel2.Controls.Add(this.label7);
            this.splitContainer2.Panel2.Controls.Add(this.label8);
            this.splitContainer2.Panel2.Controls.Add(this.label9);
            this.splitContainer2.Panel2.Controls.Add(this.label10);
            this.splitContainer2.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.splitContainer2.Size = new System.Drawing.Size(645, 423);
            this.splitContainer2.SplitterDistance = 169;
            this.splitContainer2.TabIndex = 3;
            // 
            // element_water_button
            // 
            this.element_water_button.AutoSize = true;
            this.element_water_button.Location = new System.Drawing.Point(8, 179);
            this.element_water_button.Name = "element_water_button";
            this.element_water_button.Size = new System.Drawing.Size(54, 17);
            this.element_water_button.TabIndex = 7;
            this.element_water_button.Text = "Water";
            this.element_water_button.UseVisualStyleBackColor = true;
            this.element_water_button.CheckedChanged += new System.EventHandler(this.element_water_button_CheckedChanged);
            // 
            // element_shadow_button
            // 
            this.element_shadow_button.AutoSize = true;
            this.element_shadow_button.Location = new System.Drawing.Point(8, 156);
            this.element_shadow_button.Name = "element_shadow_button";
            this.element_shadow_button.Size = new System.Drawing.Size(64, 17);
            this.element_shadow_button.TabIndex = 6;
            this.element_shadow_button.Text = "Shadow";
            this.element_shadow_button.UseVisualStyleBackColor = true;
            this.element_shadow_button.CheckedChanged += new System.EventHandler(this.element_shadow_button_CheckedChanged);
            // 
            // element_metal_button
            // 
            this.element_metal_button.AutoSize = true;
            this.element_metal_button.Location = new System.Drawing.Point(8, 133);
            this.element_metal_button.Name = "element_metal_button";
            this.element_metal_button.Size = new System.Drawing.Size(51, 17);
            this.element_metal_button.TabIndex = 5;
            this.element_metal_button.Text = "Metal";
            this.element_metal_button.UseVisualStyleBackColor = true;
            this.element_metal_button.CheckedChanged += new System.EventHandler(this.element_metal_button_CheckedChanged);
            // 
            // element_light_button
            // 
            this.element_light_button.AutoSize = true;
            this.element_light_button.Location = new System.Drawing.Point(8, 110);
            this.element_light_button.Name = "element_light_button";
            this.element_light_button.Size = new System.Drawing.Size(48, 17);
            this.element_light_button.TabIndex = 4;
            this.element_light_button.Text = "Light";
            this.element_light_button.UseVisualStyleBackColor = true;
            this.element_light_button.CheckedChanged += new System.EventHandler(this.element_light_button_CheckedChanged);
            // 
            // element_fire_button
            // 
            this.element_fire_button.AutoSize = true;
            this.element_fire_button.Location = new System.Drawing.Point(8, 87);
            this.element_fire_button.Name = "element_fire_button";
            this.element_fire_button.Size = new System.Drawing.Size(42, 17);
            this.element_fire_button.TabIndex = 3;
            this.element_fire_button.Text = "Fire";
            this.element_fire_button.UseVisualStyleBackColor = true;
            this.element_fire_button.CheckedChanged += new System.EventHandler(this.element_fire_button_CheckedChanged);
            // 
            // element_electric_button
            // 
            this.element_electric_button.AutoSize = true;
            this.element_electric_button.Location = new System.Drawing.Point(8, 64);
            this.element_electric_button.Name = "element_electric_button";
            this.element_electric_button.Size = new System.Drawing.Size(60, 17);
            this.element_electric_button.TabIndex = 2;
            this.element_electric_button.Text = "Electric";
            this.element_electric_button.UseVisualStyleBackColor = true;
            this.element_electric_button.CheckedChanged += new System.EventHandler(this.element_electric_button_CheckedChanged);
            // 
            // element_earth_button
            // 
            this.element_earth_button.AutoSize = true;
            this.element_earth_button.Location = new System.Drawing.Point(8, 41);
            this.element_earth_button.Name = "element_earth_button";
            this.element_earth_button.Size = new System.Drawing.Size(50, 17);
            this.element_earth_button.TabIndex = 1;
            this.element_earth_button.Text = "Earth";
            this.element_earth_button.UseVisualStyleBackColor = true;
            this.element_earth_button.CheckedChanged += new System.EventHandler(this.element_earth_button_CheckedChanged);
            // 
            // element_air_button
            // 
            this.element_air_button.AutoSize = true;
            this.element_air_button.Checked = true;
            this.element_air_button.Location = new System.Drawing.Point(8, 17);
            this.element_air_button.Name = "element_air_button";
            this.element_air_button.Size = new System.Drawing.Size(37, 17);
            this.element_air_button.TabIndex = 0;
            this.element_air_button.TabStop = true;
            this.element_air_button.Text = "Air";
            this.element_air_button.UseVisualStyleBackColor = true;
            this.element_air_button.CheckedChanged += new System.EventHandler(this.element_air_button_CheckedChanged);
            // 
            // save_element_button
            // 
            this.save_element_button.Location = new System.Drawing.Point(169, 293);
            this.save_element_button.Name = "save_element_button";
            this.save_element_button.Size = new System.Drawing.Size(84, 23);
            this.save_element_button.TabIndex = 32;
            this.save_element_button.Text = "Save Element";
            this.save_element_button.UseVisualStyleBackColor = true;
            this.save_element_button.Click += new System.EventHandler(this.save_element_button_Click);
            // 
            // elements_save_button
            // 
            this.elements_save_button.Location = new System.Drawing.Point(292, 340);
            this.elements_save_button.Name = "elements_save_button";
            this.elements_save_button.Size = new System.Drawing.Size(75, 23);
            this.elements_save_button.TabIndex = 31;
            this.elements_save_button.Text = "Save Stats";
            this.elements_save_button.UseVisualStyleBackColor = true;
            this.elements_save_button.Click += new System.EventHandler(this.elements_save_button_Click);
            // 
            // elements_load_button
            // 
            this.elements_load_button.Location = new System.Drawing.Point(45, 340);
            this.elements_load_button.Name = "elements_load_button";
            this.elements_load_button.Size = new System.Drawing.Size(75, 23);
            this.elements_load_button.TabIndex = 30;
            this.elements_load_button.Text = "Load Stats";
            this.elements_load_button.UseVisualStyleBackColor = true;
            this.elements_load_button.Click += new System.EventHandler(this.elements_load_button_Click);
            // 
            // elements_mp_num
            // 
            this.elements_mp_num.DecimalPlaces = 2;
            this.elements_mp_num.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.elements_mp_num.Location = new System.Drawing.Point(238, 258);
            this.elements_mp_num.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.elements_mp_num.Name = "elements_mp_num";
            this.elements_mp_num.Size = new System.Drawing.Size(47, 20);
            this.elements_mp_num.TabIndex = 29;
            this.elements_mp_num.Value = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            // 
            // elements_luck_num
            // 
            this.elements_luck_num.DecimalPlaces = 2;
            this.elements_luck_num.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.elements_luck_num.Location = new System.Drawing.Point(238, 203);
            this.elements_luck_num.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.elements_luck_num.Name = "elements_luck_num";
            this.elements_luck_num.Size = new System.Drawing.Size(47, 20);
            this.elements_luck_num.TabIndex = 28;
            this.elements_luck_num.Value = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            // 
            // elements_hp_num
            // 
            this.elements_hp_num.DecimalPlaces = 2;
            this.elements_hp_num.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.elements_hp_num.Location = new System.Drawing.Point(238, 232);
            this.elements_hp_num.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.elements_hp_num.Name = "elements_hp_num";
            this.elements_hp_num.Size = new System.Drawing.Size(47, 20);
            this.elements_hp_num.TabIndex = 27;
            this.elements_hp_num.Value = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            // 
            // elements_speed_num
            // 
            this.elements_speed_num.DecimalPlaces = 2;
            this.elements_speed_num.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.elements_speed_num.Location = new System.Drawing.Point(238, 177);
            this.elements_speed_num.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.elements_speed_num.Name = "elements_speed_num";
            this.elements_speed_num.Size = new System.Drawing.Size(47, 20);
            this.elements_speed_num.TabIndex = 26;
            this.elements_speed_num.Value = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            // 
            // elements_insight_num
            // 
            this.elements_insight_num.DecimalPlaces = 2;
            this.elements_insight_num.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.elements_insight_num.Location = new System.Drawing.Point(238, 122);
            this.elements_insight_num.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.elements_insight_num.Name = "elements_insight_num";
            this.elements_insight_num.Size = new System.Drawing.Size(47, 20);
            this.elements_insight_num.TabIndex = 25;
            this.elements_insight_num.Value = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            // 
            // elements_dexterity_num
            // 
            this.elements_dexterity_num.DecimalPlaces = 2;
            this.elements_dexterity_num.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.elements_dexterity_num.Location = new System.Drawing.Point(238, 151);
            this.elements_dexterity_num.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.elements_dexterity_num.Name = "elements_dexterity_num";
            this.elements_dexterity_num.Size = new System.Drawing.Size(47, 20);
            this.elements_dexterity_num.TabIndex = 24;
            this.elements_dexterity_num.Value = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            // 
            // elements_willpower_num
            // 
            this.elements_willpower_num.DecimalPlaces = 2;
            this.elements_willpower_num.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.elements_willpower_num.Location = new System.Drawing.Point(238, 96);
            this.elements_willpower_num.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.elements_willpower_num.Name = "elements_willpower_num";
            this.elements_willpower_num.Size = new System.Drawing.Size(47, 20);
            this.elements_willpower_num.TabIndex = 23;
            this.elements_willpower_num.Value = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            // 
            // elements_stamina_num
            // 
            this.elements_stamina_num.DecimalPlaces = 2;
            this.elements_stamina_num.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.elements_stamina_num.Location = new System.Drawing.Point(238, 41);
            this.elements_stamina_num.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.elements_stamina_num.Name = "elements_stamina_num";
            this.elements_stamina_num.Size = new System.Drawing.Size(47, 20);
            this.elements_stamina_num.TabIndex = 22;
            this.elements_stamina_num.Value = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            // 
            // elements_spirit_num
            // 
            this.elements_spirit_num.DecimalPlaces = 2;
            this.elements_spirit_num.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.elements_spirit_num.Location = new System.Drawing.Point(238, 70);
            this.elements_spirit_num.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.elements_spirit_num.Name = "elements_spirit_num";
            this.elements_spirit_num.Size = new System.Drawing.Size(47, 20);
            this.elements_spirit_num.TabIndex = 21;
            this.elements_spirit_num.Value = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            // 
            // elements_strength_num
            // 
            this.elements_strength_num.DecimalPlaces = 2;
            this.elements_strength_num.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.elements_strength_num.Location = new System.Drawing.Point(238, 15);
            this.elements_strength_num.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.elements_strength_num.Name = "elements_strength_num";
            this.elements_strength_num.Size = new System.Drawing.Size(47, 20);
            this.elements_strength_num.TabIndex = 20;
            this.elements_strength_num.Value = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(119, 261);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Magic Points";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(119, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Health Points";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(119, 207);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Luck";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(119, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Speed";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(119, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Dexterity";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(119, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Insight";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(119, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Willpower";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(119, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Spirit";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(119, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Stamina";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(119, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Strength";
            // 
            // statusStrip2
            // 
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2});
            this.statusStrip2.Location = new System.Drawing.Point(3, 426);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(645, 22);
            this.statusStrip2.TabIndex = 0;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(64, 17);
            this.toolStripStatusLabel2.Text = "Hi Markus!";
            // 
            // Familiars
            // 
            this.Familiars.Controls.Add(this.save_familiars);
            this.Familiars.Controls.Add(this.effect_three_damage_two_label);
            this.Familiars.Controls.Add(this.effect_three_damage_one_label);
            this.Familiars.Controls.Add(this.effect_three_duration_label);
            this.Familiars.Controls.Add(this.effect_three_name_label);
            this.Familiars.Controls.Add(this.label48);
            this.Familiars.Controls.Add(this.label49);
            this.Familiars.Controls.Add(this.label50);
            this.Familiars.Controls.Add(this.label51);
            this.Familiars.Controls.Add(this.effect_two_damage_two_label);
            this.Familiars.Controls.Add(this.effect_two_damage_one_label);
            this.Familiars.Controls.Add(this.effect_two_duration_label);
            this.Familiars.Controls.Add(this.effect_two_name_label);
            this.Familiars.Controls.Add(this.label29);
            this.Familiars.Controls.Add(this.label30);
            this.Familiars.Controls.Add(this.label31);
            this.Familiars.Controls.Add(this.label_1);
            this.Familiars.Controls.Add(this.animation_label);
            this.Familiars.Controls.Add(this.distance_label);
            this.Familiars.Controls.Add(this.mp_cost_label);
            this.Familiars.Controls.Add(this.label34);
            this.Familiars.Controls.Add(this.label45);
            this.Familiars.Controls.Add(this.label46);
            this.Familiars.Controls.Add(this.effect_one_damage_two_label);
            this.Familiars.Controls.Add(this.hit_chance_label);
            this.Familiars.Controls.Add(this.effect_one_damage_one_label);
            this.Familiars.Controls.Add(this.damage_label);
            this.Familiars.Controls.Add(this.effect_one_duration_label);
            this.Familiars.Controls.Add(this.cooldown_label);
            this.Familiars.Controls.Add(this.effect_one_name_label);
            this.Familiars.Controls.Add(this.crit_chance_label);
            this.Familiars.Controls.Add(this.label35);
            this.Familiars.Controls.Add(this.label36);
            this.Familiars.Controls.Add(this.label37);
            this.Familiars.Controls.Add(this.label38);
            this.Familiars.Controls.Add(this.label39);
            this.Familiars.Controls.Add(this.label40);
            this.Familiars.Controls.Add(this.label41);
            this.Familiars.Controls.Add(this.label43);
            this.Familiars.Controls.Add(this.attack_drop_down);
            this.Familiars.Controls.Add(this.element_name_label);
            this.Familiars.Controls.Add(this.sub_breed_name_label);
            this.Familiars.Controls.Add(this.main_breed_name_label);
            this.Familiars.Controls.Add(this.name_label);
            this.Familiars.Controls.Add(this.label24);
            this.Familiars.Controls.Add(this.label23);
            this.Familiars.Controls.Add(this.label22);
            this.Familiars.Controls.Add(this.label21);
            this.Familiars.Controls.Add(this.mp_num);
            this.Familiars.Controls.Add(this.hp_num);
            this.Familiars.Controls.Add(this.luck_num);
            this.Familiars.Controls.Add(this.speed_num);
            this.Familiars.Controls.Add(this.dexterity_num);
            this.Familiars.Controls.Add(this.insight_num);
            this.Familiars.Controls.Add(this.willpower_num);
            this.Familiars.Controls.Add(this.spirit_num);
            this.Familiars.Controls.Add(this.stamina_num);
            this.Familiars.Controls.Add(this.strength_num);
            this.Familiars.Controls.Add(this.label11);
            this.Familiars.Controls.Add(this.label12);
            this.Familiars.Controls.Add(this.label13);
            this.Familiars.Controls.Add(this.label14);
            this.Familiars.Controls.Add(this.label15);
            this.Familiars.Controls.Add(this.label16);
            this.Familiars.Controls.Add(this.label17);
            this.Familiars.Controls.Add(this.label18);
            this.Familiars.Controls.Add(this.label19);
            this.Familiars.Controls.Add(this.label20);
            this.Familiars.Controls.Add(this.FamiliarDropDown);
            this.Familiars.Controls.Add(this.statusStrip3);
            this.Familiars.Location = new System.Drawing.Point(4, 25);
            this.Familiars.Name = "Familiars";
            this.Familiars.Size = new System.Drawing.Size(651, 451);
            this.Familiars.TabIndex = 2;
            this.Familiars.Text = "Familiars";
            this.Familiars.UseVisualStyleBackColor = true;
            // 
            // save_familiars
            // 
            this.save_familiars.Location = new System.Drawing.Point(404, 82);
            this.save_familiars.Name = "save_familiars";
            this.save_familiars.Size = new System.Drawing.Size(142, 35);
            this.save_familiars.TabIndex = 91;
            this.save_familiars.Text = "Save Familiars";
            this.save_familiars.UseVisualStyleBackColor = true;
            this.save_familiars.Click += new System.EventHandler(this.save_familiars_Click);
            // 
            // effect_three_damage_two_label
            // 
            this.effect_three_damage_two_label.AutoSize = true;
            this.effect_three_damage_two_label.Location = new System.Drawing.Point(496, 367);
            this.effect_three_damage_two_label.Name = "effect_three_damage_two_label";
            this.effect_three_damage_two_label.Size = new System.Drawing.Size(13, 13);
            this.effect_three_damage_two_label.TabIndex = 90;
            this.effect_three_damage_two_label.Text = "0";
            // 
            // effect_three_damage_one_label
            // 
            this.effect_three_damage_one_label.AutoSize = true;
            this.effect_three_damage_one_label.Location = new System.Drawing.Point(496, 344);
            this.effect_three_damage_one_label.Name = "effect_three_damage_one_label";
            this.effect_three_damage_one_label.Size = new System.Drawing.Size(13, 13);
            this.effect_three_damage_one_label.TabIndex = 89;
            this.effect_three_damage_one_label.Text = "0";
            // 
            // effect_three_duration_label
            // 
            this.effect_three_duration_label.AutoSize = true;
            this.effect_three_duration_label.Location = new System.Drawing.Point(500, 322);
            this.effect_three_duration_label.Name = "effect_three_duration_label";
            this.effect_three_duration_label.Size = new System.Drawing.Size(13, 13);
            this.effect_three_duration_label.TabIndex = 88;
            this.effect_three_duration_label.Text = "0";
            // 
            // effect_three_name_label
            // 
            this.effect_three_name_label.AutoSize = true;
            this.effect_three_name_label.Location = new System.Drawing.Point(507, 294);
            this.effect_three_name_label.Name = "effect_three_name_label";
            this.effect_three_name_label.Size = new System.Drawing.Size(13, 13);
            this.effect_three_name_label.TabIndex = 87;
            this.effect_three_name_label.Text = "0";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(443, 367);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(47, 13);
            this.label48.TabIndex = 86;
            this.label48.Text = "Damage";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(443, 344);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(47, 13);
            this.label49.TabIndex = 85;
            this.label49.Text = "Damage";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(443, 321);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(47, 13);
            this.label50.TabIndex = 84;
            this.label50.Text = "Duration";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(443, 294);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(66, 13);
            this.label51.TabIndex = 83;
            this.label51.Text = "Effect Three";
            // 
            // effect_two_damage_two_label
            // 
            this.effect_two_damage_two_label.AutoSize = true;
            this.effect_two_damage_two_label.Location = new System.Drawing.Point(357, 367);
            this.effect_two_damage_two_label.Name = "effect_two_damage_two_label";
            this.effect_two_damage_two_label.Size = new System.Drawing.Size(13, 13);
            this.effect_two_damage_two_label.TabIndex = 82;
            this.effect_two_damage_two_label.Text = "0";
            // 
            // effect_two_damage_one_label
            // 
            this.effect_two_damage_one_label.AutoSize = true;
            this.effect_two_damage_one_label.Location = new System.Drawing.Point(357, 344);
            this.effect_two_damage_one_label.Name = "effect_two_damage_one_label";
            this.effect_two_damage_one_label.Size = new System.Drawing.Size(13, 13);
            this.effect_two_damage_one_label.TabIndex = 81;
            this.effect_two_damage_one_label.Text = "0";
            // 
            // effect_two_duration_label
            // 
            this.effect_two_duration_label.AutoSize = true;
            this.effect_two_duration_label.Location = new System.Drawing.Point(361, 322);
            this.effect_two_duration_label.Name = "effect_two_duration_label";
            this.effect_two_duration_label.Size = new System.Drawing.Size(13, 13);
            this.effect_two_duration_label.TabIndex = 80;
            this.effect_two_duration_label.Text = "0";
            // 
            // effect_two_name_label
            // 
            this.effect_two_name_label.AutoSize = true;
            this.effect_two_name_label.Location = new System.Drawing.Point(368, 294);
            this.effect_two_name_label.Name = "effect_two_name_label";
            this.effect_two_name_label.Size = new System.Drawing.Size(13, 13);
            this.effect_two_name_label.TabIndex = 79;
            this.effect_two_name_label.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(304, 367);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(47, 13);
            this.label29.TabIndex = 78;
            this.label29.Text = "Damage";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(304, 344);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(47, 13);
            this.label30.TabIndex = 77;
            this.label30.Text = "Damage";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(304, 321);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(47, 13);
            this.label31.TabIndex = 76;
            this.label31.Text = "Duration";
            // 
            // label_1
            // 
            this.label_1.AutoSize = true;
            this.label_1.Location = new System.Drawing.Point(304, 294);
            this.label_1.Name = "label_1";
            this.label_1.Size = new System.Drawing.Size(59, 13);
            this.label_1.TabIndex = 75;
            this.label_1.Text = "Effect Two";
            // 
            // animation_label
            // 
            this.animation_label.AutoSize = true;
            this.animation_label.Location = new System.Drawing.Point(416, 252);
            this.animation_label.Name = "animation_label";
            this.animation_label.Size = new System.Drawing.Size(13, 13);
            this.animation_label.TabIndex = 74;
            this.animation_label.Text = "0";
            // 
            // distance_label
            // 
            this.distance_label.AutoSize = true;
            this.distance_label.Location = new System.Drawing.Point(245, 252);
            this.distance_label.Name = "distance_label";
            this.distance_label.Size = new System.Drawing.Size(13, 13);
            this.distance_label.TabIndex = 73;
            this.distance_label.Text = "0";
            // 
            // mp_cost_label
            // 
            this.mp_cost_label.AutoSize = true;
            this.mp_cost_label.Location = new System.Drawing.Point(76, 389);
            this.mp_cost_label.Name = "mp_cost_label";
            this.mp_cost_label.Size = new System.Drawing.Size(13, 13);
            this.mp_cost_label.TabIndex = 72;
            this.mp_cost_label.Text = "0";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(8, 389);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(47, 13);
            this.label34.TabIndex = 71;
            this.label34.Text = "MP Cost";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(357, 252);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(53, 13);
            this.label45.TabIndex = 70;
            this.label45.Text = "Animation";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(190, 252);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(49, 13);
            this.label46.TabIndex = 69;
            this.label46.Text = "Distance";
            // 
            // effect_one_damage_two_label
            // 
            this.effect_one_damage_two_label.AutoSize = true;
            this.effect_one_damage_two_label.Location = new System.Drawing.Point(216, 367);
            this.effect_one_damage_two_label.Name = "effect_one_damage_two_label";
            this.effect_one_damage_two_label.Size = new System.Drawing.Size(13, 13);
            this.effect_one_damage_two_label.TabIndex = 68;
            this.effect_one_damage_two_label.Text = "0";
            // 
            // hit_chance_label
            // 
            this.hit_chance_label.AutoSize = true;
            this.hit_chance_label.Location = new System.Drawing.Point(68, 344);
            this.hit_chance_label.Name = "hit_chance_label";
            this.hit_chance_label.Size = new System.Drawing.Size(13, 13);
            this.hit_chance_label.TabIndex = 67;
            this.hit_chance_label.Text = "0";
            // 
            // effect_one_damage_one_label
            // 
            this.effect_one_damage_one_label.AutoSize = true;
            this.effect_one_damage_one_label.Location = new System.Drawing.Point(216, 344);
            this.effect_one_damage_one_label.Name = "effect_one_damage_one_label";
            this.effect_one_damage_one_label.Size = new System.Drawing.Size(13, 13);
            this.effect_one_damage_one_label.TabIndex = 66;
            this.effect_one_damage_one_label.Text = "0";
            // 
            // damage_label
            // 
            this.damage_label.AutoSize = true;
            this.damage_label.Location = new System.Drawing.Point(61, 317);
            this.damage_label.Name = "damage_label";
            this.damage_label.Size = new System.Drawing.Size(13, 13);
            this.damage_label.TabIndex = 65;
            this.damage_label.Text = "0";
            // 
            // effect_one_duration_label
            // 
            this.effect_one_duration_label.AutoSize = true;
            this.effect_one_duration_label.Location = new System.Drawing.Point(220, 322);
            this.effect_one_duration_label.Name = "effect_one_duration_label";
            this.effect_one_duration_label.Size = new System.Drawing.Size(13, 13);
            this.effect_one_duration_label.TabIndex = 64;
            this.effect_one_duration_label.Text = "0";
            // 
            // cooldown_label
            // 
            this.cooldown_label.AutoSize = true;
            this.cooldown_label.Location = new System.Drawing.Point(68, 294);
            this.cooldown_label.Name = "cooldown_label";
            this.cooldown_label.Size = new System.Drawing.Size(13, 13);
            this.cooldown_label.TabIndex = 63;
            this.cooldown_label.Text = "0";
            // 
            // effect_one_name_label
            // 
            this.effect_one_name_label.AutoSize = true;
            this.effect_one_name_label.Location = new System.Drawing.Point(227, 294);
            this.effect_one_name_label.Name = "effect_one_name_label";
            this.effect_one_name_label.Size = new System.Drawing.Size(13, 13);
            this.effect_one_name_label.TabIndex = 62;
            this.effect_one_name_label.Text = "0";
            // 
            // crit_chance_label
            // 
            this.crit_chance_label.AutoSize = true;
            this.crit_chance_label.Location = new System.Drawing.Point(76, 366);
            this.crit_chance_label.Name = "crit_chance_label";
            this.crit_chance_label.Size = new System.Drawing.Size(13, 13);
            this.crit_chance_label.TabIndex = 60;
            this.crit_chance_label.Text = "0";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(163, 367);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(47, 13);
            this.label35.TabIndex = 58;
            this.label35.Text = "Damage";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(8, 344);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(60, 13);
            this.label36.TabIndex = 57;
            this.label36.Text = "Hit Chance";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(163, 344);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(47, 13);
            this.label37.TabIndex = 56;
            this.label37.Text = "Damage";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(8, 317);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(47, 13);
            this.label38.TabIndex = 55;
            this.label38.Text = "Damage";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(163, 321);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(47, 13);
            this.label39.TabIndex = 54;
            this.label39.Text = "Duration";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(8, 294);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(54, 13);
            this.label40.TabIndex = 53;
            this.label40.Text = "Cooldown";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(163, 294);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(58, 13);
            this.label41.TabIndex = 52;
            this.label41.Text = "Effect One";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(8, 366);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(62, 13);
            this.label43.TabIndex = 50;
            this.label43.Text = "Crit Chance";
            // 
            // attack_drop_down
            // 
            this.attack_drop_down.FormattingEnabled = true;
            this.attack_drop_down.Location = new System.Drawing.Point(8, 249);
            this.attack_drop_down.Name = "attack_drop_down";
            this.attack_drop_down.Size = new System.Drawing.Size(121, 21);
            this.attack_drop_down.TabIndex = 48;
            this.attack_drop_down.SelectedIndexChanged += new System.EventHandler(this.attack_drop_down_SelectedIndexChanged);
            // 
            // element_name_label
            // 
            this.element_name_label.AutoSize = true;
            this.element_name_label.Location = new System.Drawing.Point(163, 126);
            this.element_name_label.Name = "element_name_label";
            this.element_name_label.Size = new System.Drawing.Size(76, 13);
            this.element_name_label.TabIndex = 47;
            this.element_name_label.Text = "Element Name";
            // 
            // sub_breed_name_label
            // 
            this.sub_breed_name_label.AutoSize = true;
            this.sub_breed_name_label.Location = new System.Drawing.Point(163, 104);
            this.sub_breed_name_label.Name = "sub_breed_name_label";
            this.sub_breed_name_label.Size = new System.Drawing.Size(88, 13);
            this.sub_breed_name_label.TabIndex = 46;
            this.sub_breed_name_label.Text = "Sub Breed Name";
            // 
            // main_breed_name_label
            // 
            this.main_breed_name_label.AutoSize = true;
            this.main_breed_name_label.Location = new System.Drawing.Point(163, 82);
            this.main_breed_name_label.Name = "main_breed_name_label";
            this.main_breed_name_label.Size = new System.Drawing.Size(92, 13);
            this.main_breed_name_label.TabIndex = 45;
            this.main_breed_name_label.Text = "Main Breed Name";
            // 
            // name_label
            // 
            this.name_label.AutoSize = true;
            this.name_label.Location = new System.Drawing.Point(163, 60);
            this.name_label.Name = "name_label";
            this.name_label.Size = new System.Drawing.Size(73, 13);
            this.name_label.TabIndex = 44;
            this.name_label.Text = "Familiar Name";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(8, 126);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 13);
            this.label24.TabIndex = 43;
            this.label24.Text = "Element";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(8, 104);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 13);
            this.label23.TabIndex = 42;
            this.label23.Text = "Sub Breed";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 82);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 13);
            this.label22.TabIndex = 41;
            this.label22.Text = "Main Breed";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(8, 60);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 13);
            this.label21.TabIndex = 40;
            this.label21.Text = "Name";
            // 
            // mp_num
            // 
            this.mp_num.AutoSize = true;
            this.mp_num.Location = new System.Drawing.Point(531, 196);
            this.mp_num.Name = "mp_num";
            this.mp_num.Size = new System.Drawing.Size(13, 13);
            this.mp_num.TabIndex = 39;
            this.mp_num.Text = "0";
            // 
            // hp_num
            // 
            this.hp_num.AutoSize = true;
            this.hp_num.Location = new System.Drawing.Point(533, 169);
            this.hp_num.Name = "hp_num";
            this.hp_num.Size = new System.Drawing.Size(13, 13);
            this.hp_num.TabIndex = 38;
            this.hp_num.Text = "0";
            // 
            // luck_num
            // 
            this.luck_num.AutoSize = true;
            this.luck_num.Location = new System.Drawing.Point(401, 197);
            this.luck_num.Name = "luck_num";
            this.luck_num.Size = new System.Drawing.Size(13, 13);
            this.luck_num.TabIndex = 37;
            this.luck_num.Text = "0";
            // 
            // speed_num
            // 
            this.speed_num.AutoSize = true;
            this.speed_num.Location = new System.Drawing.Point(401, 169);
            this.speed_num.Name = "speed_num";
            this.speed_num.Size = new System.Drawing.Size(13, 13);
            this.speed_num.TabIndex = 36;
            this.speed_num.Text = "0";
            // 
            // dexterity_num
            // 
            this.dexterity_num.AutoSize = true;
            this.dexterity_num.Location = new System.Drawing.Point(304, 196);
            this.dexterity_num.Name = "dexterity_num";
            this.dexterity_num.Size = new System.Drawing.Size(13, 13);
            this.dexterity_num.TabIndex = 35;
            this.dexterity_num.Text = "0";
            // 
            // insight_num
            // 
            this.insight_num.AutoSize = true;
            this.insight_num.Location = new System.Drawing.Point(294, 169);
            this.insight_num.Name = "insight_num";
            this.insight_num.Size = new System.Drawing.Size(13, 13);
            this.insight_num.TabIndex = 34;
            this.insight_num.Text = "0";
            // 
            // willpower_num
            // 
            this.willpower_num.AutoSize = true;
            this.willpower_num.Location = new System.Drawing.Point(185, 196);
            this.willpower_num.Name = "willpower_num";
            this.willpower_num.Size = new System.Drawing.Size(13, 13);
            this.willpower_num.TabIndex = 33;
            this.willpower_num.Text = "0";
            // 
            // spirit_num
            // 
            this.spirit_num.AutoSize = true;
            this.spirit_num.Location = new System.Drawing.Point(166, 169);
            this.spirit_num.Name = "spirit_num";
            this.spirit_num.Size = new System.Drawing.Size(13, 13);
            this.spirit_num.TabIndex = 32;
            this.spirit_num.Text = "0";
            // 
            // stamina_num
            // 
            this.stamina_num.AutoSize = true;
            this.stamina_num.Location = new System.Drawing.Point(59, 196);
            this.stamina_num.Name = "stamina_num";
            this.stamina_num.Size = new System.Drawing.Size(13, 13);
            this.stamina_num.TabIndex = 31;
            this.stamina_num.Text = "0";
            // 
            // strength_num
            // 
            this.strength_num.AutoSize = true;
            this.strength_num.Location = new System.Drawing.Point(61, 169);
            this.strength_num.Name = "strength_num";
            this.strength_num.Size = new System.Drawing.Size(13, 13);
            this.strength_num.TabIndex = 30;
            this.strength_num.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(457, 196);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Magic Points";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(457, 169);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Health Points";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(357, 196);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "Luck";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(357, 169);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Speed";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(250, 196);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "Dexterity";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(250, 169);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 13);
            this.label16.TabIndex = 24;
            this.label16.Text = "Insight";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(126, 196);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "Willpower";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(126, 169);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(30, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "Spirit";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(8, 197);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 13);
            this.label19.TabIndex = 21;
            this.label19.Text = "Stamina";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(8, 169);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 13);
            this.label20.TabIndex = 20;
            this.label20.Text = "Strength";
            // 
            // FamiliarDropDown
            // 
            this.FamiliarDropDown.FormattingEnabled = true;
            this.FamiliarDropDown.Location = new System.Drawing.Point(8, 18);
            this.FamiliarDropDown.Name = "FamiliarDropDown";
            this.FamiliarDropDown.Size = new System.Drawing.Size(121, 21);
            this.FamiliarDropDown.TabIndex = 1;
            this.FamiliarDropDown.SelectedIndexChanged += new System.EventHandler(this.FamiliarDropDown_SelectedIndexChanged);
            // 
            // statusStrip3
            // 
            this.statusStrip3.Location = new System.Drawing.Point(0, 429);
            this.statusStrip3.Name = "statusStrip3";
            this.statusStrip3.Size = new System.Drawing.Size(651, 22);
            this.statusStrip3.TabIndex = 0;
            this.statusStrip3.Text = "statusStrip3";
            // 
            // MakeFamiliar
            // 
            this.MakeFamiliar.Controls.Add(this.groupBox5);
            this.MakeFamiliar.Controls.Add(this.panel3);
            this.MakeFamiliar.Controls.Add(this.panel2);
            this.MakeFamiliar.Controls.Add(this.statusStrip4);
            this.MakeFamiliar.Location = new System.Drawing.Point(4, 25);
            this.MakeFamiliar.Name = "MakeFamiliar";
            this.MakeFamiliar.Size = new System.Drawing.Size(651, 451);
            this.MakeFamiliar.TabIndex = 3;
            this.MakeFamiliar.Text = "Make Familiar";
            this.MakeFamiliar.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.threadLCK);
            this.groupBox5.Controls.Add(this.threadSPD);
            this.groupBox5.Controls.Add(this.threadDEX);
            this.groupBox5.Controls.Add(this.threadINS);
            this.groupBox5.Controls.Add(this.threadWIL);
            this.groupBox5.Controls.Add(this.threadSPR);
            this.groupBox5.Controls.Add(this.threadSTA);
            this.groupBox5.Controls.Add(this.threadSTR);
            this.groupBox5.Controls.Add(this.threadLCKSR);
            this.groupBox5.Controls.Add(this.threadLCKSD);
            this.groupBox5.Controls.Add(this.threadLCKMR);
            this.groupBox5.Controls.Add(this.threadLCKMD);
            this.groupBox5.Controls.Add(this.threadSPDSR);
            this.groupBox5.Controls.Add(this.threadSPDSD);
            this.groupBox5.Controls.Add(this.threadSPDMR);
            this.groupBox5.Controls.Add(this.threadSPDMD);
            this.groupBox5.Controls.Add(this.threadDEXSR);
            this.groupBox5.Controls.Add(this.threadDEXSD);
            this.groupBox5.Controls.Add(this.threadDEXMR);
            this.groupBox5.Controls.Add(this.threadDEXMD);
            this.groupBox5.Controls.Add(this.threadINSSR);
            this.groupBox5.Controls.Add(this.threadINSSD);
            this.groupBox5.Controls.Add(this.threadINSMR);
            this.groupBox5.Controls.Add(this.threadINSMD);
            this.groupBox5.Controls.Add(this.label72);
            this.groupBox5.Controls.Add(this.label73);
            this.groupBox5.Controls.Add(this.label74);
            this.groupBox5.Controls.Add(this.label75);
            this.groupBox5.Controls.Add(this.label76);
            this.groupBox5.Controls.Add(this.threadWILSR);
            this.groupBox5.Controls.Add(this.threadWILSD);
            this.groupBox5.Controls.Add(this.threadWILMR);
            this.groupBox5.Controls.Add(this.threadWILMD);
            this.groupBox5.Controls.Add(this.threadSPRSR);
            this.groupBox5.Controls.Add(this.threadSPRSD);
            this.groupBox5.Controls.Add(this.threadSPRMR);
            this.groupBox5.Controls.Add(this.threadSPRMD);
            this.groupBox5.Controls.Add(this.threadSTASR);
            this.groupBox5.Controls.Add(this.threadSTASD);
            this.groupBox5.Controls.Add(this.threadSTAMR);
            this.groupBox5.Controls.Add(this.threadSTAMD);
            this.groupBox5.Controls.Add(this.threadSTRSR);
            this.groupBox5.Controls.Add(this.threadSTRSD);
            this.groupBox5.Controls.Add(this.threadSTRMR);
            this.groupBox5.Controls.Add(this.threadSTRMD);
            this.groupBox5.Controls.Add(this.label59);
            this.groupBox5.Controls.Add(this.label70);
            this.groupBox5.Controls.Add(this.label63);
            this.groupBox5.Controls.Add(this.label71);
            this.groupBox5.Controls.Add(this.label60);
            this.groupBox5.Controls.Add(this.label68);
            this.groupBox5.Controls.Add(this.label67);
            this.groupBox5.Controls.Add(this.label69);
            this.groupBox5.Controls.Add(this.label61);
            this.groupBox5.Controls.Add(this.label66);
            this.groupBox5.Controls.Add(this.label62);
            this.groupBox5.Controls.Add(this.label65);
            this.groupBox5.Controls.Add(this.label64);
            this.groupBox5.Location = new System.Drawing.Point(11, 284);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(446, 142);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Threads";
            // 
            // threadLCK
            // 
            this.threadLCK.Location = new System.Drawing.Point(407, 118);
            this.threadLCK.Name = "threadLCK";
            this.threadLCK.Size = new System.Drawing.Size(31, 13);
            this.threadLCK.TabIndex = 66;
            this.threadLCK.Text = "0000";
            // 
            // threadSPD
            // 
            this.threadSPD.Location = new System.Drawing.Point(407, 93);
            this.threadSPD.Name = "threadSPD";
            this.threadSPD.Size = new System.Drawing.Size(31, 13);
            this.threadSPD.TabIndex = 65;
            this.threadSPD.Text = "0000";
            // 
            // threadDEX
            // 
            this.threadDEX.Location = new System.Drawing.Point(407, 67);
            this.threadDEX.Name = "threadDEX";
            this.threadDEX.Size = new System.Drawing.Size(31, 13);
            this.threadDEX.TabIndex = 64;
            this.threadDEX.Text = "0000";
            // 
            // threadINS
            // 
            this.threadINS.Location = new System.Drawing.Point(407, 41);
            this.threadINS.Name = "threadINS";
            this.threadINS.Size = new System.Drawing.Size(31, 13);
            this.threadINS.TabIndex = 63;
            this.threadINS.Text = "0000";
            // 
            // threadWIL
            // 
            this.threadWIL.Location = new System.Drawing.Point(183, 118);
            this.threadWIL.Name = "threadWIL";
            this.threadWIL.Size = new System.Drawing.Size(31, 13);
            this.threadWIL.TabIndex = 62;
            this.threadWIL.Text = "0000";
            // 
            // threadSPR
            // 
            this.threadSPR.Location = new System.Drawing.Point(183, 93);
            this.threadSPR.Name = "threadSPR";
            this.threadSPR.Size = new System.Drawing.Size(31, 13);
            this.threadSPR.TabIndex = 61;
            this.threadSPR.Text = "0000";
            // 
            // threadSTA
            // 
            this.threadSTA.Location = new System.Drawing.Point(183, 67);
            this.threadSTA.Name = "threadSTA";
            this.threadSTA.Size = new System.Drawing.Size(31, 13);
            this.threadSTA.TabIndex = 60;
            this.threadSTA.Text = "0000";
            // 
            // threadSTR
            // 
            this.threadSTR.Location = new System.Drawing.Point(183, 41);
            this.threadSTR.Name = "threadSTR";
            this.threadSTR.Size = new System.Drawing.Size(31, 13);
            this.threadSTR.TabIndex = 59;
            this.threadSTR.Text = "0000";
            // 
            // threadLCKSR
            // 
            this.threadLCKSR.AutoSize = true;
            this.threadLCKSR.Location = new System.Drawing.Point(377, 118);
            this.threadLCKSR.Name = "threadLCKSR";
            this.threadLCKSR.Size = new System.Drawing.Size(14, 13);
            this.threadLCKSR.TabIndex = 58;
            this.threadLCKSR.TabStop = true;
            this.threadLCKSR.UseVisualStyleBackColor = true;
            this.threadLCKSR.CheckedChanged += new System.EventHandler(this.threadLCKSR_CheckedChanged);
            // 
            // threadLCKSD
            // 
            this.threadLCKSD.AutoSize = true;
            this.threadLCKSD.Location = new System.Drawing.Point(346, 118);
            this.threadLCKSD.Name = "threadLCKSD";
            this.threadLCKSD.Size = new System.Drawing.Size(14, 13);
            this.threadLCKSD.TabIndex = 57;
            this.threadLCKSD.TabStop = true;
            this.threadLCKSD.UseVisualStyleBackColor = true;
            this.threadLCKSD.CheckedChanged += new System.EventHandler(this.threadLCKSD_CheckedChanged);
            // 
            // threadLCKMR
            // 
            this.threadLCKMR.AutoSize = true;
            this.threadLCKMR.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.threadLCKMR.Location = new System.Drawing.Point(316, 118);
            this.threadLCKMR.Name = "threadLCKMR";
            this.threadLCKMR.Size = new System.Drawing.Size(14, 13);
            this.threadLCKMR.TabIndex = 56;
            this.threadLCKMR.TabStop = true;
            this.threadLCKMR.UseVisualStyleBackColor = true;
            this.threadLCKMR.CheckedChanged += new System.EventHandler(this.threadLCKMR_CheckedChanged);
            // 
            // threadLCKMD
            // 
            this.threadLCKMD.AutoSize = true;
            this.threadLCKMD.Location = new System.Drawing.Point(286, 118);
            this.threadLCKMD.Name = "threadLCKMD";
            this.threadLCKMD.Size = new System.Drawing.Size(14, 13);
            this.threadLCKMD.TabIndex = 55;
            this.threadLCKMD.TabStop = true;
            this.threadLCKMD.UseVisualStyleBackColor = true;
            this.threadLCKMD.CheckedChanged += new System.EventHandler(this.threadLCKMD_CheckedChanged);
            // 
            // threadSPDSR
            // 
            this.threadSPDSR.AutoSize = true;
            this.threadSPDSR.Location = new System.Drawing.Point(377, 93);
            this.threadSPDSR.Name = "threadSPDSR";
            this.threadSPDSR.Size = new System.Drawing.Size(14, 13);
            this.threadSPDSR.TabIndex = 54;
            this.threadSPDSR.TabStop = true;
            this.threadSPDSR.UseVisualStyleBackColor = true;
            this.threadSPDSR.CheckedChanged += new System.EventHandler(this.threadSPDSR_CheckedChanged);
            // 
            // threadSPDSD
            // 
            this.threadSPDSD.AutoSize = true;
            this.threadSPDSD.Location = new System.Drawing.Point(346, 93);
            this.threadSPDSD.Name = "threadSPDSD";
            this.threadSPDSD.Size = new System.Drawing.Size(14, 13);
            this.threadSPDSD.TabIndex = 53;
            this.threadSPDSD.TabStop = true;
            this.threadSPDSD.UseVisualStyleBackColor = true;
            this.threadSPDSD.CheckedChanged += new System.EventHandler(this.threadSPDSD_CheckedChanged);
            // 
            // threadSPDMR
            // 
            this.threadSPDMR.AutoSize = true;
            this.threadSPDMR.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.threadSPDMR.Location = new System.Drawing.Point(316, 93);
            this.threadSPDMR.Name = "threadSPDMR";
            this.threadSPDMR.Size = new System.Drawing.Size(14, 13);
            this.threadSPDMR.TabIndex = 52;
            this.threadSPDMR.TabStop = true;
            this.threadSPDMR.UseVisualStyleBackColor = true;
            this.threadSPDMR.CheckedChanged += new System.EventHandler(this.threadSPDMR_CheckedChanged);
            // 
            // threadSPDMD
            // 
            this.threadSPDMD.AutoSize = true;
            this.threadSPDMD.Location = new System.Drawing.Point(286, 93);
            this.threadSPDMD.Name = "threadSPDMD";
            this.threadSPDMD.Size = new System.Drawing.Size(14, 13);
            this.threadSPDMD.TabIndex = 51;
            this.threadSPDMD.TabStop = true;
            this.threadSPDMD.UseVisualStyleBackColor = true;
            this.threadSPDMD.CheckedChanged += new System.EventHandler(this.threadSPDMD_CheckedChanged);
            // 
            // threadDEXSR
            // 
            this.threadDEXSR.AutoSize = true;
            this.threadDEXSR.Location = new System.Drawing.Point(377, 67);
            this.threadDEXSR.Name = "threadDEXSR";
            this.threadDEXSR.Size = new System.Drawing.Size(14, 13);
            this.threadDEXSR.TabIndex = 50;
            this.threadDEXSR.TabStop = true;
            this.threadDEXSR.UseVisualStyleBackColor = true;
            this.threadDEXSR.CheckedChanged += new System.EventHandler(this.threadDEXSR_CheckedChanged);
            // 
            // threadDEXSD
            // 
            this.threadDEXSD.AutoSize = true;
            this.threadDEXSD.Location = new System.Drawing.Point(346, 67);
            this.threadDEXSD.Name = "threadDEXSD";
            this.threadDEXSD.Size = new System.Drawing.Size(14, 13);
            this.threadDEXSD.TabIndex = 49;
            this.threadDEXSD.TabStop = true;
            this.threadDEXSD.UseVisualStyleBackColor = true;
            this.threadDEXSD.CheckedChanged += new System.EventHandler(this.threadDEXSD_CheckedChanged);
            // 
            // threadDEXMR
            // 
            this.threadDEXMR.AutoSize = true;
            this.threadDEXMR.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.threadDEXMR.Location = new System.Drawing.Point(316, 67);
            this.threadDEXMR.Name = "threadDEXMR";
            this.threadDEXMR.Size = new System.Drawing.Size(14, 13);
            this.threadDEXMR.TabIndex = 48;
            this.threadDEXMR.TabStop = true;
            this.threadDEXMR.UseVisualStyleBackColor = true;
            this.threadDEXMR.CheckedChanged += new System.EventHandler(this.threadDEXMR_CheckedChanged);
            // 
            // threadDEXMD
            // 
            this.threadDEXMD.AutoSize = true;
            this.threadDEXMD.Location = new System.Drawing.Point(286, 67);
            this.threadDEXMD.Name = "threadDEXMD";
            this.threadDEXMD.Size = new System.Drawing.Size(14, 13);
            this.threadDEXMD.TabIndex = 47;
            this.threadDEXMD.TabStop = true;
            this.threadDEXMD.UseVisualStyleBackColor = true;
            this.threadDEXMD.CheckedChanged += new System.EventHandler(this.threadDEXMD_CheckedChanged);
            // 
            // threadINSSR
            // 
            this.threadINSSR.AutoSize = true;
            this.threadINSSR.Location = new System.Drawing.Point(377, 41);
            this.threadINSSR.Name = "threadINSSR";
            this.threadINSSR.Size = new System.Drawing.Size(14, 13);
            this.threadINSSR.TabIndex = 46;
            this.threadINSSR.TabStop = true;
            this.threadINSSR.UseVisualStyleBackColor = true;
            this.threadINSSR.CheckedChanged += new System.EventHandler(this.threadINSSR_CheckedChanged);
            // 
            // threadINSSD
            // 
            this.threadINSSD.AutoSize = true;
            this.threadINSSD.Location = new System.Drawing.Point(346, 41);
            this.threadINSSD.Name = "threadINSSD";
            this.threadINSSD.Size = new System.Drawing.Size(14, 13);
            this.threadINSSD.TabIndex = 45;
            this.threadINSSD.TabStop = true;
            this.threadINSSD.UseVisualStyleBackColor = true;
            this.threadINSSD.CheckedChanged += new System.EventHandler(this.threadINSSD_CheckedChanged);
            // 
            // threadINSMR
            // 
            this.threadINSMR.AutoSize = true;
            this.threadINSMR.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.threadINSMR.Location = new System.Drawing.Point(316, 41);
            this.threadINSMR.Name = "threadINSMR";
            this.threadINSMR.Size = new System.Drawing.Size(14, 13);
            this.threadINSMR.TabIndex = 44;
            this.threadINSMR.TabStop = true;
            this.threadINSMR.UseVisualStyleBackColor = true;
            this.threadINSMR.CheckedChanged += new System.EventHandler(this.threadINSMR_CheckedChanged);
            // 
            // threadINSMD
            // 
            this.threadINSMD.AutoSize = true;
            this.threadINSMD.Location = new System.Drawing.Point(286, 41);
            this.threadINSMD.Name = "threadINSMD";
            this.threadINSMD.Size = new System.Drawing.Size(14, 13);
            this.threadINSMD.TabIndex = 43;
            this.threadINSMD.TabStop = true;
            this.threadINSMD.UseVisualStyleBackColor = true;
            this.threadINSMD.CheckedChanged += new System.EventHandler(this.threadINSMD_CheckedChanged);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(407, 20);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(26, 13);
            this.label72.TabIndex = 42;
            this.label72.Text = "Stat";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(374, 20);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(22, 13);
            this.label73.TabIndex = 41;
            this.label73.Text = "SR";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(343, 20);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(22, 13);
            this.label74.TabIndex = 40;
            this.label74.Text = "SD";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(313, 20);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(24, 13);
            this.label75.TabIndex = 39;
            this.label75.Text = "MR";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(283, 20);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(24, 13);
            this.label76.TabIndex = 38;
            this.label76.Text = "MD";
            // 
            // threadWILSR
            // 
            this.threadWILSR.AutoSize = true;
            this.threadWILSR.Location = new System.Drawing.Point(154, 118);
            this.threadWILSR.Name = "threadWILSR";
            this.threadWILSR.Size = new System.Drawing.Size(14, 13);
            this.threadWILSR.TabIndex = 37;
            this.threadWILSR.TabStop = true;
            this.threadWILSR.UseVisualStyleBackColor = true;
            this.threadWILSR.CheckedChanged += new System.EventHandler(this.threadWILSR_CheckedChanged);
            // 
            // threadWILSD
            // 
            this.threadWILSD.AutoSize = true;
            this.threadWILSD.Location = new System.Drawing.Point(123, 118);
            this.threadWILSD.Name = "threadWILSD";
            this.threadWILSD.Size = new System.Drawing.Size(14, 13);
            this.threadWILSD.TabIndex = 36;
            this.threadWILSD.TabStop = true;
            this.threadWILSD.UseVisualStyleBackColor = true;
            this.threadWILSD.CheckedChanged += new System.EventHandler(this.threadWILSD_CheckedChanged);
            // 
            // threadWILMR
            // 
            this.threadWILMR.AutoSize = true;
            this.threadWILMR.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.threadWILMR.Location = new System.Drawing.Point(93, 118);
            this.threadWILMR.Name = "threadWILMR";
            this.threadWILMR.Size = new System.Drawing.Size(14, 13);
            this.threadWILMR.TabIndex = 35;
            this.threadWILMR.TabStop = true;
            this.threadWILMR.UseVisualStyleBackColor = true;
            this.threadWILMR.CheckedChanged += new System.EventHandler(this.threadWILMR_CheckedChanged);
            // 
            // threadWILMD
            // 
            this.threadWILMD.AutoSize = true;
            this.threadWILMD.Location = new System.Drawing.Point(63, 118);
            this.threadWILMD.Name = "threadWILMD";
            this.threadWILMD.Size = new System.Drawing.Size(14, 13);
            this.threadWILMD.TabIndex = 34;
            this.threadWILMD.TabStop = true;
            this.threadWILMD.UseVisualStyleBackColor = true;
            this.threadWILMD.CheckedChanged += new System.EventHandler(this.threadWILMD_CheckedChanged);
            // 
            // threadSPRSR
            // 
            this.threadSPRSR.AutoSize = true;
            this.threadSPRSR.Location = new System.Drawing.Point(154, 93);
            this.threadSPRSR.Name = "threadSPRSR";
            this.threadSPRSR.Size = new System.Drawing.Size(14, 13);
            this.threadSPRSR.TabIndex = 33;
            this.threadSPRSR.TabStop = true;
            this.threadSPRSR.UseVisualStyleBackColor = true;
            this.threadSPRSR.CheckedChanged += new System.EventHandler(this.threadSPRSR_CheckedChanged);
            // 
            // threadSPRSD
            // 
            this.threadSPRSD.AutoSize = true;
            this.threadSPRSD.Location = new System.Drawing.Point(123, 93);
            this.threadSPRSD.Name = "threadSPRSD";
            this.threadSPRSD.Size = new System.Drawing.Size(14, 13);
            this.threadSPRSD.TabIndex = 32;
            this.threadSPRSD.TabStop = true;
            this.threadSPRSD.UseVisualStyleBackColor = true;
            this.threadSPRSD.CheckedChanged += new System.EventHandler(this.threadSPRSD_CheckedChanged);
            // 
            // threadSPRMR
            // 
            this.threadSPRMR.AutoSize = true;
            this.threadSPRMR.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.threadSPRMR.Location = new System.Drawing.Point(93, 93);
            this.threadSPRMR.Name = "threadSPRMR";
            this.threadSPRMR.Size = new System.Drawing.Size(14, 13);
            this.threadSPRMR.TabIndex = 31;
            this.threadSPRMR.TabStop = true;
            this.threadSPRMR.UseVisualStyleBackColor = true;
            this.threadSPRMR.CheckedChanged += new System.EventHandler(this.threadSPRMR_CheckedChanged);
            // 
            // threadSPRMD
            // 
            this.threadSPRMD.AutoSize = true;
            this.threadSPRMD.Location = new System.Drawing.Point(63, 93);
            this.threadSPRMD.Name = "threadSPRMD";
            this.threadSPRMD.Size = new System.Drawing.Size(14, 13);
            this.threadSPRMD.TabIndex = 30;
            this.threadSPRMD.TabStop = true;
            this.threadSPRMD.UseVisualStyleBackColor = true;
            this.threadSPRMD.CheckedChanged += new System.EventHandler(this.threadSPRMD_CheckedChanged);
            // 
            // threadSTASR
            // 
            this.threadSTASR.AutoSize = true;
            this.threadSTASR.Location = new System.Drawing.Point(154, 67);
            this.threadSTASR.Name = "threadSTASR";
            this.threadSTASR.Size = new System.Drawing.Size(14, 13);
            this.threadSTASR.TabIndex = 29;
            this.threadSTASR.TabStop = true;
            this.threadSTASR.UseVisualStyleBackColor = true;
            this.threadSTASR.CheckedChanged += new System.EventHandler(this.threadSTASR_CheckedChanged);
            // 
            // threadSTASD
            // 
            this.threadSTASD.AutoSize = true;
            this.threadSTASD.Location = new System.Drawing.Point(123, 67);
            this.threadSTASD.Name = "threadSTASD";
            this.threadSTASD.Size = new System.Drawing.Size(14, 13);
            this.threadSTASD.TabIndex = 28;
            this.threadSTASD.TabStop = true;
            this.threadSTASD.UseVisualStyleBackColor = true;
            this.threadSTASD.CheckedChanged += new System.EventHandler(this.threadSTASD_CheckedChanged);
            // 
            // threadSTAMR
            // 
            this.threadSTAMR.AutoSize = true;
            this.threadSTAMR.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.threadSTAMR.Location = new System.Drawing.Point(93, 67);
            this.threadSTAMR.Name = "threadSTAMR";
            this.threadSTAMR.Size = new System.Drawing.Size(14, 13);
            this.threadSTAMR.TabIndex = 27;
            this.threadSTAMR.TabStop = true;
            this.threadSTAMR.UseVisualStyleBackColor = true;
            this.threadSTAMR.CheckedChanged += new System.EventHandler(this.threadSTAMR_CheckedChanged);
            // 
            // threadSTAMD
            // 
            this.threadSTAMD.AutoSize = true;
            this.threadSTAMD.Location = new System.Drawing.Point(63, 67);
            this.threadSTAMD.Name = "threadSTAMD";
            this.threadSTAMD.Size = new System.Drawing.Size(14, 13);
            this.threadSTAMD.TabIndex = 26;
            this.threadSTAMD.TabStop = true;
            this.threadSTAMD.UseVisualStyleBackColor = true;
            this.threadSTAMD.CheckedChanged += new System.EventHandler(this.threadSTAMD_CheckedChanged);
            // 
            // threadSTRSR
            // 
            this.threadSTRSR.AutoSize = true;
            this.threadSTRSR.Location = new System.Drawing.Point(154, 41);
            this.threadSTRSR.Name = "threadSTRSR";
            this.threadSTRSR.Size = new System.Drawing.Size(14, 13);
            this.threadSTRSR.TabIndex = 25;
            this.threadSTRSR.TabStop = true;
            this.threadSTRSR.UseVisualStyleBackColor = true;
            this.threadSTRSR.CheckedChanged += new System.EventHandler(this.threadSTRSR_CheckedChanged);
            // 
            // threadSTRSD
            // 
            this.threadSTRSD.AutoSize = true;
            this.threadSTRSD.Location = new System.Drawing.Point(123, 41);
            this.threadSTRSD.Name = "threadSTRSD";
            this.threadSTRSD.Size = new System.Drawing.Size(14, 13);
            this.threadSTRSD.TabIndex = 24;
            this.threadSTRSD.TabStop = true;
            this.threadSTRSD.UseVisualStyleBackColor = true;
            this.threadSTRSD.CheckedChanged += new System.EventHandler(this.threadSTRSD_CheckedChanged);
            // 
            // threadSTRMR
            // 
            this.threadSTRMR.AutoSize = true;
            this.threadSTRMR.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.threadSTRMR.Location = new System.Drawing.Point(93, 41);
            this.threadSTRMR.Name = "threadSTRMR";
            this.threadSTRMR.Size = new System.Drawing.Size(14, 13);
            this.threadSTRMR.TabIndex = 23;
            this.threadSTRMR.TabStop = true;
            this.threadSTRMR.UseVisualStyleBackColor = true;
            this.threadSTRMR.CheckedChanged += new System.EventHandler(this.threadSTRMR_CheckedChanged);
            // 
            // threadSTRMD
            // 
            this.threadSTRMD.AutoSize = true;
            this.threadSTRMD.Location = new System.Drawing.Point(63, 41);
            this.threadSTRMD.Name = "threadSTRMD";
            this.threadSTRMD.Size = new System.Drawing.Size(14, 13);
            this.threadSTRMD.TabIndex = 22;
            this.threadSTRMD.TabStop = true;
            this.threadSTRMD.UseVisualStyleBackColor = true;
            this.threadSTRMD.CheckedChanged += new System.EventHandler(this.threadSTRMD_CheckedChanged);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(184, 20);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(26, 13);
            this.label59.TabIndex = 20;
            this.label59.Text = "Stat";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(236, 118);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(31, 13);
            this.label70.TabIndex = 14;
            this.label70.Text = "Luck";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(234, 67);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(48, 13);
            this.label63.TabIndex = 15;
            this.label63.Text = "Dexterity";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(236, 93);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(38, 13);
            this.label71.TabIndex = 13;
            this.label71.Text = "Speed";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 93);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(30, 13);
            this.label60.TabIndex = 15;
            this.label60.Text = "Spirit";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(236, 41);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(38, 13);
            this.label68.TabIndex = 14;
            this.label68.Text = "Insight";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(151, 20);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(22, 13);
            this.label67.TabIndex = 12;
            this.label67.Text = "SR";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(6, 118);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(53, 13);
            this.label69.TabIndex = 13;
            this.label69.Text = "Willpower";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(6, 67);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(45, 13);
            this.label61.TabIndex = 14;
            this.label61.Text = "Stamina";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(120, 20);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(22, 13);
            this.label66.TabIndex = 11;
            this.label66.Text = "SD";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 41);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(47, 13);
            this.label62.TabIndex = 13;
            this.label62.Text = "Strength";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(90, 20);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(24, 13);
            this.label65.TabIndex = 10;
            this.label65.Text = "MR";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(60, 20);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(24, 13);
            this.label64.TabIndex = 9;
            this.label64.Text = "MD";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox3);
            this.panel3.Controls.Add(this.subBreedBox);
            this.panel3.Controls.Add(this.groupBox4);
            this.panel3.Controls.Add(this.label47);
            this.panel3.Controls.Add(this.label58);
            this.panel3.Controls.Add(this.label52);
            this.panel3.Controls.Add(this.label57);
            this.panel3.Controls.Add(this.label53);
            this.panel3.Controls.Add(this.label56);
            this.panel3.Controls.Add(this.label54);
            this.panel3.Controls.Add(this.label55);
            this.panel3.Location = new System.Drawing.Point(333, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(315, 275);
            this.panel3.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numLCKSRW);
            this.groupBox3.Controls.Add(this.numSTRSR);
            this.groupBox3.Controls.Add(this.numSPDSRW);
            this.groupBox3.Controls.Add(this.numSTASR);
            this.groupBox3.Controls.Add(this.numDEXSRW);
            this.groupBox3.Controls.Add(this.numSPRSR);
            this.groupBox3.Controls.Add(this.numINSSRW);
            this.groupBox3.Controls.Add(this.numWILSR);
            this.groupBox3.Controls.Add(this.numWILSRW);
            this.groupBox3.Controls.Add(this.numINSSR);
            this.groupBox3.Controls.Add(this.numSPRSRW);
            this.groupBox3.Controls.Add(this.numDEXSR);
            this.groupBox3.Controls.Add(this.numSTASRW);
            this.groupBox3.Controls.Add(this.numSPDSR);
            this.groupBox3.Controls.Add(this.numSTRSRW);
            this.groupBox3.Controls.Add(this.numLCKSR);
            this.groupBox3.Location = new System.Drawing.Point(188, 51);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(115, 220);
            this.groupBox3.TabIndex = 41;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Recessive";
            // 
            // numLCKSRW
            // 
            this.numLCKSRW.DecimalPlaces = 2;
            this.numLCKSRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numLCKSRW.Location = new System.Drawing.Point(62, 194);
            this.numLCKSRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numLCKSRW.Name = "numLCKSRW";
            this.numLCKSRW.Size = new System.Drawing.Size(46, 20);
            this.numLCKSRW.TabIndex = 31;
            this.numLCKSRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSTRSR
            // 
            this.numSTRSR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSTRSR.Location = new System.Drawing.Point(6, 14);
            this.numSTRSR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSTRSR.Name = "numSTRSR";
            this.numSTRSR.Size = new System.Drawing.Size(50, 20);
            this.numSTRSR.TabIndex = 16;
            this.numSTRSR.Value = new decimal(new int[] {
            83,
            0,
            0,
            0});
            // 
            // numSPDSRW
            // 
            this.numSPDSRW.DecimalPlaces = 2;
            this.numSPDSRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSPDSRW.Location = new System.Drawing.Point(62, 170);
            this.numSPDSRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSPDSRW.Name = "numSPDSRW";
            this.numSPDSRW.Size = new System.Drawing.Size(46, 20);
            this.numSPDSRW.TabIndex = 30;
            this.numSPDSRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSTASR
            // 
            this.numSTASR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSTASR.Location = new System.Drawing.Point(5, 40);
            this.numSTASR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSTASR.Name = "numSTASR";
            this.numSTASR.Size = new System.Drawing.Size(51, 20);
            this.numSTASR.TabIndex = 17;
            this.numSTASR.Value = new decimal(new int[] {
            83,
            0,
            0,
            0});
            // 
            // numDEXSRW
            // 
            this.numDEXSRW.DecimalPlaces = 2;
            this.numDEXSRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numDEXSRW.Location = new System.Drawing.Point(62, 144);
            this.numDEXSRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numDEXSRW.Name = "numDEXSRW";
            this.numDEXSRW.Size = new System.Drawing.Size(46, 20);
            this.numDEXSRW.TabIndex = 29;
            this.numDEXSRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSPRSR
            // 
            this.numSPRSR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSPRSR.Location = new System.Drawing.Point(5, 66);
            this.numSPRSR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSPRSR.Name = "numSPRSR";
            this.numSPRSR.Size = new System.Drawing.Size(51, 20);
            this.numSPRSR.TabIndex = 18;
            this.numSPRSR.Value = new decimal(new int[] {
            83,
            0,
            0,
            0});
            // 
            // numINSSRW
            // 
            this.numINSSRW.DecimalPlaces = 2;
            this.numINSSRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numINSSRW.Location = new System.Drawing.Point(62, 118);
            this.numINSSRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numINSSRW.Name = "numINSSRW";
            this.numINSSRW.Size = new System.Drawing.Size(46, 20);
            this.numINSSRW.TabIndex = 28;
            this.numINSSRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numWILSR
            // 
            this.numWILSR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numWILSR.Location = new System.Drawing.Point(5, 92);
            this.numWILSR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numWILSR.Name = "numWILSR";
            this.numWILSR.Size = new System.Drawing.Size(51, 20);
            this.numWILSR.TabIndex = 19;
            this.numWILSR.Value = new decimal(new int[] {
            83,
            0,
            0,
            0});
            // 
            // numWILSRW
            // 
            this.numWILSRW.DecimalPlaces = 2;
            this.numWILSRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numWILSRW.Location = new System.Drawing.Point(62, 92);
            this.numWILSRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numWILSRW.Name = "numWILSRW";
            this.numWILSRW.Size = new System.Drawing.Size(46, 20);
            this.numWILSRW.TabIndex = 27;
            this.numWILSRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numINSSR
            // 
            this.numINSSR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numINSSR.Location = new System.Drawing.Point(5, 118);
            this.numINSSR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numINSSR.Name = "numINSSR";
            this.numINSSR.Size = new System.Drawing.Size(51, 20);
            this.numINSSR.TabIndex = 20;
            this.numINSSR.Value = new decimal(new int[] {
            83,
            0,
            0,
            0});
            // 
            // numSPRSRW
            // 
            this.numSPRSRW.DecimalPlaces = 2;
            this.numSPRSRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSPRSRW.Location = new System.Drawing.Point(62, 66);
            this.numSPRSRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSPRSRW.Name = "numSPRSRW";
            this.numSPRSRW.Size = new System.Drawing.Size(46, 20);
            this.numSPRSRW.TabIndex = 26;
            this.numSPRSRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numDEXSR
            // 
            this.numDEXSR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numDEXSR.Location = new System.Drawing.Point(5, 144);
            this.numDEXSR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numDEXSR.Name = "numDEXSR";
            this.numDEXSR.Size = new System.Drawing.Size(51, 20);
            this.numDEXSR.TabIndex = 21;
            this.numDEXSR.Value = new decimal(new int[] {
            83,
            0,
            0,
            0});
            // 
            // numSTASRW
            // 
            this.numSTASRW.DecimalPlaces = 2;
            this.numSTASRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSTASRW.Location = new System.Drawing.Point(62, 40);
            this.numSTASRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSTASRW.Name = "numSTASRW";
            this.numSTASRW.Size = new System.Drawing.Size(46, 20);
            this.numSTASRW.TabIndex = 25;
            this.numSTASRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSPDSR
            // 
            this.numSPDSR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSPDSR.Location = new System.Drawing.Point(5, 170);
            this.numSPDSR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSPDSR.Name = "numSPDSR";
            this.numSPDSR.Size = new System.Drawing.Size(51, 20);
            this.numSPDSR.TabIndex = 22;
            this.numSPDSR.Value = new decimal(new int[] {
            83,
            0,
            0,
            0});
            // 
            // numSTRSRW
            // 
            this.numSTRSRW.DecimalPlaces = 2;
            this.numSTRSRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSTRSRW.Location = new System.Drawing.Point(62, 14);
            this.numSTRSRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSTRSRW.Name = "numSTRSRW";
            this.numSTRSRW.Size = new System.Drawing.Size(46, 20);
            this.numSTRSRW.TabIndex = 24;
            this.numSTRSRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numLCKSR
            // 
            this.numLCKSR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numLCKSR.Location = new System.Drawing.Point(5, 195);
            this.numLCKSR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numLCKSR.Name = "numLCKSR";
            this.numLCKSR.Size = new System.Drawing.Size(51, 20);
            this.numLCKSR.TabIndex = 23;
            this.numLCKSR.Value = new decimal(new int[] {
            83,
            0,
            0,
            0});
            // 
            // subBreedBox
            // 
            this.subBreedBox.FormattingEnabled = true;
            this.subBreedBox.Items.AddRange(new object[] {
            "Bear",
            "Feline",
            "Fox",
            "Gorilla",
            "Hawk",
            "Lizard",
            "Spider",
            "Wolf"});
            this.subBreedBox.Location = new System.Drawing.Point(67, 24);
            this.subBreedBox.Name = "subBreedBox";
            this.subBreedBox.Size = new System.Drawing.Size(121, 21);
            this.subBreedBox.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.numLCKSDW);
            this.groupBox4.Controls.Add(this.numSPDSDW);
            this.groupBox4.Controls.Add(this.numDEXSDW);
            this.groupBox4.Controls.Add(this.numINSSDW);
            this.groupBox4.Controls.Add(this.numWILSDW);
            this.groupBox4.Controls.Add(this.numSPRSDW);
            this.groupBox4.Controls.Add(this.numSTASDW);
            this.groupBox4.Controls.Add(this.numSTRSDW);
            this.groupBox4.Controls.Add(this.numLCKSD);
            this.groupBox4.Controls.Add(this.numSPDSD);
            this.groupBox4.Controls.Add(this.numDEXSD);
            this.groupBox4.Controls.Add(this.numINSSD);
            this.groupBox4.Controls.Add(this.numWILSD);
            this.groupBox4.Controls.Add(this.numSPRSD);
            this.groupBox4.Controls.Add(this.numSTASD);
            this.groupBox4.Controls.Add(this.numSTRSD);
            this.groupBox4.Location = new System.Drawing.Point(67, 51);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(115, 220);
            this.groupBox4.TabIndex = 40;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dominant";
            // 
            // numLCKSDW
            // 
            this.numLCKSDW.DecimalPlaces = 2;
            this.numLCKSDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numLCKSDW.Location = new System.Drawing.Point(63, 194);
            this.numLCKSDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numLCKSDW.Name = "numLCKSDW";
            this.numLCKSDW.Size = new System.Drawing.Size(46, 20);
            this.numLCKSDW.TabIndex = 15;
            this.numLCKSDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSPDSDW
            // 
            this.numSPDSDW.DecimalPlaces = 2;
            this.numSPDSDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSPDSDW.Location = new System.Drawing.Point(63, 170);
            this.numSPDSDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSPDSDW.Name = "numSPDSDW";
            this.numSPDSDW.Size = new System.Drawing.Size(46, 20);
            this.numSPDSDW.TabIndex = 14;
            this.numSPDSDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numDEXSDW
            // 
            this.numDEXSDW.DecimalPlaces = 2;
            this.numDEXSDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numDEXSDW.Location = new System.Drawing.Point(63, 144);
            this.numDEXSDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numDEXSDW.Name = "numDEXSDW";
            this.numDEXSDW.Size = new System.Drawing.Size(46, 20);
            this.numDEXSDW.TabIndex = 13;
            this.numDEXSDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numINSSDW
            // 
            this.numINSSDW.DecimalPlaces = 2;
            this.numINSSDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numINSSDW.Location = new System.Drawing.Point(63, 118);
            this.numINSSDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numINSSDW.Name = "numINSSDW";
            this.numINSSDW.Size = new System.Drawing.Size(46, 20);
            this.numINSSDW.TabIndex = 12;
            this.numINSSDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numWILSDW
            // 
            this.numWILSDW.DecimalPlaces = 2;
            this.numWILSDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numWILSDW.Location = new System.Drawing.Point(63, 92);
            this.numWILSDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numWILSDW.Name = "numWILSDW";
            this.numWILSDW.Size = new System.Drawing.Size(46, 20);
            this.numWILSDW.TabIndex = 11;
            this.numWILSDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSPRSDW
            // 
            this.numSPRSDW.DecimalPlaces = 2;
            this.numSPRSDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSPRSDW.Location = new System.Drawing.Point(63, 66);
            this.numSPRSDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSPRSDW.Name = "numSPRSDW";
            this.numSPRSDW.Size = new System.Drawing.Size(46, 20);
            this.numSPRSDW.TabIndex = 10;
            this.numSPRSDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSTASDW
            // 
            this.numSTASDW.DecimalPlaces = 2;
            this.numSTASDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSTASDW.Location = new System.Drawing.Point(63, 40);
            this.numSTASDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSTASDW.Name = "numSTASDW";
            this.numSTASDW.Size = new System.Drawing.Size(46, 20);
            this.numSTASDW.TabIndex = 9;
            this.numSTASDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSTRSDW
            // 
            this.numSTRSDW.DecimalPlaces = 2;
            this.numSTRSDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSTRSDW.Location = new System.Drawing.Point(63, 14);
            this.numSTRSDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSTRSDW.Name = "numSTRSDW";
            this.numSTRSDW.Size = new System.Drawing.Size(46, 20);
            this.numSTRSDW.TabIndex = 8;
            this.numSTRSDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numLCKSD
            // 
            this.numLCKSD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numLCKSD.Location = new System.Drawing.Point(6, 195);
            this.numLCKSD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numLCKSD.Name = "numLCKSD";
            this.numLCKSD.Size = new System.Drawing.Size(51, 20);
            this.numLCKSD.TabIndex = 7;
            this.numLCKSD.Value = new decimal(new int[] {
            82,
            0,
            0,
            0});
            // 
            // numSPDSD
            // 
            this.numSPDSD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSPDSD.Location = new System.Drawing.Point(6, 170);
            this.numSPDSD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSPDSD.Name = "numSPDSD";
            this.numSPDSD.Size = new System.Drawing.Size(51, 20);
            this.numSPDSD.TabIndex = 6;
            this.numSPDSD.Value = new decimal(new int[] {
            82,
            0,
            0,
            0});
            // 
            // numDEXSD
            // 
            this.numDEXSD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numDEXSD.Location = new System.Drawing.Point(6, 144);
            this.numDEXSD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numDEXSD.Name = "numDEXSD";
            this.numDEXSD.Size = new System.Drawing.Size(51, 20);
            this.numDEXSD.TabIndex = 5;
            this.numDEXSD.Value = new decimal(new int[] {
            82,
            0,
            0,
            0});
            // 
            // numINSSD
            // 
            this.numINSSD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numINSSD.Location = new System.Drawing.Point(6, 118);
            this.numINSSD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numINSSD.Name = "numINSSD";
            this.numINSSD.Size = new System.Drawing.Size(51, 20);
            this.numINSSD.TabIndex = 4;
            this.numINSSD.Value = new decimal(new int[] {
            82,
            0,
            0,
            0});
            // 
            // numWILSD
            // 
            this.numWILSD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numWILSD.Location = new System.Drawing.Point(6, 92);
            this.numWILSD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numWILSD.Name = "numWILSD";
            this.numWILSD.Size = new System.Drawing.Size(51, 20);
            this.numWILSD.TabIndex = 3;
            this.numWILSD.Value = new decimal(new int[] {
            82,
            0,
            0,
            0});
            // 
            // numSPRSD
            // 
            this.numSPRSD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSPRSD.Location = new System.Drawing.Point(6, 66);
            this.numSPRSD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSPRSD.Name = "numSPRSD";
            this.numSPRSD.Size = new System.Drawing.Size(51, 20);
            this.numSPRSD.TabIndex = 2;
            this.numSPRSD.Value = new decimal(new int[] {
            82,
            0,
            0,
            0});
            // 
            // numSTASD
            // 
            this.numSTASD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSTASD.Location = new System.Drawing.Point(6, 40);
            this.numSTASD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSTASD.Name = "numSTASD";
            this.numSTASD.Size = new System.Drawing.Size(51, 20);
            this.numSTASD.TabIndex = 1;
            this.numSTASD.Value = new decimal(new int[] {
            82,
            0,
            0,
            0});
            // 
            // numSTRSD
            // 
            this.numSTRSD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSTRSD.Location = new System.Drawing.Point(7, 14);
            this.numSTRSD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSTRSD.Name = "numSTRSD";
            this.numSTRSD.Size = new System.Drawing.Size(50, 20);
            this.numSTRSD.TabIndex = 0;
            this.numSTRSD.Value = new decimal(new int[] {
            82,
            0,
            0,
            0});
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 253);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(31, 13);
            this.label47.TabIndex = 39;
            this.label47.Text = "Luck";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(8, 72);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(47, 13);
            this.label58.TabIndex = 32;
            this.label58.Text = "Strength";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 228);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(38, 13);
            this.label52.TabIndex = 38;
            this.label52.Text = "Speed";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(8, 98);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(45, 13);
            this.label57.TabIndex = 33;
            this.label57.Text = "Stamina";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 202);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(48, 13);
            this.label53.TabIndex = 37;
            this.label53.Text = "Dexterity";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(8, 124);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(30, 13);
            this.label56.TabIndex = 34;
            this.label56.Text = "Spirit";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(8, 176);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(38, 13);
            this.label54.TabIndex = 36;
            this.label54.Text = "Insight";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(8, 150);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(53, 13);
            this.label55.TabIndex = 35;
            this.label55.Text = "Willpower";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.label42);
            this.panel2.Controls.Add(this.label33);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.mainBreedBox);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(315, 275);
            this.panel2.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numLCKMRW);
            this.groupBox2.Controls.Add(this.numSTRMR);
            this.groupBox2.Controls.Add(this.numSPDMRW);
            this.groupBox2.Controls.Add(this.numSTAMR);
            this.groupBox2.Controls.Add(this.numDEXMRW);
            this.groupBox2.Controls.Add(this.numSPRMR);
            this.groupBox2.Controls.Add(this.numINSMRW);
            this.groupBox2.Controls.Add(this.numWILMR);
            this.groupBox2.Controls.Add(this.numWILMRW);
            this.groupBox2.Controls.Add(this.numINSMR);
            this.groupBox2.Controls.Add(this.numSPRMRW);
            this.groupBox2.Controls.Add(this.numDEXMR);
            this.groupBox2.Controls.Add(this.numSTAMRW);
            this.groupBox2.Controls.Add(this.numSPDMR);
            this.groupBox2.Controls.Add(this.numSTRMRW);
            this.groupBox2.Controls.Add(this.numLCKMR);
            this.groupBox2.Location = new System.Drawing.Point(185, 51);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(115, 220);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Recessive";
            // 
            // numLCKMRW
            // 
            this.numLCKMRW.DecimalPlaces = 2;
            this.numLCKMRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numLCKMRW.Location = new System.Drawing.Point(62, 194);
            this.numLCKMRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numLCKMRW.Name = "numLCKMRW";
            this.numLCKMRW.Size = new System.Drawing.Size(46, 20);
            this.numLCKMRW.TabIndex = 31;
            this.numLCKMRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSTRMR
            // 
            this.numSTRMR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSTRMR.Location = new System.Drawing.Point(6, 14);
            this.numSTRMR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSTRMR.Name = "numSTRMR";
            this.numSTRMR.Size = new System.Drawing.Size(50, 20);
            this.numSTRMR.TabIndex = 16;
            this.numSTRMR.Value = new decimal(new int[] {
            81,
            0,
            0,
            0});
            // 
            // numSPDMRW
            // 
            this.numSPDMRW.DecimalPlaces = 2;
            this.numSPDMRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSPDMRW.Location = new System.Drawing.Point(62, 170);
            this.numSPDMRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSPDMRW.Name = "numSPDMRW";
            this.numSPDMRW.Size = new System.Drawing.Size(46, 20);
            this.numSPDMRW.TabIndex = 30;
            this.numSPDMRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSTAMR
            // 
            this.numSTAMR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSTAMR.Location = new System.Drawing.Point(5, 40);
            this.numSTAMR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSTAMR.Name = "numSTAMR";
            this.numSTAMR.Size = new System.Drawing.Size(51, 20);
            this.numSTAMR.TabIndex = 17;
            this.numSTAMR.Value = new decimal(new int[] {
            81,
            0,
            0,
            0});
            // 
            // numDEXMRW
            // 
            this.numDEXMRW.DecimalPlaces = 2;
            this.numDEXMRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numDEXMRW.Location = new System.Drawing.Point(62, 144);
            this.numDEXMRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numDEXMRW.Name = "numDEXMRW";
            this.numDEXMRW.Size = new System.Drawing.Size(46, 20);
            this.numDEXMRW.TabIndex = 29;
            this.numDEXMRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSPRMR
            // 
            this.numSPRMR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSPRMR.Location = new System.Drawing.Point(5, 66);
            this.numSPRMR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSPRMR.Name = "numSPRMR";
            this.numSPRMR.Size = new System.Drawing.Size(51, 20);
            this.numSPRMR.TabIndex = 18;
            this.numSPRMR.Value = new decimal(new int[] {
            81,
            0,
            0,
            0});
            // 
            // numINSMRW
            // 
            this.numINSMRW.DecimalPlaces = 2;
            this.numINSMRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numINSMRW.Location = new System.Drawing.Point(62, 118);
            this.numINSMRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numINSMRW.Name = "numINSMRW";
            this.numINSMRW.Size = new System.Drawing.Size(46, 20);
            this.numINSMRW.TabIndex = 28;
            this.numINSMRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numWILMR
            // 
            this.numWILMR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numWILMR.Location = new System.Drawing.Point(5, 92);
            this.numWILMR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numWILMR.Name = "numWILMR";
            this.numWILMR.Size = new System.Drawing.Size(51, 20);
            this.numWILMR.TabIndex = 19;
            this.numWILMR.Value = new decimal(new int[] {
            81,
            0,
            0,
            0});
            // 
            // numWILMRW
            // 
            this.numWILMRW.DecimalPlaces = 2;
            this.numWILMRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numWILMRW.Location = new System.Drawing.Point(62, 92);
            this.numWILMRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numWILMRW.Name = "numWILMRW";
            this.numWILMRW.Size = new System.Drawing.Size(46, 20);
            this.numWILMRW.TabIndex = 27;
            this.numWILMRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numINSMR
            // 
            this.numINSMR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numINSMR.Location = new System.Drawing.Point(5, 118);
            this.numINSMR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numINSMR.Name = "numINSMR";
            this.numINSMR.Size = new System.Drawing.Size(51, 20);
            this.numINSMR.TabIndex = 20;
            this.numINSMR.Value = new decimal(new int[] {
            81,
            0,
            0,
            0});
            // 
            // numSPRMRW
            // 
            this.numSPRMRW.DecimalPlaces = 2;
            this.numSPRMRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSPRMRW.Location = new System.Drawing.Point(62, 66);
            this.numSPRMRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSPRMRW.Name = "numSPRMRW";
            this.numSPRMRW.Size = new System.Drawing.Size(46, 20);
            this.numSPRMRW.TabIndex = 26;
            this.numSPRMRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numDEXMR
            // 
            this.numDEXMR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numDEXMR.Location = new System.Drawing.Point(5, 144);
            this.numDEXMR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numDEXMR.Name = "numDEXMR";
            this.numDEXMR.Size = new System.Drawing.Size(51, 20);
            this.numDEXMR.TabIndex = 21;
            this.numDEXMR.Value = new decimal(new int[] {
            81,
            0,
            0,
            0});
            // 
            // numSTAMRW
            // 
            this.numSTAMRW.DecimalPlaces = 2;
            this.numSTAMRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSTAMRW.Location = new System.Drawing.Point(62, 40);
            this.numSTAMRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSTAMRW.Name = "numSTAMRW";
            this.numSTAMRW.Size = new System.Drawing.Size(46, 20);
            this.numSTAMRW.TabIndex = 25;
            this.numSTAMRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSPDMR
            // 
            this.numSPDMR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSPDMR.Location = new System.Drawing.Point(5, 170);
            this.numSPDMR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSPDMR.Name = "numSPDMR";
            this.numSPDMR.Size = new System.Drawing.Size(51, 20);
            this.numSPDMR.TabIndex = 22;
            this.numSPDMR.Value = new decimal(new int[] {
            81,
            0,
            0,
            0});
            // 
            // numSTRMRW
            // 
            this.numSTRMRW.DecimalPlaces = 2;
            this.numSTRMRW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSTRMRW.Location = new System.Drawing.Point(62, 14);
            this.numSTRMRW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSTRMRW.Name = "numSTRMRW";
            this.numSTRMRW.Size = new System.Drawing.Size(46, 20);
            this.numSTRMRW.TabIndex = 24;
            this.numSTRMRW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numLCKMR
            // 
            this.numLCKMR.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numLCKMR.Location = new System.Drawing.Point(5, 195);
            this.numLCKMR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numLCKMR.Name = "numLCKMR";
            this.numLCKMR.Size = new System.Drawing.Size(51, 20);
            this.numLCKMR.TabIndex = 23;
            this.numLCKMR.Value = new decimal(new int[] {
            81,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numLCKMDW);
            this.groupBox1.Controls.Add(this.numSPDMDW);
            this.groupBox1.Controls.Add(this.numDEXMDW);
            this.groupBox1.Controls.Add(this.numINSMDW);
            this.groupBox1.Controls.Add(this.numWILMDW);
            this.groupBox1.Controls.Add(this.numSPRMDW);
            this.groupBox1.Controls.Add(this.numSTAMDW);
            this.groupBox1.Controls.Add(this.numSTRMDW);
            this.groupBox1.Controls.Add(this.numLCKMD);
            this.groupBox1.Controls.Add(this.numSPDMD);
            this.groupBox1.Controls.Add(this.numDEXMD);
            this.groupBox1.Controls.Add(this.numINSMD);
            this.groupBox1.Controls.Add(this.numWILMD);
            this.groupBox1.Controls.Add(this.numSPRMD);
            this.groupBox1.Controls.Add(this.numSTAMD);
            this.groupBox1.Controls.Add(this.numSTRMD);
            this.groupBox1.Location = new System.Drawing.Point(64, 51);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(115, 220);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dominant";
            // 
            // numLCKMDW
            // 
            this.numLCKMDW.DecimalPlaces = 2;
            this.numLCKMDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numLCKMDW.Location = new System.Drawing.Point(63, 194);
            this.numLCKMDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numLCKMDW.Name = "numLCKMDW";
            this.numLCKMDW.Size = new System.Drawing.Size(46, 20);
            this.numLCKMDW.TabIndex = 15;
            this.numLCKMDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSPDMDW
            // 
            this.numSPDMDW.DecimalPlaces = 2;
            this.numSPDMDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSPDMDW.Location = new System.Drawing.Point(63, 170);
            this.numSPDMDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSPDMDW.Name = "numSPDMDW";
            this.numSPDMDW.Size = new System.Drawing.Size(46, 20);
            this.numSPDMDW.TabIndex = 14;
            this.numSPDMDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numDEXMDW
            // 
            this.numDEXMDW.DecimalPlaces = 2;
            this.numDEXMDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numDEXMDW.Location = new System.Drawing.Point(63, 144);
            this.numDEXMDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numDEXMDW.Name = "numDEXMDW";
            this.numDEXMDW.Size = new System.Drawing.Size(46, 20);
            this.numDEXMDW.TabIndex = 13;
            this.numDEXMDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numINSMDW
            // 
            this.numINSMDW.DecimalPlaces = 2;
            this.numINSMDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numINSMDW.Location = new System.Drawing.Point(63, 118);
            this.numINSMDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numINSMDW.Name = "numINSMDW";
            this.numINSMDW.Size = new System.Drawing.Size(46, 20);
            this.numINSMDW.TabIndex = 12;
            this.numINSMDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numWILMDW
            // 
            this.numWILMDW.DecimalPlaces = 2;
            this.numWILMDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numWILMDW.Location = new System.Drawing.Point(63, 92);
            this.numWILMDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numWILMDW.Name = "numWILMDW";
            this.numWILMDW.Size = new System.Drawing.Size(46, 20);
            this.numWILMDW.TabIndex = 11;
            this.numWILMDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSPRMDW
            // 
            this.numSPRMDW.DecimalPlaces = 2;
            this.numSPRMDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSPRMDW.Location = new System.Drawing.Point(63, 66);
            this.numSPRMDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSPRMDW.Name = "numSPRMDW";
            this.numSPRMDW.Size = new System.Drawing.Size(46, 20);
            this.numSPRMDW.TabIndex = 10;
            this.numSPRMDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSTAMDW
            // 
            this.numSTAMDW.DecimalPlaces = 2;
            this.numSTAMDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSTAMDW.Location = new System.Drawing.Point(63, 40);
            this.numSTAMDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSTAMDW.Name = "numSTAMDW";
            this.numSTAMDW.Size = new System.Drawing.Size(46, 20);
            this.numSTAMDW.TabIndex = 9;
            this.numSTAMDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numSTRMDW
            // 
            this.numSTRMDW.DecimalPlaces = 2;
            this.numSTRMDW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numSTRMDW.Location = new System.Drawing.Point(63, 14);
            this.numSTRMDW.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numSTRMDW.Name = "numSTRMDW";
            this.numSTRMDW.Size = new System.Drawing.Size(46, 20);
            this.numSTRMDW.TabIndex = 8;
            this.numSTRMDW.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // numLCKMD
            // 
            this.numLCKMD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numLCKMD.Location = new System.Drawing.Point(6, 195);
            this.numLCKMD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numLCKMD.Name = "numLCKMD";
            this.numLCKMD.Size = new System.Drawing.Size(51, 20);
            this.numLCKMD.TabIndex = 7;
            this.numLCKMD.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // numSPDMD
            // 
            this.numSPDMD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSPDMD.Location = new System.Drawing.Point(6, 170);
            this.numSPDMD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSPDMD.Name = "numSPDMD";
            this.numSPDMD.Size = new System.Drawing.Size(51, 20);
            this.numSPDMD.TabIndex = 6;
            this.numSPDMD.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // numDEXMD
            // 
            this.numDEXMD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numDEXMD.Location = new System.Drawing.Point(6, 144);
            this.numDEXMD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numDEXMD.Name = "numDEXMD";
            this.numDEXMD.Size = new System.Drawing.Size(51, 20);
            this.numDEXMD.TabIndex = 5;
            this.numDEXMD.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // numINSMD
            // 
            this.numINSMD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numINSMD.Location = new System.Drawing.Point(6, 118);
            this.numINSMD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numINSMD.Name = "numINSMD";
            this.numINSMD.Size = new System.Drawing.Size(51, 20);
            this.numINSMD.TabIndex = 4;
            this.numINSMD.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // numWILMD
            // 
            this.numWILMD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numWILMD.Location = new System.Drawing.Point(6, 92);
            this.numWILMD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numWILMD.Name = "numWILMD";
            this.numWILMD.Size = new System.Drawing.Size(51, 20);
            this.numWILMD.TabIndex = 3;
            this.numWILMD.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // numSPRMD
            // 
            this.numSPRMD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSPRMD.Location = new System.Drawing.Point(6, 66);
            this.numSPRMD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSPRMD.Name = "numSPRMD";
            this.numSPRMD.Size = new System.Drawing.Size(51, 20);
            this.numSPRMD.TabIndex = 2;
            this.numSPRMD.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // numSTAMD
            // 
            this.numSTAMD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSTAMD.Location = new System.Drawing.Point(6, 40);
            this.numSTAMD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSTAMD.Name = "numSTAMD";
            this.numSTAMD.Size = new System.Drawing.Size(51, 20);
            this.numSTAMD.TabIndex = 1;
            this.numSTAMD.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // numSTRMD
            // 
            this.numSTRMD.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSTRMD.Location = new System.Drawing.Point(7, 14);
            this.numSTRMD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSTRMD.Name = "numSTRMD";
            this.numSTRMD.Size = new System.Drawing.Size(50, 20);
            this.numSTRMD.TabIndex = 0;
            this.numSTRMD.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(3, 253);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(31, 13);
            this.label44.TabIndex = 8;
            this.label44.Text = "Luck";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(3, 228);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(38, 13);
            this.label42.TabIndex = 7;
            this.label42.Text = "Speed";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(3, 202);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(48, 13);
            this.label33.TabIndex = 6;
            this.label33.Text = "Dexterity";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(5, 176);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(38, 13);
            this.label32.TabIndex = 5;
            this.label32.Text = "Insight";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(5, 150);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 13);
            this.label28.TabIndex = 4;
            this.label28.Text = "Willpower";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 124);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(30, 13);
            this.label27.TabIndex = 3;
            this.label27.Text = "Spirit";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(5, 98);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(45, 13);
            this.label26.TabIndex = 2;
            this.label26.Text = "Stamina";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(5, 72);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(47, 13);
            this.label25.TabIndex = 1;
            this.label25.Text = "Strength";
            // 
            // mainBreedBox
            // 
            this.mainBreedBox.FormattingEnabled = true;
            this.mainBreedBox.Items.AddRange(new object[] {
            "Bear",
            "Feline",
            "Fox",
            "Gorilla",
            "Hawk",
            "Lizard",
            "Spider",
            "Wolf"});
            this.mainBreedBox.Location = new System.Drawing.Point(64, 24);
            this.mainBreedBox.Name = "mainBreedBox";
            this.mainBreedBox.Size = new System.Drawing.Size(121, 21);
            this.mainBreedBox.TabIndex = 0;
            // 
            // statusStrip4
            // 
            this.statusStrip4.Location = new System.Drawing.Point(0, 429);
            this.statusStrip4.Name = "statusStrip4";
            this.statusStrip4.Size = new System.Drawing.Size(651, 22);
            this.statusStrip4.TabIndex = 0;
            this.statusStrip4.Text = "statusStrip4";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.testcombine1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(651, 451);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "CombineFamiliar";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // testcombine1
            // 
            this.testcombine1.Location = new System.Drawing.Point(154, 177);
            this.testcombine1.Name = "testcombine1";
            this.testcombine1.Size = new System.Drawing.Size(273, 23);
            this.testcombine1.TabIndex = 0;
            this.testcombine1.Text = "Test Fam Combine";
            this.testcombine1.UseVisualStyleBackColor = true;
            this.testcombine1.Click += new System.EventHandler(this.testcombine1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 504);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Familiar Breed Breeder";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl.ResumeLayout(false);
            this.BreedsPage.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.breed_luck_variance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_speed_variance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_insight_variance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_dexterity_variance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_willpower_variance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_stamina_variance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_spirit_variance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_strength_variance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_luck_rec_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_speed_rec_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_insight_rec_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_dexterity_rec_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_willpower_rec_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_stamina_rec_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_spirit_rec_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_strength_rec_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_luck_dom_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_speed_dom_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_insight_dom_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_dexterity_dom_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_willpower_dom_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_stamina_dom_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_spirit_dom_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_strength_dom_weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_mp_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_luck_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_hp_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_speed_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_insight_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_dexterity_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_willpower_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_stam_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_spirit_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.breed_strength_num)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ElementsPage.ResumeLayout(false);
            this.ElementsPage.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.elements_mp_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_luck_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_hp_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_speed_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_insight_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_dexterity_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_willpower_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_stamina_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_spirit_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elements_strength_num)).EndInit();
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.Familiars.ResumeLayout(false);
            this.Familiars.PerformLayout();
            this.MakeFamiliar.ResumeLayout(false);
            this.MakeFamiliar.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numLCKSRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRSR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDSRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTASR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXSRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRSR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSSRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILSR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILSRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSSR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRSRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXSR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTASRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDSR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRSRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKSR)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numLCKSDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDSDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXSDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSSDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILSDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRSDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTASDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRSDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTASD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRSD)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numLCKMRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRMR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDMRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTAMR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXMRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRMR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSMRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILMR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILMRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSMR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRMRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXMR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTAMRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDMR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRMRW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKMR)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numLCKMDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDMDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXMDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSMDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILMDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRMDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTAMDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRMDW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLCKMD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPDMD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDEXMD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numINSMD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWILMD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSPRMD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTAMD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSTRMD)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage BreedsPage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabPage ElementsPage;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.TabPage Familiars;
        private System.Windows.Forms.StatusStrip statusStrip3;
        private System.Windows.Forms.TabPage MakeFamiliar;
        private System.Windows.Forms.StatusStrip statusStrip4;
        private System.Windows.Forms.RadioButton breed_wolf_button;
        private System.Windows.Forms.RadioButton breed_spider_button;
        private System.Windows.Forms.RadioButton breed_lizard_button;
        private System.Windows.Forms.RadioButton breed_hawk_button;
        private System.Windows.Forms.RadioButton breed_gorilla_button;
        private System.Windows.Forms.RadioButton breed_fox_button;
        private System.Windows.Forms.RadioButton breed_feline_button;
        private System.Windows.Forms.RadioButton breed_bear_button;
        private System.Windows.Forms.Label magic_points_label;
        private System.Windows.Forms.Label health_label;
        private System.Windows.Forms.Label luck_label;
        private System.Windows.Forms.Label speed_label;
        private System.Windows.Forms.Label dexterity_label;
        private System.Windows.Forms.Label insight_label;
        private System.Windows.Forms.Label willpower_label;
        private System.Windows.Forms.Label spirit_label;
        private System.Windows.Forms.Label stamina_label;
        private System.Windows.Forms.Label strength_label;
        private System.Windows.Forms.Button SaveBreedButton;
        private System.Windows.Forms.Button LoadBreedButton;
        private System.Windows.Forms.NumericUpDown breed_mp_num;
        private System.Windows.Forms.NumericUpDown breed_luck_num;
        private System.Windows.Forms.NumericUpDown breed_hp_num;
        private System.Windows.Forms.NumericUpDown breed_speed_num;
        private System.Windows.Forms.NumericUpDown breed_insight_num;
        private System.Windows.Forms.NumericUpDown breed_dexterity_num;
        private System.Windows.Forms.NumericUpDown breed_willpower_num;
        private System.Windows.Forms.NumericUpDown breed_stam_num;
        private System.Windows.Forms.NumericUpDown breed_spirit_num;
        private System.Windows.Forms.NumericUpDown breed_strength_num;
        private System.Windows.Forms.Button SaveBreeds;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.RadioButton element_water_button;
        private System.Windows.Forms.RadioButton element_shadow_button;
        private System.Windows.Forms.RadioButton element_metal_button;
        private System.Windows.Forms.RadioButton element_light_button;
        private System.Windows.Forms.RadioButton element_fire_button;
        private System.Windows.Forms.RadioButton element_electric_button;
        private System.Windows.Forms.RadioButton element_earth_button;
        private System.Windows.Forms.RadioButton element_air_button;
        private System.Windows.Forms.Button save_element_button;
        private System.Windows.Forms.Button elements_save_button;
        private System.Windows.Forms.Button elements_load_button;
        private System.Windows.Forms.NumericUpDown elements_mp_num;
        private System.Windows.Forms.NumericUpDown elements_luck_num;
        private System.Windows.Forms.NumericUpDown elements_hp_num;
        private System.Windows.Forms.NumericUpDown elements_speed_num;
        private System.Windows.Forms.NumericUpDown elements_insight_num;
        private System.Windows.Forms.NumericUpDown elements_dexterity_num;
        private System.Windows.Forms.NumericUpDown elements_willpower_num;
        private System.Windows.Forms.NumericUpDown elements_stamina_num;
        private System.Windows.Forms.NumericUpDown elements_spirit_num;
        private System.Windows.Forms.NumericUpDown elements_strength_num;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ComboBox FamiliarDropDown;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label mp_num;
        private System.Windows.Forms.Label hp_num;
        private System.Windows.Forms.Label luck_num;
        private System.Windows.Forms.Label speed_num;
        private System.Windows.Forms.Label dexterity_num;
        private System.Windows.Forms.Label insight_num;
        private System.Windows.Forms.Label willpower_num;
        private System.Windows.Forms.Label spirit_num;
        private System.Windows.Forms.Label stamina_num;
        private System.Windows.Forms.Label strength_num;
        private System.Windows.Forms.Label element_name_label;
        private System.Windows.Forms.Label sub_breed_name_label;
        private System.Windows.Forms.Label main_breed_name_label;
        private System.Windows.Forms.Label name_label;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label effect_one_damage_two_label;
        private System.Windows.Forms.Label hit_chance_label;
        private System.Windows.Forms.Label effect_one_damage_one_label;
        private System.Windows.Forms.Label damage_label;
        private System.Windows.Forms.Label effect_one_duration_label;
        private System.Windows.Forms.Label cooldown_label;
        private System.Windows.Forms.Label effect_one_name_label;
        private System.Windows.Forms.Label crit_chance_label;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox attack_drop_down;
        private System.Windows.Forms.Label mp_cost_label;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label animation_label;
        private System.Windows.Forms.Label distance_label;
        private System.Windows.Forms.Label effect_three_damage_two_label;
        private System.Windows.Forms.Label effect_three_damage_one_label;
        private System.Windows.Forms.Label effect_three_duration_label;
        private System.Windows.Forms.Label effect_three_name_label;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label effect_two_damage_two_label;
        private System.Windows.Forms.Label effect_two_damage_one_label;
        private System.Windows.Forms.Label effect_two_duration_label;
        private System.Windows.Forms.Label effect_two_name_label;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label_1;
        private System.Windows.Forms.Button save_familiars;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox subBreedBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numLCKMD;
        private System.Windows.Forms.NumericUpDown numSPDMD;
        private System.Windows.Forms.NumericUpDown numDEXMD;
        private System.Windows.Forms.NumericUpDown numINSMD;
        private System.Windows.Forms.NumericUpDown numWILMD;
        private System.Windows.Forms.NumericUpDown numSPRMD;
        private System.Windows.Forms.NumericUpDown numSTAMD;
        private System.Windows.Forms.NumericUpDown numSTRMD;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox mainBreedBox;
        private System.Windows.Forms.NumericUpDown numSTRMDW;
        private System.Windows.Forms.NumericUpDown numLCKMRW;
        private System.Windows.Forms.NumericUpDown numSTRMR;
        private System.Windows.Forms.NumericUpDown numSPDMRW;
        private System.Windows.Forms.NumericUpDown numSTAMR;
        private System.Windows.Forms.NumericUpDown numDEXMRW;
        private System.Windows.Forms.NumericUpDown numSPRMR;
        private System.Windows.Forms.NumericUpDown numINSMRW;
        private System.Windows.Forms.NumericUpDown numWILMR;
        private System.Windows.Forms.NumericUpDown numWILMRW;
        private System.Windows.Forms.NumericUpDown numINSMR;
        private System.Windows.Forms.NumericUpDown numSPRMRW;
        private System.Windows.Forms.NumericUpDown numDEXMR;
        private System.Windows.Forms.NumericUpDown numSTAMRW;
        private System.Windows.Forms.NumericUpDown numSPDMR;
        private System.Windows.Forms.NumericUpDown numSTRMRW;
        private System.Windows.Forms.NumericUpDown numLCKMR;
        private System.Windows.Forms.NumericUpDown numLCKMDW;
        private System.Windows.Forms.NumericUpDown numSPDMDW;
        private System.Windows.Forms.NumericUpDown numDEXMDW;
        private System.Windows.Forms.NumericUpDown numINSMDW;
        private System.Windows.Forms.NumericUpDown numWILMDW;
        private System.Windows.Forms.NumericUpDown numSPRMDW;
        private System.Windows.Forms.NumericUpDown numSTAMDW;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numLCKSRW;
        private System.Windows.Forms.NumericUpDown numSTRSR;
        private System.Windows.Forms.NumericUpDown numSPDSRW;
        private System.Windows.Forms.NumericUpDown numSTASR;
        private System.Windows.Forms.NumericUpDown numDEXSRW;
        private System.Windows.Forms.NumericUpDown numSPRSR;
        private System.Windows.Forms.NumericUpDown numINSSRW;
        private System.Windows.Forms.NumericUpDown numWILSR;
        private System.Windows.Forms.NumericUpDown numWILSRW;
        private System.Windows.Forms.NumericUpDown numINSSR;
        private System.Windows.Forms.NumericUpDown numSPRSRW;
        private System.Windows.Forms.NumericUpDown numDEXSR;
        private System.Windows.Forms.NumericUpDown numSTASRW;
        private System.Windows.Forms.NumericUpDown numSPDSR;
        private System.Windows.Forms.NumericUpDown numSTRSRW;
        private System.Windows.Forms.NumericUpDown numLCKSR;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown numLCKSDW;
        private System.Windows.Forms.NumericUpDown numSPDSDW;
        private System.Windows.Forms.NumericUpDown numDEXSDW;
        private System.Windows.Forms.NumericUpDown numINSSDW;
        private System.Windows.Forms.NumericUpDown numWILSDW;
        private System.Windows.Forms.NumericUpDown numSPRSDW;
        private System.Windows.Forms.NumericUpDown numSTASDW;
        private System.Windows.Forms.NumericUpDown numSTRSDW;
        private System.Windows.Forms.NumericUpDown numLCKSD;
        private System.Windows.Forms.NumericUpDown numSPDSD;
        private System.Windows.Forms.NumericUpDown numDEXSD;
        private System.Windows.Forms.NumericUpDown numINSSD;
        private System.Windows.Forms.NumericUpDown numWILSD;
        private System.Windows.Forms.NumericUpDown numSPRSD;
        private System.Windows.Forms.NumericUpDown numSTASD;
        private System.Windows.Forms.NumericUpDown numSTRSD;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label threadLCK;
        private System.Windows.Forms.Label threadSPD;
        private System.Windows.Forms.Label threadDEX;
        private System.Windows.Forms.Label threadINS;
        private System.Windows.Forms.Label threadWIL;
        private System.Windows.Forms.Label threadSPR;
        private System.Windows.Forms.Label threadSTA;
        private System.Windows.Forms.Label threadSTR;
        private System.Windows.Forms.RadioButton threadLCKSR;
        private System.Windows.Forms.RadioButton threadLCKSD;
        private System.Windows.Forms.RadioButton threadLCKMR;
        private System.Windows.Forms.RadioButton threadLCKMD;
        private System.Windows.Forms.RadioButton threadSPDSR;
        private System.Windows.Forms.RadioButton threadSPDSD;
        private System.Windows.Forms.RadioButton threadSPDMR;
        private System.Windows.Forms.RadioButton threadSPDMD;
        private System.Windows.Forms.RadioButton threadDEXSR;
        private System.Windows.Forms.RadioButton threadDEXSD;
        private System.Windows.Forms.RadioButton threadDEXMR;
        private System.Windows.Forms.RadioButton threadDEXMD;
        private System.Windows.Forms.RadioButton threadINSSR;
        private System.Windows.Forms.RadioButton threadINSSD;
        private System.Windows.Forms.RadioButton threadINSMR;
        private System.Windows.Forms.RadioButton threadINSMD;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.RadioButton threadWILSR;
        private System.Windows.Forms.RadioButton threadWILSD;
        private System.Windows.Forms.RadioButton threadWILMR;
        private System.Windows.Forms.RadioButton threadWILMD;
        private System.Windows.Forms.RadioButton threadSPRSR;
        private System.Windows.Forms.RadioButton threadSPRSD;
        private System.Windows.Forms.RadioButton threadSPRMR;
        private System.Windows.Forms.RadioButton threadSPRMD;
        private System.Windows.Forms.RadioButton threadSTASR;
        private System.Windows.Forms.RadioButton threadSTASD;
        private System.Windows.Forms.RadioButton threadSTAMR;
        private System.Windows.Forms.RadioButton threadSTAMD;
        private System.Windows.Forms.RadioButton threadSTRSR;
        private System.Windows.Forms.RadioButton threadSTRSD;
        private System.Windows.Forms.RadioButton threadSTRMR;
        private System.Windows.Forms.RadioButton threadSTRMD;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.NumericUpDown breed_luck_variance;
        private System.Windows.Forms.NumericUpDown breed_speed_variance;
        private System.Windows.Forms.NumericUpDown breed_insight_variance;
        private System.Windows.Forms.NumericUpDown breed_dexterity_variance;
        private System.Windows.Forms.NumericUpDown breed_willpower_variance;
        private System.Windows.Forms.NumericUpDown breed_stamina_variance;
        private System.Windows.Forms.NumericUpDown breed_spirit_variance;
        private System.Windows.Forms.NumericUpDown breed_strength_variance;
        private System.Windows.Forms.NumericUpDown breed_luck_rec_weight;
        private System.Windows.Forms.NumericUpDown breed_speed_rec_weight;
        private System.Windows.Forms.NumericUpDown breed_insight_rec_weight;
        private System.Windows.Forms.NumericUpDown breed_dexterity_rec_weight;
        private System.Windows.Forms.NumericUpDown breed_willpower_rec_weight;
        private System.Windows.Forms.NumericUpDown breed_stamina_rec_weight;
        private System.Windows.Forms.NumericUpDown breed_spirit_rec_weight;
        private System.Windows.Forms.NumericUpDown breed_strength_rec_weight;
        private System.Windows.Forms.NumericUpDown breed_luck_dom_weight;
        private System.Windows.Forms.NumericUpDown breed_speed_dom_weight;
        private System.Windows.Forms.NumericUpDown breed_insight_dom_weight;
        private System.Windows.Forms.NumericUpDown breed_dexterity_dom_weight;
        private System.Windows.Forms.NumericUpDown breed_willpower_dom_weight;
        private System.Windows.Forms.NumericUpDown breed_stamina_dom_weight;
        private System.Windows.Forms.NumericUpDown breed_spirit_dom_weight;
        private System.Windows.Forms.NumericUpDown breed_strength_dom_weight;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button testcombine1;
    }
}

