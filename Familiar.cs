﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Familiar_Breed_Breeder
{
    class Familiar
    {
        enum Fibers {Main_Dominant, Main_Recessive, Sub_Dominant, Sub_Recessive, MAX_FIBERS};
        public enum Stats { Strength, Stamina, Spirit, Willpower, Insight, Dexterity, Speed, Luck, MAX_STATS };
        public int base_value = 80;
        public int index;
        public string name;

        public struct Winner
        {
            public int index;
            public decimal value;
        }

        public decimal[] stats;

        public decimal hp;
        public decimal mp;

        public Breed main_breed;
        public Breed sub_breed;
        public Element element;
        public int[] WinningFibers = { (int)Fibers.Main_Dominant,
                                       (int)Fibers.Main_Dominant,
                                       (int)Fibers.Main_Dominant,
                                       (int)Fibers.Main_Dominant,
                                       (int)Fibers.Main_Dominant,
                                       (int)Fibers.Main_Dominant,
                                       (int)Fibers.Main_Dominant
                                     };

       

        public Familiar()
        {
            index = -1;
            name = "default";
            stats = new decimal[8];
            main_breed = new Breed();
            sub_breed = new Breed();
 
            hp = base_value;
            mp = base_value;
        }

        public Familiar(int _index)
        {
            index = _index;
            name = "Create Me";
            stats = new decimal[8];
            hp = base_value;
            mp = base_value;
        }
        public Familiar(String _name, Breed _main_breed, Breed _sub_breed, Element _element)
        {
            
            name = _name;
            stats = new decimal[8];
            hp = base_value;
            mp = base_value;
            main_breed = _main_breed;
            sub_breed = _sub_breed;
            element = _element;
            index = (main_breed.index * 64) + sub_breed.index + (element.index*8) + 1;
            Apply_Stats();


        }
        
        public Familiar(Familiar first_familiar, Familiar second_familiar)
        {
            stats = new decimal[8];
            int[] winners = { -1, -1, -1, -1, -1, -1, -1, -1 }; //needs to expand if stats grows
            int[] counts = { 0, 0, 0, 0 };
            int first_place_breed = -1;
            int second_place_breed = -1;

            Breed[] weaves = { first_familiar.main_breed, first_familiar.sub_breed, second_familiar.main_breed, second_familiar.sub_breed };

            Random random = new Random();
            for (int i = 0; i < (int)Stats.MAX_STATS; i++)
            {
                winners[i] = ThreadTourny(weaves[0].breed_stat[i], weaves[1].breed_stat[i], weaves[2].breed_stat[i], weaves[3].breed_stat[i],random);
            }

            for (int i = 0; i < (int)Stats.MAX_STATS; i++)
            {
                if (winners[i] == 0 || winners[i] == 1)
                    counts[0]++;
                if (winners[i] == 2 || winners[i] == 3)
                    counts[1]++;
                if (winners[i] == 4 || winners[i] == 5)
                    counts[2]++;
                if (winners[i] == 6 || winners[i] == 7)
                    counts[0]++;
            }

            if (counts[0] >= counts[1])
            {
                first_place_breed = 0;
            }
            else
            {
                first_place_breed = 1;
            }

            if(counts[first_place_breed] < counts[2])
            {
                first_place_breed = 2;
            }

            if(counts[first_place_breed] < counts[3])
            {
                first_place_breed = 3;
            }

            counts[first_place_breed] = -1;

            if (counts[0] >= counts[1])
            {
                second_place_breed = 0;
            }
            else
            {
                second_place_breed = 1;
            }

            if (counts[second_place_breed] < counts[2])
            {
                second_place_breed = 2;
            }

            if (counts[second_place_breed] < counts[3])
            {
                second_place_breed = 3;
            }

            this.main_breed = weaves[first_place_breed];
            this.sub_breed = weaves[second_place_breed];

            if (first_place_breed == 0 || first_place_breed == 1)
                this.element = first_familiar.element;
            else
                this.element = second_familiar.element;

            this.index = (main_breed.index * 64) + sub_breed.index + (element.index * 8) + 1;

            this.Apply_Stats();

            //need to get name out of first area

 
           
        }

        private int ThreadTourny( Spool first_mb, Spool first_sb, Spool second_mb, Spool second_sb, Random random)
        {
            Winner first_winner, second_winner,third_winner,fourth_winner;

            decimal first_total = (first_mb.dominant.weight*100) + (first_mb.recessive.weight*100);
            decimal second_total = (first_sb.dominant.weight*100) + (first_sb.recessive.weight*100);
            decimal third_total = (second_mb.dominant.weight*100) + (second_mb.recessive.weight*100);
            decimal fourth_total = (second_sb.dominant.weight*100) + (second_sb.recessive.weight*100);

            
       //     Random random = new Random();
            int decider = random.Next(0, (int)first_total);
            if (decider < first_mb.dominant.weight * 100)
            {
                first_winner.index = 0;
                first_winner.value = first_mb.dominant.weight;
            }
            else
            {
                first_winner.index = 1;
                first_winner.value = first_mb.recessive.weight;
            }

            decider = random.Next(0, (int)second_total);

            if(decider < first_sb.dominant.weight*100)
            {
                second_winner.index = 2;
                second_winner.value = first_sb.dominant.weight;
            }
            else
            {
                second_winner.index = 3;
                second_winner.value = first_sb.recessive.weight;
            }

            decider = random.Next(0, (int)third_total);

            if (decider < second_mb.dominant.weight * 100)
            {
                third_winner.index = 4;
                third_winner.value = second_mb.dominant.weight;
            }
            else
            {
                third_winner.index = 5;
                third_winner.value = second_mb.recessive.weight;
            }

            decider = random.Next(0, (int)fourth_total);

            if (decider < first_sb.dominant.weight * 100)
            {
                fourth_winner.index = 6;
                fourth_winner.value = first_sb.dominant.weight;
            }
            else
            {
                fourth_winner.index = 7;
                fourth_winner.value = first_sb.recessive.weight;
            }


            //have 4 winners, now next stuff

            first_total = (first_winner.value * 100 + second_winner.value * 100);
            second_total = (third_winner.value * 100 + fourth_winner.value * 100);

            decider = random.Next(0, (int)first_total);

            if(decider >= first_winner.value*100)
            {
                first_winner = second_winner;
            }

            decider = random.Next(0, (int)second_total);

            if(decider < third_winner.value*100)
            {
                second_winner = third_winner;
            }
            else
            {
                second_winner = fourth_winner;
            }
            
            //have final two

            first_total = (first_winner.value * 100 + second_winner.value * 100);
            
            decider = random.Next(0,(int)first_total);

            if(decider < first_winner.value*100)
            {
                return first_winner.index;
            }
            else
            {
                return second_winner.index;
            }
            
        }
       

        private decimal GetStatFromSpool(decimal main_dom, decimal main_rec, decimal sub_dom, decimal sub_rec,decimal elem_val)
        {
            decimal main_stat = main_dom;
            decimal sub_stat = (main_rec + sub_dom + sub_rec) * 3;
            sub_stat = sub_stat / 3;
            main_stat += sub_stat;
            main_stat *= elem_val;
            return main_stat;

        }

        public void Apply_Stats()
        {
            decimal sub_ratio = (decimal).25;

            stats[(int)Stats.Strength] = base_value + GetStatFromSpool(main_breed.breed_stat[(int)Stats.Strength].dominant.value, main_breed.breed_stat[(int)Stats.Strength].recessive.value,
                sub_breed.breed_stat[(int)Stats.Strength].dominant.value, sub_breed.breed_stat[(int)Stats.Strength].recessive.value, element.stats[(int)Stats.Strength]);
            stats[(int)Stats.Stamina] = base_value + GetStatFromSpool(main_breed.breed_stat[(int)Stats.Stamina].dominant.value, main_breed.breed_stat[(int)Stats.Stamina].recessive.value,
                sub_breed.breed_stat[(int)Stats.Stamina].dominant.value, sub_breed.breed_stat[(int)Stats.Stamina].recessive.value, element.stats[(int)Stats.Stamina]);
            stats[(int)Stats.Spirit] = base_value + GetStatFromSpool(main_breed.breed_stat[(int)Stats.Spirit].dominant.value, main_breed.breed_stat[(int)Stats.Spirit].recessive.value,
                sub_breed.breed_stat[(int)Stats.Spirit].dominant.value, sub_breed.breed_stat[(int)Stats.Spirit].recessive.value, element.stats[(int)Stats.Spirit]);
            stats[(int)Stats.Willpower] = base_value + GetStatFromSpool(main_breed.breed_stat[(int)Stats.Willpower].dominant.value, main_breed.breed_stat[(int)Stats.Willpower].recessive.value,
                sub_breed.breed_stat[(int)Stats.Willpower].dominant.value, sub_breed.breed_stat[(int)Stats.Willpower].recessive.value, element.stats[(int)Stats.Willpower]);
            stats[(int)Stats.Insight] = base_value + GetStatFromSpool(main_breed.breed_stat[(int)Stats.Insight].dominant.value, main_breed.breed_stat[(int)Stats.Insight].recessive.value,
                sub_breed.breed_stat[(int)Stats.Insight].dominant.value, sub_breed.breed_stat[(int)Stats.Insight].recessive.value, element.stats[(int)Stats.Insight]);
            stats[(int)Stats.Dexterity] = base_value + GetStatFromSpool(main_breed.breed_stat[(int)Stats.Dexterity].dominant.value, main_breed.breed_stat[(int)Stats.Dexterity].recessive.value,
                sub_breed.breed_stat[(int)Stats.Dexterity].dominant.value, sub_breed.breed_stat[(int)Stats.Dexterity].recessive.value, element.stats[(int)Stats.Dexterity]);
            stats[(int)Stats.Speed] = base_value + GetStatFromSpool(main_breed.breed_stat[(int)Stats.Speed].dominant.value, main_breed.breed_stat[(int)Stats.Speed].recessive.value,
                sub_breed.breed_stat[(int)Stats.Speed].dominant.value, sub_breed.breed_stat[(int)Stats.Speed].recessive.value, element.stats[(int)Stats.Speed]);
            stats[(int)Stats.Luck] = base_value + GetStatFromSpool(main_breed.breed_stat[(int)Stats.Luck].dominant.value, main_breed.breed_stat[(int)Stats.Luck].recessive.value,
                sub_breed.breed_stat[(int)Stats.Luck].dominant.value, sub_breed.breed_stat[(int)Stats.Luck].recessive.value, element.stats[(int)Stats.Luck]);
            hp = (base_value + main_breed.hp + (sub_breed.hp * sub_ratio)) * element.hp;
            mp = (base_value + main_breed.mp + (sub_breed.mp * sub_ratio)) * element.mp;
          
        }

        public Familiar CreateBreed(Familiar second)
        {
            Familiar temp = new Familiar();
            return temp;
        }


    }
}
