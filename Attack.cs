﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Familiar_Breed_Breeder
{
    public enum animation { Bite_1, Bite_2, Claw_1, Claw_2, Roar, Taunt};
    public enum effect {None,Blind,Focus,Charm,Fear,Knock_Back,Pull,Burn,Poison,Smite,Refresh,
        Envigor,Heal,Slow,Quick,Cripple,Enrage,Haste,Sloth,Sunder,Fortify,Confuse,Stun}
    public enum distance { Close, Middle, Far, Self };

    class Attack
    {
        public String name;
        public int index;
        public distance attack_distance;
        public animation attack_animation;
        public decimal cooldown;
        public decimal damage;
        public decimal hit_chance;
        public decimal crit_chance;
        public decimal mp_cost;
        public effect first_effect;
        public decimal first_duration;
        public decimal first_damage;
        public decimal first_second_damage;
        public effect second_effect;
        public decimal second_duration;
        public decimal second_damage;
        public decimal second_second_damage;
        public effect third_effect;
        public decimal third_duration;
        public decimal third_damage;
        public decimal third_second_damage;

        public Attack()
        {
            name = "Attack";
            attack_distance = distance.Close;
            index = -1;
            attack_animation = animation.Bite_1;
            cooldown = 3;
            damage = 9000;
            hit_chance = .75m;
            crit_chance = .15m;
            mp_cost = 30;
            first_effect = effect.Haste;
            first_duration = 8;
            first_damage = 8;
            first_second_damage = 1;
            second_effect = effect.Knock_Back;
            second_duration = 2;
            second_damage = 7;
            second_second_damage = 8;
            third_effect = effect.Smite;
            third_duration = 1;
            third_damage = 5;
            third_second_damage = 10;
        }
    }
}
