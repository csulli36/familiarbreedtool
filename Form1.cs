﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Familiar_Breed_Breeder
{
    public partial class Form1 : Form
    {

        //breeds
        SortedDictionary<String, Breed> base_breed_dic = new SortedDictionary<String, Breed>();
        Breed[] breed_array = new Breed[8];
        String[,] breed_name;

        //elements

        SortedDictionary<String, Element> base_element_dic = new SortedDictionary<String, Element>();
        Element[] element_array = new Element[8];
        String[] element_name = new String[8];

        //familiars
        SortedDictionary<String, Familiar> base_familiar_dic = new SortedDictionary<String, Familiar>();
        Familiar[] familiar_array = new Familiar[512];


        public Form1()
        {
            InitializeComponent();

            //breed initialization
            breed_array[0] = new Breed(0,"Bear");
            breed_array[1] = new Breed(1,"Feline");
            breed_array[2] = new Breed(2,"Fox");
            breed_array[3] = new Breed(3,"Gorilla");
            breed_array[4] = new Breed(4,"Hawk");
            breed_array[5] = new Breed(5,"Lizard");
            breed_array[6] = new Breed(6,"Spider");
            breed_array[7] = new Breed(7,"Wolf");

            breed_name = new String[8, 8] {
                                            { "Bear", "Ursal", "Ainu", "Fomorian","Onikuma","Pangul","Koyukon","Kodiak"},
                                            { "Bearcat", "Cat", "Serval", "Sabertooth","Lamassu","Lynx","Mehit","Nemea"},
                                            { "Corsac", "Fennix", "Fox", "Kitsune","Vulpec","Culpeo","Kit","Dusicyon"},
                                            { "Goldmane", "Mandril", "Gibbon", "Gorilla","Nikko","Wukong","Ngi","Silverback"},
                                            { "Roc", "Kestrel", "Eagle", "Harpy","Hawk","Vulture","Crow","Falcon"},
                                            { "Gila", "Iguana", "Tegu", "Zila","Draco","Lizard","Mintor","Komodo"},                   
                                            { "Tarantula", "Orb Weaver", "Recluse", "Widow","Air Jumping Spider","Funnelweb","Spider","Wolf Spider"},
                                            { "Direwolf", "Hyena", "Dingo", "Worg","Amarok","Fenrus","Lupos","Wolf"},
                                          };
            
            base_breed_dic.Add("Bear", breed_array[0]);
            base_breed_dic.Add("Feline", breed_array[1]);
            base_breed_dic.Add("Fox", breed_array[2]);
            base_breed_dic.Add("Gorilla", breed_array[3]);
            base_breed_dic.Add("Hawk", breed_array[4]);
            base_breed_dic.Add("Lizard", breed_array[5]);
            base_breed_dic.Add("Spider", breed_array[6]);
            base_breed_dic.Add("Wolf", breed_array[7]);

       //     LoadBreeds();

            //element initialization
            element_array[0] = new Element(0,"Air");
            element_array[1] = new Element(1,"Earth");
            element_array[2] = new Element(2,"Electric");
            element_array[3] = new Element(3,"Fire");
            element_array[4] = new Element(4,"Light");
            element_array[5] = new Element(5,"Metal");
            element_array[6] = new Element(6,"Shadow");
            element_array[7] = new Element(7,"Water");

            element_name[0] = "Air";
            element_name[1] = "Earth";
            element_name[2] = "Electric";
            element_name[3] = "Fire";
            element_name[4] = "Light";
            element_name[5] = "Metal";
            element_name[6] = "Shadow";
            element_name[7] = "Water";

            base_element_dic.Add("Air", element_array[0]);
            base_element_dic.Add("Earth", element_array[1]);
            base_element_dic.Add("Electric", element_array[2]);
            base_element_dic.Add("Fire", element_array[3]);
            base_element_dic.Add("Light", element_array[4]);
            base_element_dic.Add("Metal", element_array[5]);
            base_element_dic.Add("Shadow", element_array[6]);
            base_element_dic.Add("Water", element_array[7]);

      //    Load_Elements();

            //familiar initialization

            for (int i = 0; i < familiar_array.Length; i++)
            {
                int main_breed = i / 64;
                int sub_breed = (i - main_breed * 64) % 8;
                int element = ((i - main_breed * 64) - sub_breed) / 8;
                int index = i + 1;
                string name = element_name[element] + " " + breed_name[main_breed, sub_breed];
                familiar_array[i] = new Familiar(name,breed_array[main_breed],breed_array[sub_breed],element_array[element]);
                base_familiar_dic.Add(name, familiar_array[i]);
                FamiliarDropDown.Items.Add(name);
            }

  
            toolStripStatusLabel1.Text = "512 MARKUS, WHY?!?!!";

        }

        #region things im not using
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        } 
        #endregion

        #region Breed Tab

        private void BreedButtonChange(string breed_name)
        {
            breed_strength_num.Value =          base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].dominant.value;
            breed_strength_dom_weight.Value =   base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].dominant.weight;
            breed_strength_num.Value =          base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].recessive.value;
            breed_strength_rec_weight.Value =   base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].recessive.weight;
            breed_strength_variance.Value =     base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].dominant.variance;
            breed_strength_variance.Value =     base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].recessive.variance;

            breed_stam_num.Value =              base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].dominant.value;
            breed_stamina_dom_weight.Value =    base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].dominant.weight;
            breed_stam_num.Value =              base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].recessive.value;
            breed_stamina_rec_weight.Value =    base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].recessive.weight;
            breed_stamina_variance.Value =      base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].dominant.variance;
            breed_stamina_variance.Value =      base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].recessive.variance;

            breed_spirit_num.Value =            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].dominant.value;
            breed_spirit_dom_weight.Value =     base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].dominant.weight;
            breed_spirit_num.Value =            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].recessive.value;
            breed_spirit_rec_weight.Value =     base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].recessive.weight;
            breed_spirit_variance.Value =       base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].dominant.variance;
            breed_spirit_variance.Value =       base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].recessive.variance;

            breed_willpower_num.Value =         base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].dominant.value;
            breed_willpower_dom_weight.Value =  base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].dominant.weight;
            breed_willpower_num.Value =         base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].recessive.value;
            breed_willpower_rec_weight.Value =  base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].recessive.weight;
            breed_willpower_variance.Value =    base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].dominant.variance;
            breed_willpower_variance.Value =    base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].recessive.variance;

            breed_insight_num.Value =           base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].dominant.value;
            breed_insight_dom_weight.Value =    base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].dominant.weight;
            breed_insight_num.Value =           base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].recessive.value;
            breed_insight_rec_weight.Value =    base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].recessive.weight;
            breed_insight_variance.Value =      base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].dominant.variance;
            breed_insight_variance.Value =      base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].recessive.variance;

            breed_dexterity_num.Value =         base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].dominant.value;
            breed_dexterity_dom_weight.Value =  base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].dominant.weight;
            breed_dexterity_num.Value =         base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].recessive.value;
            breed_dexterity_rec_weight.Value =  base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].recessive.weight;
            breed_dexterity_variance.Value =    base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].dominant.variance;
            breed_dexterity_variance.Value =    base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].recessive.variance;

            breed_speed_num.Value =             base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].dominant.value;
            breed_speed_dom_weight.Value =      base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].dominant.weight;
            breed_speed_num.Value =             base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].recessive.value;
            breed_speed_rec_weight.Value =      base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].recessive.weight;
            breed_speed_variance.Value =        base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].dominant.variance;
            breed_speed_variance.Value =        base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].recessive.variance;

            breed_luck_num.Value =              base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].dominant.value;
            breed_luck_dom_weight.Value =       base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].dominant.weight;
            breed_luck_num.Value =              base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].recessive.value;
            breed_luck_rec_weight.Value =       base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].recessive.weight;
            breed_luck_variance.Value =         base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].dominant.variance;
            breed_luck_variance.Value =         base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].recessive.variance;

            breed_hp_num.Value = base_breed_dic[breed_name].hp;
            breed_mp_num.Value = base_breed_dic[breed_name].mp;
        }
        private void breed_bear_button_CheckedChanged(object sender, EventArgs e)
        {
            BreedButtonChange("Bear");
            toolStripStatusLabel1.Text = "BEARS!";
        }

        private void breed_feline_button_CheckedChanged(object sender, EventArgs e)
        {
            BreedButtonChange("Feline");
            toolStripStatusLabel1.Text = "KITTIES!";
        }

        private void breed_fox_button_CheckedChanged(object sender, EventArgs e)
        {
            BreedButtonChange("Fox");
            toolStripStatusLabel1.Text = "FOXES!";
        }

        private void breed_gorilla_button_CheckedChanged(object sender, EventArgs e)
        {
            BreedButtonChange("Gorilla");
            toolStripStatusLabel1.Text = "BANANA MONSTERS!";
        }

        private void breed_hawk_button_CheckedChanged(object sender, EventArgs e)
        {
            BreedButtonChange("Hawk");
            toolStripStatusLabel1.Text = "BIRD THAT IS NOT AS COOL AS A FALCON!";
        }

        private void breed_lizard_button_CheckedChanged(object sender, EventArgs e)
        {
            BreedButtonChange("Lizard");
            toolStripStatusLabel1.Text = "SOBE!";
        }

        private void breed_spider_button_CheckedChanged(object sender, EventArgs e)
        {
            BreedButtonChange("Spider");
            toolStripStatusLabel1.Text = "NOPES!";
        }

        private void breed_wolf_button_CheckedChanged(object sender, EventArgs e)
        {
            BreedButtonChange("Wolf");
            toolStripStatusLabel1.Text = "LYCANTHROPES!";
        }

        private void SaveBreedButton_Click(object sender, EventArgs e)
        {
            using (XmlWriter writer = XmlWriter.Create("breeds.xml"))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Breeds");

                foreach (Breed breed in breed_array)
                {
                    writer.WriteStartElement("Breed");

                    writer.WriteElementString("Name", breed.name);
                    writer.WriteElementString("Strength", breed.breed_stat[(int)Familiar.Stats.Strength].dominant.value.ToString());      
                    writer.WriteElementString("DominantWeight", breed.breed_stat[(int)Familiar.Stats.Strength].dominant.weight.ToString());
                    writer.WriteElementString("RecessiveWeight", breed.breed_stat[(int)Familiar.Stats.Strength].recessive.weight.ToString());
                    writer.WriteElementString("StrengthVariance", breed.breed_stat[(int)Familiar.Stats.Strength].dominant.variance.ToString());

                    writer.WriteElementString("Stamina", breed.breed_stat[(int)Familiar.Stats.Stamina].dominant.value.ToString());
                    writer.WriteElementString("DominantWeight", breed.breed_stat[(int)Familiar.Stats.Stamina].dominant.weight.ToString());
                    writer.WriteElementString("RecessiveWeight", breed.breed_stat[(int)Familiar.Stats.Stamina].recessive.weight.ToString());
                    writer.WriteElementString("StaminaVariance", breed.breed_stat[(int)Familiar.Stats.Stamina].dominant.variance.ToString());

                    writer.WriteElementString("Spirit", breed.breed_stat[(int)Familiar.Stats.Spirit].dominant.value.ToString());
                    writer.WriteElementString("DominantWeight", breed.breed_stat[(int)Familiar.Stats.Spirit].dominant.weight.ToString());
                    writer.WriteElementString("RecessiveWeight", breed.breed_stat[(int)Familiar.Stats.Spirit].recessive.weight.ToString());
                    writer.WriteElementString("SpiritVariance", breed.breed_stat[(int)Familiar.Stats.Spirit].dominant.variance.ToString());

                    writer.WriteElementString("Willpower", breed.breed_stat[(int)Familiar.Stats.Willpower].dominant.value.ToString());
                    writer.WriteElementString("DominantWeight", breed.breed_stat[(int)Familiar.Stats.Willpower].dominant.weight.ToString());
                    writer.WriteElementString("RecessiveWeight", breed.breed_stat[(int)Familiar.Stats.Willpower].recessive.weight.ToString());
                    writer.WriteElementString("WillpowerVariance", breed.breed_stat[(int)Familiar.Stats.Willpower].dominant.variance.ToString());

                    writer.WriteElementString("Insight", breed.breed_stat[(int)Familiar.Stats.Insight].dominant.value.ToString());
                    writer.WriteElementString("DominantWeight", breed.breed_stat[(int)Familiar.Stats.Insight].dominant.weight.ToString());
                    writer.WriteElementString("RecessiveWeight", breed.breed_stat[(int)Familiar.Stats.Insight].recessive.weight.ToString());
                    writer.WriteElementString("InsightVariance", breed.breed_stat[(int)Familiar.Stats.Insight].dominant.variance.ToString());

                    writer.WriteElementString("Dexterity", breed.breed_stat[(int)Familiar.Stats.Dexterity].dominant.value.ToString());
                    writer.WriteElementString("DominantWeight", breed.breed_stat[(int)Familiar.Stats.Dexterity].dominant.weight.ToString());
                    writer.WriteElementString("RecessiveWeight", breed.breed_stat[(int)Familiar.Stats.Dexterity].recessive.weight.ToString());
                    writer.WriteElementString("DexterityVariance", breed.breed_stat[(int)Familiar.Stats.Dexterity].dominant.variance.ToString());

                    writer.WriteElementString("Speed", breed.breed_stat[(int)Familiar.Stats.Speed].dominant.value.ToString());
                    writer.WriteElementString("DominantWeight", breed.breed_stat[(int)Familiar.Stats.Speed].dominant.weight.ToString());
                    writer.WriteElementString("RecessiveWeight", breed.breed_stat[(int)Familiar.Stats.Speed].recessive.weight.ToString());
                    writer.WriteElementString("SpeedVariance", breed.breed_stat[(int)Familiar.Stats.Speed].dominant.variance.ToString());
              
                    writer.WriteElementString("Luck", breed.breed_stat[(int)Familiar.Stats.Luck].dominant.value.ToString());
                    writer.WriteElementString("DominantWeight", breed.breed_stat[(int)Familiar.Stats.Luck].dominant.weight.ToString());
                    writer.WriteElementString("RecessiveWeight", breed.breed_stat[(int)Familiar.Stats.Luck].recessive.weight.ToString());
                    writer.WriteElementString("LuckVariance", breed.breed_stat[(int)Familiar.Stats.Luck].dominant.variance.ToString());

                    writer.WriteElementString("HP", breed.hp.ToString());
                    writer.WriteElementString("MP", breed.mp.ToString());

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        private void LoadBreedButton_Click(object sender, EventArgs e)
        {
            LoadBreeds();
            breed_bear_button.Checked = false;
            breed_bear_button.Checked = true;
        }

        private void LoadBreeds()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("breeds.xml");
            XmlNode node = doc.DocumentElement.SelectSingleNode("Breed");
            while (node != null)
            {
                string name_in = node.FirstChild.InnerText;
                XmlNode temp_node = node.FirstChild;

                //strength
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Strength].dominant.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Strength].recessive.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Strength].dominant.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Strength].recessive.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Strength].dominant.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Strength].recessive.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                //stamina
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Stamina].dominant.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Stamina].recessive.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Stamina].dominant.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Stamina].recessive.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Stamina].dominant.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Stamina].recessive.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                //spirit
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Spirit].dominant.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Spirit].recessive.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Spirit].dominant.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Spirit].recessive.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Spirit].dominant.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Spirit].recessive.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                //willpower
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Willpower].dominant.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Willpower].recessive.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Willpower].dominant.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Willpower].recessive.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Willpower].dominant.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Willpower].recessive.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                //insight
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Insight].dominant.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Insight].recessive.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Insight].dominant.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Insight].recessive.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Insight].dominant.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Insight].recessive.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                //dexterity
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Dexterity].dominant.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Dexterity].recessive.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Dexterity].dominant.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Dexterity].recessive.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Dexterity].dominant.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Dexterity].recessive.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                //speed
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Speed].dominant.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Speed].recessive.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Speed].dominant.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Speed].recessive.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Speed].dominant.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Speed].recessive.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                //luck
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Luck].dominant.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Luck].recessive.value = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Luck].dominant.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Luck].recessive.weight = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Luck].dominant.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                base_breed_dic[name_in].breed_stat[(int)Familiar.Stats.Luck].recessive.variance = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                //hp
                base_breed_dic[name_in].hp = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;
                //mp
                base_breed_dic[name_in].mp = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;

                //next breed
                node = node.NextSibling;

            }




        }

        private void SaveBreed(String breed_name)
        {
            
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].dominant.value=        breed_strength_num.Value           ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].dominant.weight=       breed_strength_dom_weight.Value    ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].recessive.value=       breed_strength_num.Value           ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].recessive.weight=      breed_strength_rec_weight.Value    ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].dominant.variance=     breed_strength_variance.Value      ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Strength].recessive.variance=    breed_strength_variance.Value      ;

            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].dominant.value=          breed_stam_num.Value                ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].dominant.weight=         breed_stamina_dom_weight.Value      ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].recessive.value=         breed_stam_num.Value                ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].recessive.weight=        breed_stamina_rec_weight.Value      ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].dominant.variance=       breed_stamina_variance.Value        ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Stamina].recessive.variance=      breed_stamina_variance.Value        ;

            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Spirit].dominant.value=           breed_spirit_num.Value              ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Spirit].dominant.weight=          breed_spirit_dom_weight.Value       ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Spirit].recessive.value=          breed_spirit_num.Value              ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Spirit].recessive.weight=         breed_spirit_rec_weight.Value       ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Spirit].dominant.variance=        breed_spirit_variance.Value         ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Spirit].recessive.variance=       breed_spirit_variance.Value         ;

            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].dominant.value=        breed_willpower_num.Value           ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].dominant.weight=       breed_willpower_dom_weight.Value    ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].recessive.value=       breed_willpower_num.Value           ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].recessive.weight=      breed_willpower_rec_weight.Value    ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].dominant.variance=     breed_willpower_variance.Value      ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Willpower].recessive.variance=    breed_willpower_variance.Value      ;

            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].dominant.value=          breed_insight_num.Value             ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].dominant.weight=         breed_insight_dom_weight.Value      ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].recessive.value=         breed_insight_num.Value             ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].recessive.weight=        breed_insight_rec_weight.Value      ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].dominant.variance=       breed_insight_variance.Value        ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Insight].recessive.variance=      breed_insight_variance.Value        ;

            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].dominant.value=        breed_dexterity_num.Value           ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].dominant.weight=       breed_dexterity_dom_weight.Value    ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].recessive.value=       breed_dexterity_num.Value           ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].recessive.weight=      breed_dexterity_rec_weight.Value    ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].dominant.variance=     breed_dexterity_variance.Value      ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Dexterity].recessive.variance=    breed_dexterity_variance.Value      ;

            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].dominant.value=            breed_speed_num.Value               ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].dominant.weight=           breed_speed_dom_weight.Value        ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].recessive.value=           breed_speed_num.Value               ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].recessive.weight=          breed_speed_rec_weight.Value        ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].dominant.variance=         breed_speed_variance.Value          ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Speed].recessive.variance=        breed_speed_variance.Value          ;

            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].dominant.value=             breed_luck_num.Value                ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].dominant.weight=            breed_luck_dom_weight.Value         ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].recessive.value=            breed_luck_num.Value                ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].recessive.weight=           breed_luck_rec_weight.Value         ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].dominant.variance=          breed_luck_variance.Value           ;
            base_breed_dic[breed_name].breed_stat[(int)Familiar.Stats.Luck].recessive.variance=         breed_luck_variance.Value           ;

            base_breed_dic[breed_name].hp=                              breed_hp_num.Value                  ;
            base_breed_dic[breed_name].mp=                              breed_mp_num.Value                  ;

        }
        private void SaveBreeds_Click(object sender, EventArgs e)
        {
            if (breed_bear_button.Checked)
            {
                SaveBreed("Bear");
            }
            if (breed_feline_button.Checked)
            {
                SaveBreed("Feline");
            }
            if (breed_fox_button.Checked)
            {
                SaveBreed("Fox");
            }
            if (breed_gorilla_button.Checked)
            {
                SaveBreed("Gorilla");
            }
            if (breed_hawk_button.Checked)
            {
                SaveBreed("Hawk");
            }
            if (breed_lizard_button.Checked)
            {
                SaveBreed("Lizard");
            }
            if (breed_spider_button.Checked)
            {
                SaveBreed("Spider");
            }
            if (breed_wolf_button.Checked)
            {
                SaveBreed("Wolf");
            }
        } 
        #endregion
        #region Elements

        private void ElementButtonChange(string element_name)
        {
            elements_strength_num.Value = base_element_dic[element_name].stats[(int)Familiar.Stats.Strength];
            elements_stamina_num.Value = base_element_dic[element_name].stats[(int)Familiar.Stats.Stamina];
            elements_spirit_num.Value = base_element_dic[element_name].stats[(int)Familiar.Stats.Spirit];
            elements_willpower_num.Value = base_element_dic[element_name].stats[(int)Familiar.Stats.Willpower];
            elements_insight_num.Value = base_element_dic[element_name].stats[(int)Familiar.Stats.Insight];
            elements_dexterity_num.Value = base_element_dic[element_name].stats[(int)Familiar.Stats.Dexterity];
            elements_speed_num.Value = base_element_dic[element_name].stats[(int)Familiar.Stats.Speed];
            elements_luck_num.Value = base_element_dic[element_name].stats[(int)Familiar.Stats.Luck];
            elements_hp_num.Value = base_element_dic[element_name].hp;
            elements_mp_num.Value = base_element_dic[element_name].mp;
        }

        private void element_air_button_CheckedChanged(object sender, EventArgs e)
        {
            ElementButtonChange("Air");
            toolStripStatusLabel2.Text = "CLOUDS!";
        }

        private void element_earth_button_CheckedChanged(object sender, EventArgs e)
        {
            ElementButtonChange("Earth");            
            toolStripStatusLabel2.Text = "ROCKS!";
        }

        private void element_electric_button_CheckedChanged(object sender, EventArgs e)
        {
            ElementButtonChange("Electric");
            toolStripStatusLabel2.Text = "SHOCKS!";
        }

        private void element_fire_button_CheckedChanged(object sender, EventArgs e)
        {
            ElementButtonChange("Fire");
            toolStripStatusLabel2.Text = "SMELLS LIKE BURNING!";
        }

        private void element_light_button_CheckedChanged(object sender, EventArgs e)
        {
            ElementButtonChange("Light");
            toolStripStatusLabel2.Text = "SHINY!";
        }

        private void element_metal_button_CheckedChanged(object sender, EventArgs e)
        {
            ElementButtonChange("Metal");
            toolStripStatusLabel2.Text = "GUITAR SOLOS!";
        }

        private void element_shadow_button_CheckedChanged(object sender, EventArgs e)
        {
            ElementButtonChange("Shadow");           
            toolStripStatusLabel2.Text = "NINJAS!";
        }

        private void element_water_button_CheckedChanged(object sender, EventArgs e)
        {
            ElementButtonChange("Water");
            toolStripStatusLabel2.Text = "BUBBLES!";
        }

        private void ElementSaveButton(string element_name)
        {
            base_element_dic[element_name].stats[(int)Familiar.Stats.Strength] = elements_strength_num.Value;
            base_element_dic[element_name].stats[(int)Familiar.Stats.Stamina] = elements_stamina_num.Value;
            base_element_dic[element_name].stats[(int)Familiar.Stats.Spirit] = elements_spirit_num.Value;
            base_element_dic[element_name].stats[(int)Familiar.Stats.Willpower] = elements_willpower_num.Value;
            base_element_dic[element_name].stats[(int)Familiar.Stats.Insight] = elements_insight_num.Value;
            base_element_dic[element_name].stats[(int)Familiar.Stats.Dexterity] = elements_dexterity_num.Value;
            base_element_dic[element_name].stats[(int)Familiar.Stats.Speed] = elements_speed_num.Value;
            base_element_dic[element_name].stats[(int)Familiar.Stats.Luck] = elements_luck_num.Value;
            base_element_dic[element_name].hp = elements_hp_num.Value;
            base_element_dic[element_name].mp = elements_mp_num.Value;
        }
        private void save_element_button_Click(object sender, EventArgs e)
        {
            if (element_air_button.Checked)
            {
                ElementButtonChange("Air");
            }
            if (element_earth_button.Checked)
            {
                ElementButtonChange("Earth");             
            }
            if (element_electric_button.Checked)
            {
                ElementButtonChange("Electric");
            }
            if (element_fire_button.Checked)
            {
                ElementButtonChange("Fire");
            }
            if (element_light_button.Checked)
            {
                ElementButtonChange("Light");
            }
            if (element_metal_button.Checked)
            {
                ElementButtonChange("Metal"); 
            }
            if (element_shadow_button.Checked)
            {
                ElementButtonChange("Shadow");  
            }
            if (element_water_button.Checked)
            {
                ElementButtonChange("Water");
            }
        }

        private void elements_load_button_Click(object sender, EventArgs e)
        {
            Load_Elements();
            element_air_button.Checked = false;
            element_air_button.Checked = true;
        }

        private void elements_save_button_Click(object sender, EventArgs e)
        {
            using (XmlWriter writer = XmlWriter.Create("elements.xml"))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Elements");

                foreach (Element element in element_array)
                {
                    writer.WriteStartElement("Element");

                    writer.WriteElementString("Name", element.name);
                    writer.WriteElementString("Strength", element.stats[(int)Familiar.Stats.Strength].ToString());
                    writer.WriteElementString("Stamina", element.stats[(int)Familiar.Stats.Stamina].ToString());
                    writer.WriteElementString("Spirit", element.stats[(int)Familiar.Stats.Spirit].ToString());
                    writer.WriteElementString("Willpower", element.stats[(int)Familiar.Stats.Willpower].ToString());
                    writer.WriteElementString("Insight", element.stats[(int)Familiar.Stats.Insight].ToString());
                    writer.WriteElementString("Dexterity", element.stats[(int)Familiar.Stats.Dexterity].ToString());
                    writer.WriteElementString("Speed", element.stats[(int)Familiar.Stats.Speed].ToString());
                    writer.WriteElementString("Luck", element.stats[(int)Familiar.Stats.Luck].ToString());
                    writer.WriteElementString("HP", element.hp.ToString());
                    writer.WriteElementString("MP", element.mp.ToString());

                    writer.WriteEndElement();

                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        private void Load_Elements()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("elements.xml");
            XmlNode node = doc.DocumentElement.SelectSingleNode("Element");
            while (node != null)
            {
                string name_in = node.FirstChild.InnerText;
                XmlNode temp_node = node.FirstChild;


                base_element_dic[name_in].stats[(int)Familiar.Stats.Strength] = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;

                base_element_dic[name_in].stats[(int)Familiar.Stats.Stamina] = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;

                base_element_dic[name_in].stats[(int)Familiar.Stats.Spirit] = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;

                base_element_dic[name_in].stats[(int)Familiar.Stats.Willpower] = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;

                base_element_dic[name_in].stats[(int)Familiar.Stats.Insight] = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;

                base_element_dic[name_in].stats[(int)Familiar.Stats.Dexterity] = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;

                base_element_dic[name_in].stats[(int)Familiar.Stats.Speed] = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;

                base_element_dic[name_in].stats[(int)Familiar.Stats.Luck] = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;

                base_element_dic[name_in].hp = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;

                base_element_dic[name_in].mp = Convert.ToDecimal(temp_node.NextSibling.InnerText);
                temp_node = temp_node.NextSibling;


                node = node.NextSibling;

            }

        } 
        #endregion

        private void FamiliarDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {

            String temp_string = FamiliarDropDown.SelectedItem.ToString();
            Familiar temp = base_familiar_dic[temp_string];
            temp.Apply_Stats();
            strength_num.Text = temp.stats[(int)Familiar.Stats.Strength].ToString();
            stamina_num.Text = temp.stats[(int)Familiar.Stats.Stamina].ToString();
            spirit_num.Text = temp.stats[(int)Familiar.Stats.Spirit].ToString();
            willpower_num.Text = temp.stats[(int)Familiar.Stats.Willpower].ToString();
            insight_num.Text = temp.stats[(int)Familiar.Stats.Insight].ToString();
            dexterity_num.Text = temp.stats[(int)Familiar.Stats.Dexterity].ToString();
            speed_num.Text = temp.stats[(int)Familiar.Stats.Speed].ToString();
            luck_num.Text = temp.stats[(int)Familiar.Stats.Luck].ToString();
            hp_num.Text = temp.hp.ToString();
            mp_num.Text = temp.mp.ToString();
            name_label.Text = temp.name;
            main_breed_name_label.Text = temp.main_breed.name;
            sub_breed_name_label.Text = temp.sub_breed.name;
            element_name_label.Text = temp.element.name;

 
           
        }

        private void attack_drop_down_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = attack_drop_down.SelectedIndex;
            String temp_string = FamiliarDropDown.SelectedItem.ToString();
            Familiar temp_fam = base_familiar_dic[temp_string];
      

        }

        private void save_familiars_Click(object sender, EventArgs e)
        {
            using (XmlWriter writer = XmlWriter.Create("familiars.xml"))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Familiars");

                foreach (Familiar familiar in familiar_array)
                {
                    writer.WriteStartElement("Familiar");

                    writer.WriteElementString("Name", familiar.name);
                    writer.WriteElementString("MainBreed", familiar.main_breed.name.ToString());
                    writer.WriteElementString("SubBreed", familiar.sub_breed.name.ToString());
                    writer.WriteElementString("Element", familiar.element.ToString());
                    writer.WriteElementString("Index", familiar.index.ToString());
                    
                    //foreach(Attack attack in familiar.Attacks)
                    //{
                    //    writer.WriteStartElement("Attack");
                    //    writer.WriteElementString("Name", attack.name);
                    //    writer.WriteElementString("AttackDistance", attack.attack_distance.ToString());
                    //    writer.WriteElementString("AttackAnimation", attack.attack_animation.ToString());
                    //    writer.WriteElementString("Cooldown", attack.cooldown.ToString());
                    //    writer.WriteElementString("Damage", attack.damage.ToString());
                    //    writer.WriteElementString("HitChance", attack.hit_chance.ToString());
                    //    writer.WriteElementString("CritChance", attack.crit_chance.ToString());
                    //    writer.WriteElementString("MPCost", attack.mp_cost.ToString());
                    //    writer.WriteElementString("Effect1", attack.first_effect.ToString());
                    //    writer.WriteElementString("Effect1Duraion", attack.first_duration.ToString());
                    //    writer.WriteElementString("Effect1Damage1", attack.first_damage.ToString());
                    //    writer.WriteElementString("Effect1Damage2", attack.first_second_damage.ToString());
                    //    writer.WriteElementString("Effect2", attack.second_effect.ToString());
                    //    writer.WriteElementString("Effect2Duraion", attack.second_duration.ToString());
                    //    writer.WriteElementString("Effect2Damage1", attack.second_damage.ToString());
                    //    writer.WriteElementString("Effect2Damage2", attack.second_second_damage.ToString());
                    //    writer.WriteElementString("Effect3", attack.third_effect.ToString());
                    //    writer.WriteElementString("Effect3Duraion", attack.third_duration.ToString());
                    //    writer.WriteElementString("Effect3Damage1", attack.third_damage.ToString());
                    //    writer.WriteElementString("Effect3Damage2", attack.third_second_damage.ToString());
                    //    writer.WriteEndElement();
                    //}

                    writer.WriteEndElement();

                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }



        private decimal threadStatCombination(decimal winner, decimal loser_one, decimal loser_two, decimal loser_three)
        {


            decimal loser_stat = loser_one + loser_two + loser_three;
            loser_stat = loser_stat / 3;
            loser_stat = loser_stat / 4;

            decimal overall_thread_stat = winner+loser_stat;

            return overall_thread_stat;
        }

        private void threadSTRMD_CheckedChanged(object sender, EventArgs e)
        {

            threadSTR.Text = threadStatCombination(numSTRMD.Value, numSTRMR.Value, numSTRSD.Value, numSTRSR.Value).ToString();
        }

        private void threadSTRMR_CheckedChanged(object sender, EventArgs e)
        {
            threadSTR.Text = threadStatCombination(numSTRMR.Value, numSTRMD.Value, numSTRSD.Value, numSTRSR.Value).ToString();
          
        }

        private void threadSTRSD_CheckedChanged(object sender, EventArgs e)
        {
            threadSTR.Text = threadStatCombination(numSTRSD.Value, numSTRMR.Value, numSTRMD.Value, numSTRSR.Value).ToString();
         
        }

        private void threadSTRSR_CheckedChanged(object sender, EventArgs e)
        {
            threadSTR.Text = threadStatCombination(numSTRSR.Value, numSTRMD.Value, numSTRMR.Value, numSTRSD.Value).ToString();

       
        }

        private void threadSTAMD_CheckedChanged(object sender, EventArgs e)
        {
            threadSTA.Text = threadStatCombination(numSTAMD.Value, numSTAMR.Value, numSTASD.Value, numSTASR.Value).ToString();
        }

        private void threadSTAMR_CheckedChanged(object sender, EventArgs e)
        {
            threadSTA.Text = threadStatCombination(numSTAMR.Value, numSTAMD.Value, numSTASD.Value, numSTASR.Value).ToString();
        }

        private void threadSTASD_CheckedChanged(object sender, EventArgs e)
        {
            threadSTA.Text = threadStatCombination(numSTASD.Value, numSTAMR.Value, numSTAMD.Value, numSTASR.Value).ToString();
        }

        private void threadSTASR_CheckedChanged(object sender, EventArgs e)
        {
            threadSTA.Text = threadStatCombination(numSTASR.Value, numSTAMD.Value, numSTAMR.Value, numSTASD.Value).ToString();
        }

        private void threadSPRMD_CheckedChanged(object sender, EventArgs e)
        {
            threadSPR.Text =  threadStatCombination(numSPRMD.Value, numSPRMR.Value, numSPRSD.Value, numSPRSR.Value).ToString();
        }

        private void threadSPRMR_CheckedChanged(object sender, EventArgs e)
        {
            threadSPR.Text = threadStatCombination(numSPRMR.Value, numSPRMD.Value, numSPRSD.Value, numSPRSR.Value).ToString();

        }

        private void threadSPRSD_CheckedChanged(object sender, EventArgs e)
        {
            threadSPR.Text = threadStatCombination(numSPRSD.Value, numSPRMR.Value, numSPRMD.Value, numSPRSR.Value).ToString();

        }

        private void threadSPRSR_CheckedChanged(object sender, EventArgs e)
        {
            threadSPR.Text = threadStatCombination(numSPRSR.Value, numSPRMD.Value, numSPRMR.Value, numSPRSD.Value).ToString();

        }

        private void threadWILMD_CheckedChanged(object sender, EventArgs e)
        {
            threadWIL.Text = threadStatCombination(numWILMD.Value, numWILMR.Value, numWILSD.Value, numWILSR.Value).ToString();

        }

        private void threadWILMR_CheckedChanged(object sender, EventArgs e)
        {
            threadWIL.Text = threadStatCombination(numWILMR.Value, numWILMD.Value, numWILSD.Value, numWILSR.Value).ToString();

        }

        private void threadWILSD_CheckedChanged(object sender, EventArgs e)
        {
            threadWIL.Text = threadStatCombination(numWILSD.Value, numWILMR.Value, numWILMD.Value, numWILSR.Value).ToString();

        }

        private void threadWILSR_CheckedChanged(object sender, EventArgs e)
        {
            threadWIL.Text = threadStatCombination(numWILSR.Value, numWILMD.Value, numWILMR.Value, numWILSD.Value).ToString();

        }

        private void threadINSMD_CheckedChanged(object sender, EventArgs e)
        {
            threadINS.Text = threadStatCombination(numINSMD.Value, numINSMR.Value, numINSSD.Value, numINSSR.Value).ToString();

        }

        private void threadINSMR_CheckedChanged(object sender, EventArgs e)
        {
            threadINS.Text = threadStatCombination(numINSMR.Value, numINSMD.Value, numINSSD.Value, numINSSR.Value).ToString();

        }

        private void threadINSSD_CheckedChanged(object sender, EventArgs e)
        {
            threadINS.Text = threadStatCombination(numINSSD.Value, numINSMR.Value, numINSMD.Value, numINSSR.Value).ToString();

        }

        private void threadINSSR_CheckedChanged(object sender, EventArgs e)
        {
            threadINS.Text = threadStatCombination(numINSSR.Value, numINSMD.Value, numINSMR.Value, numINSSD.Value).ToString();

        }

        private void threadDEXMD_CheckedChanged(object sender, EventArgs e)
        {
            threadDEX.Text = threadStatCombination(numDEXMD.Value, numDEXMR.Value, numDEXSD.Value, numDEXSR.Value).ToString();

        }

        private void threadDEXMR_CheckedChanged(object sender, EventArgs e)
        {
            threadDEX.Text = threadStatCombination(numDEXMR.Value, numDEXMD.Value, numDEXSD.Value, numDEXSR.Value).ToString();

        }

        private void threadDEXSD_CheckedChanged(object sender, EventArgs e)
        {
            threadDEX.Text = threadStatCombination(numDEXSD.Value, numDEXMR.Value, numDEXMD.Value, numDEXSR.Value).ToString();

        }

        private void threadDEXSR_CheckedChanged(object sender, EventArgs e)
        {
            threadDEX.Text = threadStatCombination(numDEXSR.Value, numDEXMD.Value, numDEXMR.Value, numDEXSD.Value).ToString();

        }

        private void threadSPDMD_CheckedChanged(object sender, EventArgs e)
        {
            threadSPD.Text = threadStatCombination(numSPDMD.Value, numSPDMR.Value, numSPDSD.Value, numSPDSR.Value).ToString();

        }

        private void threadSPDMR_CheckedChanged(object sender, EventArgs e)
        {
            threadSPD.Text = threadStatCombination(numSPDMR.Value, numSPDMD.Value, numSPDSD.Value, numSPDSR.Value).ToString();

        }

        private void threadSPDSD_CheckedChanged(object sender, EventArgs e)
        {
            threadSPD.Text = threadStatCombination(numSPDSD.Value, numSPDMR.Value, numSPDMD.Value, numSPDSR.Value).ToString();

        }

        private void threadSPDSR_CheckedChanged(object sender, EventArgs e)
        {
            threadSPD.Text = threadStatCombination(numSPDSR.Value, numSPDMD.Value, numSPDMR.Value, numSPDSD.Value).ToString();

        }

        private void threadLCKMD_CheckedChanged(object sender, EventArgs e)
        {
            threadLCK.Text = threadStatCombination(numLCKMD.Value, numLCKMR.Value, numLCKSD.Value, numLCKSR.Value).ToString();

        }

        private void threadLCKMR_CheckedChanged(object sender, EventArgs e)
        {
            threadLCK.Text = threadStatCombination(numLCKMR.Value, numLCKMD.Value, numLCKSD.Value, numLCKSR.Value).ToString();

        }

        private void threadLCKSD_CheckedChanged(object sender, EventArgs e)
        {
            threadLCK.Text = threadStatCombination(numLCKSD.Value, numLCKMR.Value, numLCKMD.Value, numLCKSR.Value).ToString();

        }

        private void threadLCKSR_CheckedChanged(object sender, EventArgs e)
        {
            threadLCK.Text = threadStatCombination(numLCKSR.Value, numLCKMD.Value, numLCKMR.Value, numLCKSD.Value).ToString();

        }

        private void testcombine1_Click(object sender, EventArgs e)
        {
            Familiar test = new Familiar(familiar_array[0], familiar_array[205]);
            Familiar test2 = new Familiar(familiar_array[6], familiar_array[156]);
            Familiar test3 = new Familiar(familiar_array[504], familiar_array[406]);
            Familiar test4 = new Familiar(familiar_array[250], familiar_array[57]);
            Familiar test5 = new Familiar(familiar_array[120], familiar_array[350]);



        }


 




    }
}
